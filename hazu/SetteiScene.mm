
#import "SetteiScene.h"
#import "ScenesManager.h"

// 各種のスプライトを表示するためのレイヤーを作成し、
// シーンに登録した上でシーン返します。
@implementation SetteiScene
+ (CCScene *)sceneWithSceneId:(NSInteger)sceneId:(BOOL)nextPage {
	CCScene *scene = [SetteiScene node];
	CCLayer *layer = [SetteiLayer layerWithSceneId:sceneId];
	
    // 作成したLastSceneクラスのレイヤーをsceneに
    // 追加した上で、sceneの方を返しています。
	[scene addChild: layer];
    
	return scene;
}


@end

#pragma mark -
@implementation  SetteiLayer
#pragma mark インスタンスの初期化/解放
// インスタンスの初期化を行うメソッド。
// このシーン（レイヤー）で使用するオブジェクトを作成・初期化します。
+ (id)layerWithSceneId:(NSInteger)sceneId {
    return [[[self alloc] initWithSceneId:sceneId] autorelease];
}

- (id)initWithSceneId:(NSInteger)sceneId {
    
    if( (self=[super init])) {

        
        self.tag = sceneId;
        
        [self setteiRead];
        
        CCSprite *sprite = [CCSprite spriteWithFile:@"settei.pvr.gz"];
        sprite.anchorPoint = CGPointMake(0, 0);
        [self addChild:sprite];
        
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"otherButton.plist" textureFile:@"otherButton.pvr.gz"];

        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"otherButton.pvr.gz"];
        [self addChild:spriteSheet];

        CCSprite *buttonSprite1 = [CCSprite spriteWithSpriteFrameName:@"hpButton.png"];
        
        CCSprite *buttonSprite2 = [CCSprite spriteWithSpriteFrameName:@"hpButtonOn.png"];
        
        CCSprite *buttonSprite3 = [CCSprite spriteWithSpriteFrameName:@"hpButtonOn.png"];
        
        CCSprite *buttonSprite4 = [CCSprite spriteWithSpriteFrameName:@"yukariButton.png"];
        
        CCSprite *buttonSprite5 = [CCSprite spriteWithSpriteFrameName:@"yukariButtonOn.png"];
        
        CCSprite *buttonSprite6 = [CCSprite spriteWithSpriteFrameName:@"yukariButtonOn.png"];
        
        CCMenuItemImage *maryItem1 = [CCMenuItemImage 
                                      itemFromNormalImage:@"on.png" 
                                      selectedImage: @"on.png"
                                      target:self
                                      selector:@selector(pressMenuItem:)];
        maryItem1.position = ccp(12, 160);
        maryItem1.tag =1;

        
        CCMenuItemImage *maryItem2 = [CCMenuItemImage 
                                      itemFromNormalImage:@"off.png" 
                                      selectedImage: @"off.png"
                                      target:self
                                      selector:@selector(pressMenuItem:)];
        maryItem2.position = ccp(173, 160);
        maryItem2.tag =2;
        
        CCMenuItemImage *maryItem3 = [CCMenuItemImage 
                                      itemFromNormalImage:@"on.png" 
                                      selectedImage: @"on.png"
                                      target:self
                                      selector:@selector(pressMenuItem:)];
        maryItem3.position = ccp(12, 66);
        maryItem3.tag =3;
        
        
        CCMenuItemImage *maryItem4 = [CCMenuItemImage 
                                      itemFromNormalImage:@"off.png" 
                                      selectedImage: @"off.png"
                                      target:self
                                      selector:@selector(pressMenuItem:)];
        maryItem4.position = ccp(173, 66);
        maryItem4.tag =4;
        
        
        CCMenuItemImage *maryItem5 = [CCMenuItemImage 
                                      itemFromNormalImage:@"on.png" 
                                      selectedImage: @"on.png"
                                      target:self
                                      selector:@selector(pressMenuItem:)];
        maryItem5.position = ccp(12, -29);
        maryItem5.tag =5;
        
        
        CCMenuItemImage *maryItem6 = [CCMenuItemImage 
                                      itemFromNormalImage:@"off.png" 
                                      selectedImage: @"off.png"
                                      target:self
                                      selector:@selector(pressMenuItem:)];
        maryItem6.position = ccp(173, -29);
        maryItem6.tag =6;
        
        CCMenuItemImage *maryItem7 = [CCMenuItemImage 
                                      itemFromNormalImage:@"back_btn.pvr.gz" 
                                      selectedImage: @"back_btn.pvr.gz"
                                      target:self
                                      selector:@selector(pressMenuItem:)];
        maryItem7.position = ccp(0, -310);
        maryItem7.tag =7;
        
        
        CCMenu *menu2 = [CCMenu menuWithItems:maryItem1,maryItem2,maryItem3,maryItem4,maryItem5,maryItem6,maryItem7,nil];
        [self addChild:menu2 z:2];
        
        sprite2[0] = [CCSprite spriteWithFile:@"setteiwaku.pvr.gz"];
        if(optionnum3[0] == 0){
            sprite2[0].position = ccp(524, 543);
        }else{
            sprite2[0].position = ccp(683, 543);
        }
        [self addChild:sprite2[0]];
        
        sprite2[1] = [CCSprite spriteWithFile:@"setteiwaku.pvr.gz"];
        if(optionnum3[1] == 0){
            sprite2[1].position = ccp(524, 450);
        }else{
            sprite2[1].position = ccp(683, 450);
        }
        [self addChild:sprite2[1]];
        
        sprite2[2] = [CCSprite spriteWithFile:@"setteiwaku.pvr.gz"];
        if(optionnum3[2] == 0){
            sprite2[2].position = ccp(524, 355);
        }else{
            sprite2[2].position = ccp(683, 355);
        }
        [self addChild:sprite2[2]];
        
        
        //HPとゆかりの地ボタン
        CCMenuItemSprite *maryItem8 = [CCMenuItemSprite 
                                       itemFromNormalSprite:buttonSprite1
                                       selectedSprite:buttonSprite2
                                       disabledSprite:buttonSprite3
                                       target:self
                                       selector:@selector(pressMenuItem2:)];
        maryItem8.position = ccp(0, 0);
        maryItem8.tag =1;
        
        
        CCMenuItemSprite *maryItem9 = [CCMenuItemSprite 
                                       itemFromNormalSprite:buttonSprite4
                                       selectedSprite:buttonSprite5
                                       disabledSprite:buttonSprite6
                                       target:self
                                      selector:@selector(pressMenuItem2:)];
        maryItem9.position = ccp(0, -70);
        maryItem9.tag =2;
        
        CCMenu *menu3 = [CCMenu menuWithItems:maryItem8,maryItem9,nil];
        
        menu3.position = ccp(522, 260);
        [self addChild:menu3 z:3];
        
    }
    return self;
}

-(void)pressMenuItem:(id)sender{
    if([sender tag] == 1){
        id move1 = [CCMoveTo actionWithDuration:0.3f position:ccp(524, 543)];
        id ease = [CCEaseSineOut actionWithAction:move1];
        [sprite2[0] runAction:ease];
        optionnum3[0]=0;
        
    }else if([sender tag] == 2){
        id move1 = [CCMoveTo actionWithDuration:0.3f position:ccp(683, 543)];
        id ease = [CCEaseSineOut actionWithAction:move1];
        [sprite2[0] runAction:ease];
        optionnum3[0]=1;
        
    }else if([sender tag] == 3){
        id move1 = [CCMoveTo actionWithDuration:0.3f position:ccp(524, 450)];
        id ease = [CCEaseSineOut actionWithAction:move1];
        [sprite2[1] runAction:ease];
         optionnum3[1]=0;
    }else if([sender tag] == 4){
        id move1 = [CCMoveTo actionWithDuration:0.3f position:ccp(683, 450)];
        id ease = [CCEaseSineOut actionWithAction:move1];
        [sprite2[1] runAction:ease];
         optionnum3[1]=1;
    }else if([sender tag] == 5){
        id move1 = [CCMoveTo actionWithDuration:0.3f position:ccp(524, 355)];
        id ease = [CCEaseSineOut actionWithAction:move1];
        [sprite2[2] runAction:ease];
         optionnum3[2]=0;
    }else if([sender tag] == 6){
        id move1 = [CCMoveTo actionWithDuration:0.3f position:ccp(683, 355)];
        id ease = [CCEaseSineOut actionWithAction:move1];
        [sprite2[2] runAction:ease];
         optionnum3[2]=1;
    }else if([sender tag] == 7){
        
        
        //設定の保存
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setInteger:optionnum3[0] forKey:@"OPTION1"];
        [userDefaults setInteger:optionnum3[1] forKey:@"OPTION2"];
        [userDefaults setInteger:optionnum3[2] forKey:@"OPTION3"];
        [userDefaults synchronize];

        CCScene *newScene = [SceneManager nextSceneOfScene:self.parent
                                                    choice:kSceneChoiseC next:true];
        [CCTransitionHelper replaceScene:newScene transitionType:TransitionTypePageBackward duration:0.5];
    }
    
    
}

-(void)pressMenuItem2:(id)sender{
    if([sender tag] == 1){
        NSString *path = [NSString stringWithFormat:@"http://www.karakuri-books.com/"];
        NSURL *url = [NSURL URLWithString:path];
        NSURLRequest *req = [NSURLRequest requestWithURL:url];
        webView_ = [[UIWebView alloc] initWithFrame:CGRectMake(0, 46, 1024, 768)];
        webView_.bounds = CGRectMake(0, 46, 1024, 768);
        webView_.scalesPageToFit = YES;
        
        //UIToolbarの生成
        toolBar = [[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 1024, 46)] autorelease];
        
        
        //インジケーターの追加
         activityInducator_ = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
         activityInducator_.frame = CGRectMake(0, 0, 20, 20);
         UIBarButtonItem *inducator = [[UIBarButtonItem alloc] initWithCustomView:activityInducator_];
         UIBarButtonItem *adjustment =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
         
        
        //戻るボタン
        UIBarButtonItem *goBackButton = [[UIBarButtonItem alloc]init];
        goBackButton.title = @"閉じる";
        goBackButton.style = UIBarButtonItemStyleBordered;
        goBackButton.target = self;
        goBackButton.action = @selector(goBack);
        
        NSArray *elements = [[NSArray alloc] initWithObjects:goBackButton,adjustment,inducator,nil];
        [adjustment release];
        [inducator release];
        [goBackButton release];
        adjustment = nil;
        inducator = nil;
        goBackButton = nil;
        [toolBar setItems:elements animated:YES];
        [elements release];
        elements = nil;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:1.0f];
        [UIView setAnimationTransition:UIViewAnimationTransitionCurlDown 
                               forView:[[CCDirector sharedDirector] openGLView] cache:YES];
        webView_.delegate =self;
        [[[CCDirector sharedDirector] openGLView] addSubview:webView_];
        [[[CCDirector sharedDirector] openGLView] addSubview:toolBar];
        
        [UIView commitAnimations];
        [webView_ loadRequest:req];
       
        
    }else if([sender tag] == 2){
        CCScene *newScene = [SceneManager nextSceneOfScene:self.parent
                                                    choice:kSceneChoiseE next:true];
        [CCTransitionHelper replaceScene:newScene transitionType:TransitionTypePageForward duration:0.5];

    }
}

-(void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{

}

//設定ファイル読み込み
-(void)setteiRead{
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    optionnum3[0] = [userDefault integerForKey:@"OPTION1"];
    optionnum3[1] = [userDefault integerForKey:@"OPTION2"];
    optionnum3[2] = [userDefault integerForKey:@"OPTION3"];
    
}

-(void)webViewDidStartLoad:webView_{
 [activityInducator_ startAnimating];
}
 
 -(void)webViewDidFinishLoad:webView_{
 [activityInducator_ stopAnimating];
}

//webViewを閉じる-------------------------------------------------------------------
- (void)goBack {
	[UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0f];
    [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp
                           forView:[[CCDirector sharedDirector] openGLView] cache:YES];
    [webView_ removeFromSuperview];
    [toolBar removeFromSuperview];
    [UIView commitAnimations];
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
    [self removeAllChildrenWithCleanup:YES];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    [[CCTextureCache sharedTextureCache] removeAllTextures];
    [webView_ release];
    [activityInducator_ release];
    //[toolBar release];
	[super dealloc];
}

@end
