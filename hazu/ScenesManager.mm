
#import "ScenesManager.h"
#include "FirstScene.h"
#include "MiddleScene.h"
#include "LastScene.h"
#include "SetteiScene.h"
static struct {
    int current;              // 現在のシーン番号
    int next;                 //移動先シーン番号
    NSString *sceneClassName; // 現在のシーンに使うクラス名
} Graph[] = {
    { 0,  1,@"FirstScene"},//説明1
    { 1,  2, @"FirstScene"},//説明2
    { 2,  3, @"FirstScene"},//タイトル
    { 3,  4, @"MiddleScene"},//page1
    { 3,  5, @"MiddleScene"},//page2
    { 4,  6, @"MiddleScene"},//page3
    { 5,  7, @"MiddleScene"},//page4
    { 6,  8, @"MiddleScene"},//page5
    { 7,  9, @"MiddleScene"},//page6
    { 8, 10, @"MiddleScene"},//page7
    { 9, 11, @"MiddleScene"},//page8
    {10, 12, @"MiddleScene"},//page9
    {11, 13, @"MiddleScene"},//page10
    {12, 14, @"MiddleScene"},//page11
    {13, 15, @"MiddleScene"},//page12
    {14, 16, @"MiddleScene" },//page13
    {15, 17, @"MiddleScene"},//page14
    {16, 18, @"MiddleScene"},//page15
    {17, 19, @"MiddleScene"},//page16
    {18, 20, @"MiddleScene"},//page17
    {19, 21, @"MiddleScene"},//page18
    
    {20, 22, @"LastScene"},//終わりページ
    {21, 23, @"LastScene"},//クレジット
    {22, 24, @"LastScene"},//ゆかりの地
    {23, 25, @"LastScene"},//他物語
    {24, 26, @"SetteiScene"},//設定
};

@implementation SceneManager
+ (CCScene *)nextSceneOfScene:(CCNode *)from choice:(NSInteger)choise next:(BOOL)nextPage {
    int nextSceneId = 0;
    
    if (choise == 0) {
        nextSceneId = Graph[from.tag].next;
    }else if (choise == 1){
        nextSceneId = Graph[from.tag].current;
    }else if (choise == 2){
        nextSceneId = 2;
    }else if (choise == 3){
        nextSceneId = 25;
    }else{
        nextSceneId = 23;
    }
    CCScene *nextScene = [NSClassFromString(Graph[nextSceneId].sceneClassName)
                          sceneWithSceneId:nextSceneId:nextPage];
    nextScene.tag = nextSceneId;
    return nextScene;
}

//テキスト表示
+ (NSString *)textForSceneId:(int)sceneId {
    sceneId = sceneId-3;
    
    NSDictionary * dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"data" ofType:@"plist"]];
    NSArray *components;
    components = [dictionary allKeys];
    NSArray *sorted;
    sorted = [components sortedArrayUsingSelector:@selector(compare:)];
    NSArray *Page2;
    Page2 = sorted;
    NSString *title;
    title = [Page2 objectAtIndex:sceneId];
    NSString *Story_text;
    Story_text = [dictionary objectForKey:title];
    
    return Story_text;
}


@end
