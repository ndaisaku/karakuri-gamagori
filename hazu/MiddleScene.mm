// Import the interfaces
#import "MiddleScene.h"
#import "ScenesManager.h"
#import "SoundPlayer.h"
#import <Foundation/Foundation.h>
#include <mach/mach.h>

#define PTM_RATIO 32.0

// enums that will be used as tags
enum {
	kTagTileMap = 1,
	kTagBatchNode = 1,
	kTagAnimation1 = 1,
};


@implementation MiddleScene

+ (CCScene *)sceneWithSceneId:(NSInteger)sceneId:(BOOL)nextPage {
	CCScene *scene = [MiddleScene node];
	CCLayer *layer = [MiddleLayer layerWithSceneId:sceneId:nextPage];

	// 作成したMiddleSceneクラスのレイヤーをsceneに追加した上で
    // sceneの方を返しています。
	[scene addChild: layer];
	return scene;
}

@end


#pragma mark -

@implementation MiddleLayer

static bool lastReturn = false; // lastSceneから戻ったかどうか

// lastReturnの切り替え
+(void)lastReturn {
    lastReturn = true;
}

#pragma mark インスタンスの初期化/解放
// インスタンスの初期化を行うメソッド。
// このシーン（レイヤー）で使用するオブジェクトを作成・初期化します。
+ (id)layerWithSceneId:(NSInteger)sceneId:(BOOL)nextPage {
    return [[[self alloc] initWithSceneId:sceneId:nextPage] autorelease];
}

- (id)initWithSceneId:(NSInteger)sceneId:(BOOL)nextPage{
	
	if( (self=[super init])) {
        
        page = sceneId-2;
        next = nextPage;
       
        //設定を読み込み
        [self setteiRead];
        
        
        //BGMの読み込み
        /* 
            BGMの必要なページはbgmPlay
            BGMのないページは前後のページのBGMをbgmStop
         */

        if (page == 1){
            if (next) {
                [SoundPlayer bgmLoad:optionnum3[1]];
            }
            [SoundPlayer bgmPlay:optionnum3[1]:0];
            [SoundPlayer bgmStop:optionnum3[1]:5];
        } else if (page == 2){
            [SoundPlayer bgmPlay:optionnum3[1]:5];
        } else if (page == 3){
            [SoundPlayer bgmStop:optionnum3[1]:5];
        } else if (page == 4){
            [SoundPlayer bgmStop:optionnum3[1]:6];
        } else if (page == 5){
            [SoundPlayer bgmPlay:optionnum3[1]:6];
        } else if (page == 6) {
            [SoundPlayer bgmStop:optionnum3[1]:1];
        } else if (page == 7) {
            [SoundPlayer bgmStop:optionnum3[1]:6];
            [SoundPlayer bgmPlay:optionnum3[1]:1];
        } else if (page == 8) {
            [SoundPlayer bgmPlay:optionnum3[1]:2];
        } else if (page == 9) {
            [SoundPlayer bgmStop:optionnum3[1]:2];
            [SoundPlayer bgmStop:optionnum3[1]:3];
        } else if (page == 10) {
            [SoundPlayer bgmPlay:optionnum3[1]:3];
        } else if (page == 11) {
            [SoundPlayer bgmPlay:optionnum3[1]:4];
        } else if (page == 12) {
            [SoundPlayer bgmPlay:optionnum3[1]:2];
        } else if (page == 13) {
            [SoundPlayer bgmStop:optionnum3[1]:2];
        } else if (page == 14) {
            [SoundPlayer bgmStop:optionnum3[1]:7];
        } else if (page == 15) {
            [SoundPlayer bgmPlay:optionnum3[1]:7];
        } else if (page == 16) {
            [SoundPlayer bgmStop:optionnum3[1]:1];
            [SoundPlayer bgmStop:optionnum3[1]:7];
        } else if (page == 17) {
            [SoundPlayer bgmPlay:optionnum3[1]:1];
            [SoundPlayer bgmStop:optionnum3[1]:5];
        } else if (page == 18) {
            //[SoundPlayer bgmLoad:optionnum3[1]];
            if(lastReturn) {
                [SoundPlayer bgmLoad:optionnum3[1]];
            }
            [SoundPlayer bgmPlay:optionnum3[1]:5];
            lastReturn = false;
        }
        
		//画面サイズ
        CGSize winSize = [[CCDirector sharedDirector] winSize];
        
		// enable touches
		self.isTouchEnabled = YES;
		
		// enable accelerometer
		self.isAccelerometerEnabled = NO;
		
		CGSize screenSize = [CCDirector sharedDirector].winSize;
        
        // ナレーションの再生フラグ
        soundFlag = false;
		
		// Define the gravity vector.
		b2Vec2 gravity;
		gravity.Set(0.0f, -10.0f);
		
		// Do we want to let bodies sleep?
		// This will speed up the physics simulation
		bool doSleep = false;
		
		// Construct a world object, which will hold and simulate the rigid bodies.
		world = new b2World(gravity, doSleep);
		
		world->SetContinuousPhysics(true);
        
		/*
         //Debug Draw functions
         m_debugDraw = new GLESDebugDraw( PTM_RATIO );
         world->SetDebugDraw(m_debugDraw);
         
         uint32 flags = 0;
         flags += b2DebugDraw::e_shapeBit;
         flags += b2DebugDraw::e_jointBit;
         flags += b2DebugDraw::e_aabbBit;
         //flags += b2DebugDraw::e_pairBit;
         //flags += b2DebugDraw::e_centerOfMassBit;
         m_debugDraw->SetFlags(flags);
         */
		
		// Define the ground body.
		b2BodyDef groundBodyDef;
		groundBodyDef.position.Set(0, 0); // bottom-left corner
		
		// Call the body factory which allocates memory for the ground body
		// from a pool and creates the ground box shape (also from a pool).
		// The body is also added to the world.
		groundBody = world->CreateBody(&groundBodyDef);
		
		// Define the ground box shape.
		b2PolygonShape groundBox;		
		
		// bottom
		groundBox.SetAsEdge(b2Vec2(0,0), b2Vec2(screenSize.width/PTM_RATIO,0));
		groundBody->CreateFixture(&groundBox,0);
		
		// top
		groundBox.SetAsEdge(b2Vec2(0,screenSize.height/PTM_RATIO), b2Vec2(screenSize.width/PTM_RATIO,screenSize.height/PTM_RATIO));
		groundBody->CreateFixture(&groundBox,0);
		
		// left
		groundBox.SetAsEdge(b2Vec2(0,screenSize.height/PTM_RATIO), b2Vec2(0,0));
		groundBody->CreateFixture(&groundBox,0);
		
		// right
		groundBox.SetAsEdge(b2Vec2(screenSize.width/PTM_RATIO,screenSize.height/PTM_RATIO), b2Vec2(screenSize.width/PTM_RATIO,0));
		groundBody->CreateFixture(&groundBox,0);
        
        SE_ArrayCount = 0;
        
		//背景画面
        NSString *path;
        if (page == 1) {
            path = [NSString stringWithFormat:@"background_black.png"];
            texture[sceneId] = [[CCTextureCache sharedTextureCache] addImage:path];
            CCSprite *spriteBack = [CCSprite spriteWithTexture:texture[sceneId] rect:CGRectMake(0,0,winSize.width,winSize.height)];
            spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);                
            [self addChild: spriteBack z:-1];
        } else if (page == 2) {
            path = [NSString stringWithFormat:@"background_gama02.pvr.gz"];
            texture[sceneId] = [[CCTextureCache sharedTextureCache] addImage:path];
            CCSprite *spriteBack = [CCSprite spriteWithTexture:texture[sceneId] rect:CGRectMake(0,0,winSize.width,winSize.height)];
            spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);                
            [self addChild: spriteBack z:-1];
        } else if (page == 3) {
            path = [NSString stringWithFormat:@"background_gama03.pvr.gz"];
            texture[sceneId] = [[CCTextureCache sharedTextureCache] addImage:path];
            CCSprite *spriteBack = [CCSprite spriteWithTexture:texture[sceneId] rect:CGRectMake(0,0,winSize.width,winSize.height)];
            spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);                
            [self addChild: spriteBack z:-1];
        } else if (page == 4) {
            path = [NSString stringWithFormat:@"background_gama04.pvr.gz"];
            texture[sceneId] = [[CCTextureCache sharedTextureCache] addImage:path];
            CCSprite *spriteBack = [CCSprite spriteWithTexture:texture[sceneId] rect:CGRectMake(0,0,winSize.width,winSize.height)];
            spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);                
            [self addChild: spriteBack z:-1];
        } else if (page == 5) {
            path = [NSString stringWithFormat:@"background_white.png"];
            texture[sceneId] = [[CCTextureCache sharedTextureCache] addImage:path];
            CCSprite *spriteBack = [CCSprite spriteWithTexture:texture[sceneId] rect:CGRectMake(0,0,winSize.width,winSize.height)];
            spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);                
            [self addChild: spriteBack z:-1];
        } else if (page == 6) {
            path = [NSString stringWithFormat:@"background_white.png"];
            texture[sceneId] = [[CCTextureCache sharedTextureCache] addImage:path];
            CCSprite *spriteBack = [CCSprite spriteWithTexture:texture[sceneId] rect:CGRectMake(0,0,winSize.width,winSize.height)];
            spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);               
            [self addChild: spriteBack z:-1];
        } else if (page == 7) {
            path = [NSString stringWithFormat:@"background_gama07.pvr.gz"];
            texture[sceneId] = [[CCTextureCache sharedTextureCache] addImage:path];
            CCSprite *spriteBack = [CCSprite spriteWithTexture:texture[sceneId] rect:CGRectMake(0,0,winSize.width,winSize.height)];
            spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);                
            [self addChild: spriteBack z:-1];
        } else if (page == 8) {
            path = [NSString stringWithFormat:@"background_white.png"];
            texture[sceneId] = [[CCTextureCache sharedTextureCache] addImage:path];
            CCSprite *spriteBack = [CCSprite spriteWithTexture:texture[sceneId] rect:CGRectMake(0,0,winSize.width,winSize.height)];
            spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);                
            [self addChild: spriteBack z:-1];
        } else if (page == 9) {
            path = [NSString stringWithFormat:@"background_gama09.png"];
            texture[sceneId] = [[CCTextureCache sharedTextureCache] addImage:path];
            CCSprite *spriteBack = [CCSprite spriteWithTexture:texture[sceneId] rect:CGRectMake(0,0,winSize.width,winSize.height)];
            spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);                
            [self addChild: spriteBack z:-1];
        } else if (page == 10) {
            path = [NSString stringWithFormat:@"background_gama10.pvr.gz"];
            texture[sceneId] = [[CCTextureCache sharedTextureCache] addImage:path];
            CCSprite *spriteBack = [CCSprite spriteWithTexture:texture[sceneId] rect:CGRectMake(0,0,winSize.width,winSize.height)];
            spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);                
            [self addChild: spriteBack z:-1];
        } else if (page == 11) {
            path = [NSString stringWithFormat:@"background_gama11_2.pvr.gz"];
            texture[sceneId] = [[CCTextureCache sharedTextureCache] addImage:path];
            CCSprite *spriteBack = [CCSprite spriteWithTexture:texture[sceneId] rect:CGRectMake(0,0,winSize.width,winSize.height)];
            spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);
            [self addChild: spriteBack z:-1];
        } else if (page == 12) {
            path = [NSString stringWithFormat:@"background_gama12.pvr.gz"];
            texture[sceneId] = [[CCTextureCache sharedTextureCache] addImage:path];
            CCSprite *spriteBack = [CCSprite spriteWithTexture:texture[sceneId] rect:CGRectMake(0,0,winSize.width,winSize.height)];
            spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);
            [self addChild: spriteBack z:-1];
        } else if (page == 13) {
            path = [NSString stringWithFormat:@"background_gama13.pvr.gz"];
            texture[sceneId] = [[CCTextureCache sharedTextureCache] addImage:path];
            CCSprite *spriteBack = [CCSprite spriteWithTexture:texture[sceneId] rect:CGRectMake(0,0,winSize.width,winSize.height)];
            spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);
            [self addChild: spriteBack z:-1];
        } else if (page == 14) {
            path = [NSString stringWithFormat:@"background_gama14.pvr.gz"];
            texture[sceneId] = [[CCTextureCache sharedTextureCache] addImage:path];
            CCSprite *spriteBack = [CCSprite spriteWithTexture:texture[sceneId] rect:CGRectMake(0,0,winSize.width,winSize.height)];
            spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);
            [self addChild: spriteBack z:-1];
        }else if (page == 15) {
            path = [NSString stringWithFormat:@"background_white.png"];
            texture[sceneId] = [[CCTextureCache sharedTextureCache] addImage:path];
            CCSprite *spriteBack = [CCSprite spriteWithTexture:texture[sceneId] rect:CGRectMake(0,0,winSize.width,winSize.height)];
            spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);                
            [self addChild: spriteBack z:-1];
        }else if (page == 16) {
            path = [NSString stringWithFormat:@"background_gama16.pvr.gz"];
            texture[sceneId] = [[CCTextureCache sharedTextureCache] addImage:path];
            CCSprite *spriteBack = [CCSprite spriteWithTexture:texture[sceneId] rect:CGRectMake(0,0,winSize.width,winSize.height)];
            spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);                
            [self addChild: spriteBack z:-1];
        }else if (page == 17) {
            path = [NSString stringWithFormat:@"background_gama17.pvr.gz"];
            texture[sceneId] = [[CCTextureCache sharedTextureCache] addImage:path];
            CCSprite *spriteBack = [CCSprite spriteWithTexture:texture[sceneId] rect:CGRectMake(0,0,winSize.width,winSize.height)];
            spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);                
            [self addChild: spriteBack z:-1];
        } else if (page == 18) {
            path = [NSString stringWithFormat:@"background_white.png"];
            texture[sceneId] = [[CCTextureCache sharedTextureCache] addImage:path];
            CCSprite *spriteBack = [CCSprite spriteWithTexture:texture[sceneId] rect:CGRectMake(0,0,winSize.width,winSize.height)];
            spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);                
            [self addChild: spriteBack z:-1];
        }
                         
        //からくりマーク
        if(page == 2 || page == 3 || page == 4 || page == 5 || page == 7 || page == 8 || page == 9 || page == 10 
           || page == 11 || page == 13 || page == 14 || page == 15 || page == 16 || page == 17){
            NSString *path2;
            path2 = [NSString stringWithFormat:@"karakuri.png"];
            CCTexture2D *texture2 = [[CCTextureCache sharedTextureCache] addImage:path2];
            CCSprite *mark = [CCSprite spriteWithTexture:texture2];
            mark.position = ccp(122,722);
            [self addChild: mark z:10];
        }
        
        
        //テキストOFFの時
        if(optionnum3[2] == 1){
            //ページ送り用コントロール
            CCMenuItemImage *maryItem1 = [CCMenuItemImage 
                                          itemFromNormalImage:@"arrow_box_left.pvr.gz" 
                                          selectedImage: @"arrow_box_left.pvr.gz"
                                          target:self
                                          selector:@selector(pressMenuItem:)];
            maryItem1.tag =2;
            
            CCMenuItemImage *maryItem2 = [CCMenuItemImage 
                                          itemFromNormalImage:@"arrow_box_right.pvr.gz" 
                                          selectedImage: @"arrow_box_right.pvr.gz"
                                          target:self
                                          selector:@selector(pressMenuItem:)];
            maryItem2.position = ccp(maryItem2.contentSize.width/2, 140);
            maryItem1.position = ccp(maryItem1.contentSize.width/2-maryItem2.contentSize.width,140);    
            maryItem2.tag =1;
            CCMenu *menu = [CCMenu menuWithItems:maryItem1, maryItem2,nil];
            menu.position = ccp(512,-100);
            [self addChild:menu z:10];
            
            id func =[CCCallFunc actionWithTarget:self selector:@selector(Playnarration)];
            id action2 = [CCSequence actions:[CCDelayTime actionWithDuration:1.0f],func,nil];
            
            [self runAction:action2];

            
            //テキストONの時
        }else{
            
            //テキストの背景
            textBG_Sprite = [CCSprite spriteWithFile:@"text_field.pvr.gz"];
            textBG_Sprite.position = ccp(512, 0-textBG_Sprite.contentSize.height);
            [self addChild:textBG_Sprite z:3];
            
            NSString *text = [SceneManager textForSceneId:sceneId];
            CCLabelTTF *label = [CCLabelTTF labelWithString:text
                                                 dimensions:CGSizeMake(850, 145)
                                                  alignment:UITextAlignmentLeft
                                              lineBreakMode:UILineBreakModeWordWrap
                                                   fontName:@"HiraKakuProN-W3"
                                                   fontSize:20];
            label.color = ccc3(255, 255, 255); 
            label.position =  ccp(520, 115);
            [textBG_Sprite addChild:label z:3];
            
            id move1 = [CCMoveTo actionWithDuration:0.5f position:ccp(512, 110)];
            id func =[CCCallFunc actionWithTarget:self selector:@selector(Playnarration)];
            id ease1 =[CCEaseSineOut actionWithAction:move1];
            id action1 = [CCSequence actions:[CCDelayTime actionWithDuration:1.0f],ease1,nil];
            id action2 = [CCSequence actions:[CCDelayTime actionWithDuration:1.0f],func,nil];
            
            [self runAction:action2];
            [textBG_Sprite runAction:action1];
            
            //ページめくり+homeボタンの生成
            CCMenuItemImage *maryItem1 = [CCMenuItemImage 
                                          itemFromNormalImage:@"nextpage.pvr.gz" 
                                          selectedImage: @"nextpage.pvr.gz"
                                          target:self
                                          selector:@selector(pressMenuItem:)];
            maryItem1.position = ccp(512-maryItem1.contentSize.width/2, -265);
            maryItem1.tag =1;
            
            CCMenuItemImage *maryItem2 = [CCMenuItemImage 
                                          itemFromNormalImage:@"prepage.pvr.gz" 
                                          selectedImage: @"prepage.pvr.gz"
                                          target:self
                                          selector:@selector(pressMenuItem:)];
            maryItem2.position = ccp(-512+maryItem2.contentSize.width/2, -265);
            maryItem2.tag =2;
            
            maryItem3 = [CCMenuItemImage 
                         itemFromNormalImage:@"arrow_down.pvr.gz" 
                         selectedImage: @"arrow_down.pvr.gz"
                         target:self
                         selector:@selector(pressMenuItem:)];
            maryItem3.position = ccp(330+maryItem3.contentSize.width/2, -142);
            maryItem3.tag =3;
            
            maryItem4 = [CCMenuItemImage 
                         itemFromNormalImage:@"arrow_up.pvr.gz" 
                         selectedImage: @"arrow_up.pvr.gz"
                         target:self
                         selector:@selector(pressMenuItem:)];
            maryItem4.position = ccp(330+maryItem4.contentSize.width/2, -142);
            maryItem4.tag =4;
            maryItem4.visible = false;
            
            CCMenu *menu = [CCMenu menuWithItems:maryItem1, maryItem2,maryItem3,maryItem4,nil];
            [textBG_Sprite addChild:menu z:15];
            
        }

        
        //ホームボタン
        CCMenuItemImage *maryItem5 = [CCMenuItemImage 
                                      itemFromNormalImage:@"home.png" 
                                      selectedImage: @"home.png"
                                      target:self
                                      selector:@selector(pressMenuItem:)];
        maryItem5.position = ccp(-460, 340);
        maryItem5.tag =5;
        CCMenu *menu2 = [CCMenu menuWithItems:maryItem5,nil];
        [self addChild:menu2 z:10];
		
        
        //[self PageEffect];
        
        [self PageEffect];
		[self schedule: @selector(tick:)];
	}

	return self;
}

//メニューボタン処理
#pragma mark イベント処理
- (void)pressMenuItem:(id)sender {
    if([sender tag] == 1){
        [[CCDirector sharedDirector] setDepthTest:YES];
        //FadeOut
		[self FadeOut];
        
        if (page == 18) {
            [SoundPlayer bgmStop2:optionnum3[1]];
            /*
            [Movie_Sprite[0] removeFromParentAndCleanup:YES];
            [Movie_Sprite[1] removeFromParentAndCleanup:YES];
            [Movie_Sprite[2] removeFromParentAndCleanup:YES];
            [Movie_Sprite[3] removeFromParentAndCleanup:YES];
            [Movie_Sprite[4] removeFromParentAndCleanup:YES];
            [Movie_Sprite[5] removeFromParentAndCleanup:YES];
             */
            //[Movie_Sprite[6] removeFromParentAndCleanup:YES];
        }

        CCScene *newScene = [SceneManager nextSceneOfScene:self.parent
                                                    choice:kSceneChoiseA
                                                      next:true];
        [CCTransitionHelper replaceScene:newScene transitionType:TransitionTypePageForward duration:0.5];
    }else if([sender tag] == 2 && page > 1){
        [[CCDirector sharedDirector] setDepthTest:YES];
        //FadeOut
        [self FadeOut];
        CCScene *newScene = [SceneManager nextSceneOfScene:self.parent
                                                    choice:kSceneChoiseB
                                                      next:false];
        [CCTransitionHelper replaceScene:newScene transitionType:TransitionTypePageBackward duration:0.5];
        
       

                
    }else if([sender tag] == 3){
        [[CCDirector sharedDirector] setDepthTest:NO];
        
        id move1 = [CCMoveTo actionWithDuration:0.5f position:ccp(512, -80)];
        CCEaseExponentialOut *ease1 =[CCEaseExponentialOut actionWithAction:move1];
        [textBG_Sprite runAction:ease1];
        maryItem3.visible =false;
        maryItem4.visible =true;
        
    }else if([sender tag] == 4){        
        id move1 = [CCMoveTo actionWithDuration:0.5f position:ccp(512, 110)];
        CCEaseExponentialOut *ease1 =[CCEaseExponentialOut actionWithAction:move1];
        [textBG_Sprite runAction:ease1];
        maryItem3.visible =true;
        maryItem4.visible =false;
        
        
    }else{
        UIAlertView *alertView = [[UIAlertView alloc] 
                                  initWithTitle:@"タイトルへ戻りますか？"
                                  message:nil                             
                                  delegate:self
                                  cancelButtonTitle:nil
                                  otherButtonTitles:@"はい",@"いいえ",nil];
        [alertView show];
        [alertView release];
        alertView = nil;
        
        
    }
    
    for(int i = 0; i < 5; i++) {
        [[SimpleAudioEngine sharedEngine] stopEffect:SE_Array[i]]; // 再生終了
    }
    
    // myEffectによる再生終了とリソース解放
    for(int i = 0; i < (int)(sizeof(myEffect) / sizeof(myEffect[0])); i++) {
        if(myEffect[i] != nil) {
            [myEffect[i] stop]; // 再生終了
            [myEffect[i] release];
            myEffect[i] = nil;
        }
    }
    //[self memoryState];
}

// メモリ使用量をログに表示する
-(void)memoryState {
    struct task_basic_info t_info;
    mach_msg_type_number_t t_info_count = TASK_BASIC_INFO_COUNT;
    
    if (task_info(current_task(), TASK_BASIC_INFO, (task_info_t)&t_info, &t_info_count)!= KERN_SUCCESS)
    {
        NSLog(@"%s(): Error in task_info(): %s",
              __FUNCTION__, strerror(errno));
    }
    
    // 物理メモリの使用量(byte) - Activity MonitorのReal Memoryに該当
    u_int rss = t_info.resident_size;
    
    // 仮想メモリの使用量(byte) - Activity MonitorのVirtual Memoryに該当??
    u_int vs = t_info.virtual_size;
    
    NSLog(@"RSS: %u Bytes, VS: %u Bytes.", rss, vs);
}

//タイトルへ戻るアラート
- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            /*
            if(page == 5){
                [Movie_Sprite[2]  removeFromParentAndCleanup:YES];
            }
            */
            
            CDLongAudioSource *player2 = [[CDAudioManager sharedManager] audioSourceForChannel:kASC_Left];
            CDLongAudioSourceFader* fader = [[CDLongAudioSourceFader alloc] init:player2 interpolationType:kIT_Exponential startVal:player2.volume endVal:0.0];
            [fader setStopTargetWhenComplete:YES];
            //Create a property modifier action to wrap the fader
            CDXPropertyModifierAction* action = [CDXPropertyModifierAction actionWithDuration:0.5 modifier:fader];
            [fader release];//Action will retain
            fader = nil;
            id actionCallFuncN = [CCCallFuncN actionWithTarget:self selector:@selector(actionComplete)];
            [[CCActionManager sharedManager] addAction:[CCSequence actions:action,actionCallFuncN, nil] target:player2 paused:NO];
            
            [SoundPlayer bgmStop2:optionnum3[1]];
    }
}

//フェードアウト
-(void)FadeOut{
    CDLongAudioSource *player2 = [[CDAudioManager sharedManager] audioSourceForChannel:kASC_Left];
    CDLongAudioSourceFader* fader = [[CDLongAudioSourceFader alloc] init:player2 interpolationType:kIT_Exponential startVal:player2.volume endVal:0.0];
    [fader setStopTargetWhenComplete:YES];
    //Create a property modifier action to wrap the fader
    CDXPropertyModifierAction* action = [CDXPropertyModifierAction actionWithDuration:0.5 modifier:fader];
    [fader release];//Action will retain
    fader = nil;
    id actionCallFuncN = [CCCallFuncN actionWithTarget:self selector:@selector(actionComplete2)];
    [[CCActionManager sharedManager] addAction:[CCSequence actions:action,actionCallFuncN, nil] target:player2 paused:NO];
}

- (void)actionComplete{
    [[CCDirector sharedDirector] setDepthTest:YES];

    CCScene *newScene = [SceneManager nextSceneOfScene:self.parent
                                                choice:kSceneChoiseC next:true];
    [CCTransitionHelper replaceScene:newScene transitionType:TransitionTypePageBackward duration:0.5];
    
}

- (void)actionComplete2{
    [player stop];
    soundFlag = false;
}
//ナレーション
-(void)Playnarration{
    //ipad2バグ調整
    if(page == 1){
        [[CCDirector sharedDirector] setDepthTest:NO];
    }
    if(setteino[0] == 0){
        NSString *narration = [[NSString alloc] initWithFormat:@"scene%d.mp3",page];
        player = [[CDAudioManager sharedManager] audioSourceForChannel:kASC_Left];
        [player load:narration];
        player.volume = 1.0f;
        [player setNumberOfLoops:0];
        [player play];
        soundFlag = true;
        [narration release];
        narration = nil;
    }
}

#pragma mark ギミック
-(void)PageEffect{
    // ちらつき防止
    [self zbuffer:NO];

    if (page == 1) {
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"gama01.plist" textureFile:@"gama01.pvr.gz"];
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"gama01.pvr.gz"];
        [self addChild:spriteSheet];
        Movie_Sprite[0] = [CCSprite spriteWithSpriteFrameName:@"gama01_01.png"];
        Movie_Sprite[1] = [CCSprite spriteWithSpriteFrameName:@"gama01_02.png"];
        
        
        Movie_Sprite[0].position = ccp(1416, 390);
        Movie_Sprite[1].position = ccp(516, 390);
        
        //particles[1] = [CCParticleSystemQuad particleWithFile:@"gama02_particle_water_tween.plist"];
        particles[1] = [CCParticleSystemQuad particleWithFile:@"gama02_particle.plist"];
        particles[1].autoRemoveOnFinish = YES;
        [self addChild:particles[1]];
        
        [self addChild:Movie_Sprite[1]];
        [self addChild:Movie_Sprite[0]];
        [self scene1];
    } else if (page == 2) {
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"gama02.plist" textureFile:@"gama02.pvr.gz"];
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"gama02.pvr.gz"];
        [self addChild:spriteSheet];
        
        for (iCounter = 0; iCounter <= 6; iCounter++) {
            NSString *path;
            path = [NSString stringWithFormat:@"gama02_0%d.png", (iCounter + 1)];
            Movie_Sprite[iCounter] = [CCSprite spriteWithSpriteFrameName:path];
        }
        
        Movie_Sprite[0].position = ccp(1150,  710); // gama02_01.png
        Movie_Sprite[1].position = ccp(-320,  700);
        Movie_Sprite[2].position = ccp(190, -550);
        Movie_Sprite[3].position = ccp(655, -450);
        Movie_Sprite[4].position = ccp(510, -630);
        Movie_Sprite[5].position = ccp(815, -460);
        Movie_Sprite[6].position = ccp(420, -440);
        
        Movie_Sprite[0].anchorPoint = CGPointMake(1.0f, 0.5f);
        Movie_Sprite[1].anchorPoint = CGPointMake(-0.5f, 0.5f);
        Movie_Sprite[2].anchorPoint = CGPointMake(0.5f, -0.5f);
        Movie_Sprite[3].anchorPoint = CGPointMake(0.5f, -0.5f);
        Movie_Sprite[4].anchorPoint = CGPointMake(0.5f, -0.5f);
        Movie_Sprite[5].anchorPoint = CGPointMake(0.5f, -0.5f);
        Movie_Sprite[6].anchorPoint = CGPointMake(0.5f, -0.5f);
        
        
        //レイヤーの奥から順に配置していく
        for (iCounter = 6; iCounter >= 0; iCounter--) {
            [self addChild:Movie_Sprite[iCounter]];
        }
        touchCount = 0;
        touchFlag = true;
        touchMoveFlag = true;
    } else if(page == 3) {
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"gama03-hd.plist" textureFile:@"gama03-hd.pvr.gz"];
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"gama03-hd.pvr.gz"];
        [self addChild:spriteSheet];
        
        for (iCounter = 0; iCounter <= 8; iCounter++) {
            NSString *path;
            path = [NSString stringWithFormat:@"gama03_0%d.png", (iCounter + 1)];
            Movie_Sprite[iCounter] = [CCSprite spriteWithSpriteFrameName:path];
        }
        // ファイル名が010になってしまうので個別に追加
        Movie_Sprite[9] = [CCSprite spriteWithSpriteFrameName:@"gama03_10.png"];
        Movie_Sprite[10] = [CCSprite spriteWithSpriteFrameName:@"gama03_11.png"];
        Movie_Sprite[11] = [CCSprite spriteWithSpriteFrameName:@"gama03_12.png"];
        Movie_Sprite[0].position = ccp(240, 140);
        Movie_Sprite[1].position = ccp(750, 140);
        Movie_Sprite[2].position = ccp(513, 390);
        Movie_Sprite[3].position = ccp(700, 250);
        Movie_Sprite[4].position = ccp(320, 250);
        Movie_Sprite[5].position = ccp(525, 250);
        Movie_Sprite[6].position = ccp(513, 390);
        //Movie_Sprite[7].position = ccp(130, 350); // animationでアンカーポイントを変更するので下で設定
        Movie_Sprite[8].position = ccp(125, 350);
        Movie_Sprite[9].position = ccp(350, 340);
        Movie_Sprite[10].position = ccp(513, 390);
        Movie_Sprite[11].position = ccp(513, 390);
        
        for (iCounter = 11; iCounter >= 0; iCounter--) {
            [self addChild:Movie_Sprite[iCounter]];
        }
        
        touchCount = 0;
        touchFlag = true;
        
        [self scene3]; // 石を運ぶ人の動き
        [self scene3_1]; // 木こりの動き
    } else if (page == 4) {
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"gama04.plist" textureFile:@"gama04.pvr.gz"];
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"gama04.pvr.gz"];
        [self addChild:spriteSheet];
        
        Movie_Sprite[0] = [CCSprite spriteWithSpriteFrameName:@"gama04_01.png"];
        Movie_Sprite[1] = [CCSprite spriteWithSpriteFrameName:@"gama04_02.png"];
        Movie_Sprite[2] = [CCSprite spriteWithSpriteFrameName:@"gama04_03.png"];
        Movie_Sprite[3] = [CCSprite spriteWithSpriteFrameName:@"gama04_04.png"];
        Movie_Sprite[4] = [CCSprite spriteWithSpriteFrameName:@"gama04_05.png"];
        
        Movie_Sprite[0].position = ccp(247.0f, 274.0f);
        Movie_Sprite[1].position = ccp(696.0f, 256.0f);
        Movie_Sprite[2].position = ccp(814.0f, 367.0f);
        //Movie_Sprite[3].position = ccp(585.0f, 187.0f);
        Movie_Sprite[3].anchorPoint = CGPointMake(0.2f, 0.5f);
        Movie_Sprite[3].position = ccp(505.0f, 180.0f);
        Movie_Sprite[4].position = ccp(362.0f, 114.0f);
        [self addChild:Movie_Sprite[0]];
        [self addChild:Movie_Sprite[1]];
        [self addChild:Movie_Sprite[2]];
        [self addChild:Movie_Sprite[3]];
        [self addChild:Movie_Sprite[4]];
        touchCount = 5;
        touchFlag = true;//琵琶
    } else if (page == 5) {
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"gama05-1-hd.plist" textureFile:@"gama05-1-hd.pvr.gz"];
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"gama05-1-hd.pvr.gz"];
        [self addChild:spriteSheet];
        [frameCache addSpriteFramesWithFile:@"gama05-2-hd.plist" textureFile:@"gama05-2-hd.pvr.gz"];
        spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"gama05-2-hd.pvr.gz"];
        [self addChild:spriteSheet];
        
        Movie_Sprite[0] = [CCSprite spriteWithSpriteFrameName:@"gama05_01.png"];
        Movie_Sprite[1] = [CCSprite spriteWithSpriteFrameName:@"gama05_02.png"];
        Movie_Sprite[2] = [CCSprite spriteWithSpriteFrameName:@"gama05_03.png"];
        Movie_Sprite[3] = [CCSprite spriteWithSpriteFrameName:@"gama05_04.png"];
        Movie_Sprite[4] = [CCSprite spriteWithSpriteFrameName:@"gama05_05.png"];
        Movie_Sprite[5] = [CCSprite spriteWithSpriteFrameName:@"gama05_06.png"];
        Movie_Sprite[6] = [CCSprite spriteWithSpriteFrameName:@"gama05_07.png"];
        Movie_Sprite[7] = [CCSprite spriteWithSpriteFrameName:@"gama05_08.png"];
        Movie_Sprite[8] = [CCSprite spriteWithSpriteFrameName:@"gama05_09.png"];
        Movie_Sprite[9] = [CCSprite spriteWithSpriteFrameName:@"gama05_10.png"];
        Movie_Sprite[10] = [CCSprite spriteWithSpriteFrameName:@"gama05_11.png"];
        Movie_Sprite[11] = [CCSprite spriteWithSpriteFrameName:@"gama05_12.png"];
        Movie_Sprite[12] = [CCSprite spriteWithSpriteFrameName:@"gama05_13.png"];
        
        Movie_Sprite[0].position = ccp(135.0f,-284.0f);
        Movie_Sprite[1].position = ccp(949.0f,-320.0f);
        Movie_Sprite[2].position = ccp(323.0f, 217.0f);
        Movie_Sprite[3].position = ccp(786.0f, 142.0f);
        Movie_Sprite[4].position = ccp(468.0f, 150.0f);
        Movie_Sprite[5].position = ccp(512.0f, 465.0f);
        Movie_Sprite[6].position = ccp(516.0f, 182.0f);
        Movie_Sprite[7].position = ccp(523.0f, 279.0f);
        Movie_Sprite[8].position = ccp(500.0f, 405.0f);
        Movie_Sprite[9].position = ccp(526.0f, 289.0f);
        Movie_Sprite[10].position = ccp(528.0f, 334.0f);
        Movie_Sprite[11].position = ccp(511.0f, 533.0f);
        Movie_Sprite[12].position = ccp(511.0f, 644.0f);
        
        Movie_Sprite[0].anchorPoint = CGPointMake(0.5f, -0.5f);
        Movie_Sprite[1].anchorPoint = CGPointMake(0.5f, -0.5f);
        
        [self addChild:Movie_Sprite[12]];
        [self addChild:Movie_Sprite[11]];
        [self addChild:Movie_Sprite[10]];
        [self addChild:Movie_Sprite[9]];
        [self addChild:Movie_Sprite[5]];
        [self addChild:Movie_Sprite[8]];
        [self addChild:Movie_Sprite[7]];
        [self addChild:Movie_Sprite[6]];
        [self addChild:Movie_Sprite[4]];
        [self addChild:Movie_Sprite[2]];
        [self addChild:Movie_Sprite[3]];
        [self addChild:Movie_Sprite[0]];
        [self addChild:Movie_Sprite[1]];
        [self scene5_1];
        
        touchFlag = true;
        

    } else if (page == 6) {
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"gama06-hd.plist" textureFile:@"gama06-hd.pvr.gz"];
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"gama06-hd.pvr.gz"];
        [self addChild:spriteSheet];
        
        frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"gama06_sora-hd.plist" textureFile:@"gama06_sora-hd.pvr.gz"];
        spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"gama06_sora-hd.pvr.gz"];
        [self addChild:spriteSheet];
        
        Movie_Sprite[0] = [CCSprite spriteWithSpriteFrameName:@"gama06_01.png"];
        Movie_Sprite[1] = [CCSprite spriteWithSpriteFrameName:@"gama06_02.png"];
        Movie_Sprite[2] = [CCSprite spriteWithSpriteFrameName:@"gama06_03.png"];
        Movie_Sprite[3] = [CCSprite spriteWithSpriteFrameName:@"gama06_04.png"];
        Movie_Sprite[4] = [CCSprite spriteWithSpriteFrameName:@"gama06_05.png"];
        Movie_Sprite[5] = [CCSprite spriteWithSpriteFrameName:@"gama06_06.png"];
        Movie_Sprite[6] = [CCSprite spriteWithSpriteFrameName:@"gama06_07.png"];
        Movie_Sprite[7] = [CCSprite spriteWithSpriteFrameName:@"gama06_sora.png"];
        
        
        Movie_Sprite[0].position = ccp(300.0f, 500.0f);
        Movie_Sprite[1].position = ccp(675.0f, 225.0f);
        Movie_Sprite[2].position = ccp(500.0f, 60.0f);
        Movie_Sprite[3].position = ccp(500.0f, 90.0f);
        Movie_Sprite[4].position = ccp(500.0f, 125.0f);
        Movie_Sprite[5].position = ccp(500.0f, 195.0f);
        Movie_Sprite[6].position = ccp(500.0f, 165.0f);
        Movie_Sprite[7].position = ccp(520.0f, 525.0f);
        
        [self addChild:Movie_Sprite[7]];
        [self addChild:Movie_Sprite[6]];
        [self addChild:Movie_Sprite[5]];
        [self addChild:Movie_Sprite[4]];
        [self addChild:Movie_Sprite[3]];
        [self addChild:Movie_Sprite[2]];
        [self addChild:Movie_Sprite[1]];
        [self addChild:Movie_Sprite[0]];
        
        [self scene6_1];
        [self scene6_2];
    } else if (page == 7) {
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"gama07-hd.plist" textureFile:@"gama07-hd.pvr.gz"];
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"gama07-hd.pvr.gz"];
        [self addChild:spriteSheet];
        
        Movie_Sprite[0] = [CCSprite spriteWithSpriteFrameName:@"gama07_01.png"];
        Movie_Sprite[1] = [CCSprite spriteWithSpriteFrameName:@"gama07_02.png"];
        Movie_Sprite[2] = [CCSprite spriteWithSpriteFrameName:@"gama07_03.png"];
        Movie_Sprite[3] = [CCSprite spriteWithSpriteFrameName:@"gama07_04.png"];
        Movie_Sprite[4] = [CCSprite spriteWithSpriteFrameName:@"gama07_05.png"];
        Movie_Sprite[5] = [CCSprite spriteWithSpriteFrameName:@"gama07_06.png"];
        Movie_Sprite[6] = [CCSprite spriteWithSpriteFrameName:@"gama07_07.png"];
        Movie_Sprite[7] = [CCSprite spriteWithSpriteFrameName:@"gama07_08.png"];
        Movie_Sprite[8] = [CCSprite spriteWithSpriteFrameName:@"gama07_09.png"];
        Movie_Sprite[9] = [CCSprite spriteWithSpriteFrameName:@"gama07_10.png"];
        Movie_Sprite[10] = [CCSprite spriteWithSpriteFrameName:@"gama07_11.png"];
        Movie_Sprite[11] = [CCSprite spriteWithSpriteFrameName:@"gama07_12.png"];
        Movie_Sprite[12] = [CCSprite spriteWithSpriteFrameName:@"gama07_13.png"];
        Movie_Sprite[13] = [CCSprite spriteWithSpriteFrameName:@"gama07_14.png"];
        Movie_Sprite[14] = [CCSprite spriteWithSpriteFrameName:@"gama07_15.png"];
        Movie_Sprite[15] = [CCSprite spriteWithSpriteFrameName:@"gama07_16.png"];
        Movie_Sprite[16] = [CCSprite spriteWithSpriteFrameName:@"gama07_17.png"];
        Movie_Sprite[17] = [CCSprite spriteWithSpriteFrameName:@"gama07_18.png"];
        Movie_Sprite[18] = [CCSprite spriteWithSpriteFrameName:@"gama07_19.png"];
        Movie_Sprite[19] = [CCSprite spriteWithSpriteFrameName:@"gama07_20.png"];
        Movie_Sprite[20] = [CCSprite spriteWithSpriteFrameName:@"gama07_21.png"];
        Movie_Sprite[21] = [CCSprite spriteWithSpriteFrameName:@"gama07_22.png"];
        Movie_Sprite[22] = [CCSprite spriteWithSpriteFrameName:@"gama07_23.png"];
        Movie_Sprite[23] = [CCSprite spriteWithSpriteFrameName:@"gama07_24.png"];
        Movie_Sprite[24] = [CCSprite spriteWithSpriteFrameName:@"gama07_25.png"];
        Movie_Sprite[25] = [CCSprite spriteWithSpriteFrameName:@"gama07_26.png"];
        Movie_Sprite[26] = [CCSprite spriteWithSpriteFrameName:@"gama07_27.png"];
        Movie_Sprite[27] = [CCSprite spriteWithSpriteFrameName:@"gama07_28.png"];
        Movie_Sprite[28] = [CCSprite spriteWithSpriteFrameName:@"gama07_29.png"];
        
        Movie_Sprite[0].anchorPoint = CGPointMake(0.5f, 0.9f);
        Movie_Sprite[17].anchorPoint = CGPointMake(0.1f, 0.2f);
        Movie_Sprite[18].anchorPoint = CGPointMake(0.9f, 0.2f);
        
        Movie_Sprite[0].position = ccp(393.0f, 612.0f); // 中央魚
        Movie_Sprite[1].position = ccp(-500.0f, -500.0f);
        Movie_Sprite[2].position = ccp(-500.0f, -500.0f);
        Movie_Sprite[3].position = ccp(300.0f, 120.0f);
        Movie_Sprite[4].position = ccp(880.0f, 100.0f);
        Movie_Sprite[5].position = ccp(848.0f, 351.0f);
        Movie_Sprite[6].position = ccp(809.0f, 284.0f);
        Movie_Sprite[7].position = ccp(775.0f, 343.0f);
        Movie_Sprite[8].position = ccp(905.0f, 150.0f);
        Movie_Sprite[9].position = ccp(878.0f, 157.0f);
        Movie_Sprite[10].position = ccp(967.0f, 122.0f);
        Movie_Sprite[11].position = ccp(262.0f, 154.0f);
        Movie_Sprite[12].position = ccp(66.0f, 242.0f);
        Movie_Sprite[13].position = ccp(178.0f, 181.0f);
        Movie_Sprite[14].position = ccp(275.0f, 163.0f);
        Movie_Sprite[15].position = ccp(125.0f, 241.0f);
        Movie_Sprite[16].position = ccp(512.0f, 429.0f);
        Movie_Sprite[17].position = ccp(534.0f, 463.0f); // 与吉の右手
        Movie_Sprite[18].position = ccp(519.0f, 444.0f); // 与吉の左手
        Movie_Sprite[19].position = ccp(512.0f, 384.0f);
        Movie_Sprite[20].position = ccp(512.0f, 209.0f);
        
        [self addChild:Movie_Sprite[20]];
        [self addChild:Movie_Sprite[19]];
        [self addChild:Movie_Sprite[18]];
        [self addChild:Movie_Sprite[17]];
        [self addChild:Movie_Sprite[16]];
        [self addChild:Movie_Sprite[15]];
        [self addChild:Movie_Sprite[14]];
        [self addChild:Movie_Sprite[13]];
        [self addChild:Movie_Sprite[12]];
        [self addChild:Movie_Sprite[11]];
        [self addChild:Movie_Sprite[10]];
        [self addChild:Movie_Sprite[9]];
        [self addChild:Movie_Sprite[8]];
        [self addChild:Movie_Sprite[7]];
        [self addChild:Movie_Sprite[6]];
        [self addChild:Movie_Sprite[5]];
        [self addChild:Movie_Sprite[4]];
        [self addChild:Movie_Sprite[3]];
        [self addChild:Movie_Sprite[2]];
        [self addChild:Movie_Sprite[1]];
        [self addChild:Movie_Sprite[0]];
        
        for(iCounter = 0;iCounter < 20; iCounter++) {
            Fish_Sprite[iCounter] = [CCSprite spriteWithSpriteFrameName:@"gama07_02.png"];
            Fish_Sprite[iCounter].position = ccp(-500, -500);
            [self addChild:Fish_Sprite[iCounter]];
        }
        
        // レア魚
        Movie_Sprite[21].position = ccp(-500.0f, -500.0f);
        Movie_Sprite[22].position = ccp(-500.0f, -500.0f);
        Movie_Sprite[23].position = ccp(-500.0f, -500.0f);
        Movie_Sprite[24].position = ccp(-500.0f, -500.0f);
        Movie_Sprite[25].position = ccp(-500.0f, -500.0f);
        Movie_Sprite[26].position = ccp(-500.0f, -500.0f);
        Movie_Sprite[27].position = ccp(-500.0f, -500.0f);
        Movie_Sprite[28].position = ccp(-500.0f, -500.0f);
        
        [self addChild:Movie_Sprite[21]];
        [self addChild:Movie_Sprite[22]];
        [self addChild:Movie_Sprite[23]];
        [self addChild:Movie_Sprite[24]];
        [self addChild:Movie_Sprite[25]];
        [self addChild:Movie_Sprite[26]];
        [self addChild:Movie_Sprite[27]];
        [self addChild:Movie_Sprite[28]];
        
        touchFlag = true;
        
        [self scene7];
        [self scene7_1];
        [self scene7_2];
        
        touchCount = 0;
    } else if (page == 8) {
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"gama08_1-hd.plist" textureFile:@"gama08_1-hd.pvr.gz"];
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"gama08_1-hd.pvr.gz"];
        [self addChild:spriteSheet];
        
        frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"gama08_2-hd.plist" textureFile:@"gama08_2-hd.pvr.gz"];
        spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"gama08_2-hd.pvr.gz"];
        [self addChild:spriteSheet];
        
        Movie_Sprite[0] = [CCSprite spriteWithSpriteFrameName:@"gama08_01.png"];
        Movie_Sprite[1] = [CCSprite spriteWithSpriteFrameName:@"gama08_02.png"];
        Movie_Sprite[2] = [CCSprite spriteWithSpriteFrameName:@"gama08_03.png"];
        Movie_Sprite[3] = [CCSprite spriteWithSpriteFrameName:@"gama08_04.png"];
        Movie_Sprite[4] = [CCSprite spriteWithSpriteFrameName:@"gama08_05.png"];
        Movie_Sprite[5] = [CCSprite spriteWithSpriteFrameName:@"gama08_06.png"];
        Movie_Sprite[6] = [CCSprite spriteWithSpriteFrameName:@"gama08_07.png"];
        Movie_Sprite[7] = [CCSprite spriteWithSpriteFrameName:@"gama08_08.png"];
        Movie_Sprite[8] = [CCSprite spriteWithSpriteFrameName:@"gama08_09.png"];
        Movie_Sprite[9] = [CCSprite spriteWithSpriteFrameName:@"gama08_10.png"];
        Movie_Sprite[10] = [CCSprite spriteWithSpriteFrameName:@"gama08_11.png"];
        Movie_Sprite[11] = [CCSprite spriteWithSpriteFrameName:@"gama08_12.png"];
        Movie_Sprite[12] = [CCSprite spriteWithSpriteFrameName:@"gama08_13.png"];
        Movie_Sprite[13] = [CCSprite spriteWithSpriteFrameName:@"gama08_14.png"];
        Movie_Sprite[14] = [CCSprite spriteWithSpriteFrameName:@"gama08_15.png"];
        Movie_Sprite[15] = [CCSprite spriteWithSpriteFrameName:@"gama08_16.png"];
        Movie_Sprite[16] = [CCSprite spriteWithSpriteFrameName:@"gama08_17.png"];
        
        Movie_Sprite[0].position = ccp(5900.0f, 3360.0f); // ぐおー
        Movie_Sprite[1].position = ccp(5900.0f, 3360.0f); // ひゃひゃ
        Movie_Sprite[2].position = ccp(2880.0f, 3600.0f); // くすぐったい
        Movie_Sprite[3].position = ccp(590.0f, 336.0f);
        Movie_Sprite[4].position = ccp(288.0f, 360.0f);
        Movie_Sprite[5].position = ccp(512.0f, 137.0f);
        Movie_Sprite[6].position = ccp(716.0f, 300.0f);
        Movie_Sprite[7].position = ccp(522.0f, 235.0f);
        Movie_Sprite[8].position = ccp(372.0f, 334.0f);
        Movie_Sprite[9].position = ccp(512.0f, 254.0f);
        Movie_Sprite[10].position = ccp(510.0f, 314.0f);
        Movie_Sprite[11].position = ccp(512.0f, 297.0f);
        Movie_Sprite[12].position = ccp(502.0f, 322.0f);
        Movie_Sprite[13].position = ccp(512.0f, 352.0f);
        Movie_Sprite[14].position = ccp(677.0f, 432.0f);
        Movie_Sprite[15].position = ccp(512.0f, 417.0f);
        Movie_Sprite[16].position = ccp(512.0f, 562.0f);
        
        [self addChild:Movie_Sprite[16]];
        [self addChild:Movie_Sprite[15]];
        [self addChild:Movie_Sprite[14]];
        [self addChild:Movie_Sprite[13]];
        [self addChild:Movie_Sprite[12]];
        [self addChild:Movie_Sprite[11]];
        [self addChild:Movie_Sprite[10]];
        [self addChild:Movie_Sprite[9]];
        [self addChild:Movie_Sprite[8]];
        [self addChild:Movie_Sprite[7]];
        [self addChild:Movie_Sprite[6]];
        [self addChild:Movie_Sprite[5]];
        [self addChild:Movie_Sprite[4]];
        [self addChild:Movie_Sprite[3]];
        [self addChild:Movie_Sprite[2]];
        [self addChild:Movie_Sprite[1]];
        [self addChild:Movie_Sprite[0]];
        
        touchFlag = true;
        
        [self scene8_1];

    } else if (page == 9) {
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"gama09-hd.plist" textureFile:@"gama09-hd.pvr.gz"];
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"gama09-hd.pvr.gz"];
        [self addChild:spriteSheet];
        
        Movie_Sprite[0] = [CCSprite spriteWithSpriteFrameName:@"gama09_01.png"];
        Movie_Sprite[1] = [CCSprite spriteWithSpriteFrameName:@"gama09_02.png"];
        Movie_Sprite[2] = [CCSprite spriteWithSpriteFrameName:@"gama09_03.png"];
        Movie_Sprite[3] = [CCSprite spriteWithSpriteFrameName:@"gama09_04.png"];
        Movie_Sprite[4] = [CCSprite spriteWithSpriteFrameName:@"gama09_05.png"];
        Movie_Sprite[5] = [CCSprite spriteWithSpriteFrameName:@"gama09_06.png"];
        Movie_Sprite[6] = [CCSprite spriteWithSpriteFrameName:@"gama09_07.png"];
        Movie_Sprite[7] = [CCSprite spriteWithSpriteFrameName:@"gama09_08.png"];
        Movie_Sprite[8] = [CCSprite spriteWithSpriteFrameName:@"gama09_09.png"];
        Movie_Sprite[9] = [CCSprite spriteWithSpriteFrameName:@"gama09_10.png"];
        Movie_Sprite[10] = [CCSprite spriteWithSpriteFrameName:@"gama09_11.png"];
        Movie_Sprite[11] = [CCSprite spriteWithSpriteFrameName:@"gama09_12.png"];
        
        Movie_Sprite[0].position = ccp(265.0f, 193.0f);
        Movie_Sprite[1].position = ccp(595.0f, 199.0f);
        Movie_Sprite[2].position = ccp(450.0f, 321.0f);
        Movie_Sprite[3].position = ccp(666.0f, 365.0f);
        Movie_Sprite[4].position = ccp(670.0f, 299.0f);
        Movie_Sprite[5].position = ccp(799.0f, 209.0f);
        Movie_Sprite[6].position = ccp(293.0f, 321.0f);
        Movie_Sprite[7].position = ccp(511.0f, 382.0f);
        Movie_Sprite[8].position = ccp(512.0f, 184.0f);
        Movie_Sprite[9].position = ccp(219.0f, 526.0f);
        Movie_Sprite[10].position = ccp(470.0f, 640.0f);
        Movie_Sprite[11].position = ccp(790.0f, 584.0f);
        
        [self addChild:Movie_Sprite[11]];
        [self addChild:Movie_Sprite[9]];
        [self addChild:Movie_Sprite[8]];
        [self addChild:Movie_Sprite[7]];
        [self addChild:Movie_Sprite[6]];
        [self addChild:Movie_Sprite[5]];
        [self addChild:Movie_Sprite[4]];
        [self addChild:Movie_Sprite[3]];
        [self addChild:Movie_Sprite[2]];
        [self addChild:Movie_Sprite[1]];
        [self addChild:Movie_Sprite[0]];
        [self addChild:Movie_Sprite[10]];
        
        [Movie_Sprite[10] runAction:[CCHide action]]; //ぐおー非表示
        touchFlag = true;
        
        [self addNewSpriteWithCoords];
    } else if (page == 10) {
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"gama10-1-hd.plist" textureFile:@"gama10-1-hd.pvr.gz"];
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"gama10-1-hd.pvr.gz"];
        [self addChild:spriteSheet];
        frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"gama10-2-hd.plist" textureFile:@"gama10-2-hd.pvr.gz"];
        spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"gama10-2-hd.pvr.gz"];
        [self addChild:spriteSheet];
        frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"gama10_tori-hd.plist" textureFile:@"gama10_tori-hd.pvr.gz"];
        spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"gama10_tori-hd.pvr.gz"];
        [self addChild:spriteSheet];
        
        Movie_Sprite[0] = [CCSprite spriteWithSpriteFrameName:@"gama10_01.png"];
        Movie_Sprite[1] = [CCSprite spriteWithSpriteFrameName:@"gama10_02.png"];
        Movie_Sprite[2] = [CCSprite spriteWithSpriteFrameName:@"gama10_03.png"];
        Movie_Sprite[3] = [CCSprite spriteWithSpriteFrameName:@"gama10_04.png"];
        Movie_Sprite[4] = [CCSprite spriteWithSpriteFrameName:@"gama10_05.png"];
        Movie_Sprite[5] = [CCSprite spriteWithSpriteFrameName:@"gama10_06.png"];
        //Movie_Sprite[6] = [CCSprite spriteWithSpriteFrameName:@"gama10_07.png"];        //とり(小)
        //Movie_Sprite[7] = [CCSprite spriteWithSpriteFrameName:@"gama10_08.png"];        //とり(中)
        //Movie_Sprite[8] = [CCSprite spriteWithSpriteFrameName:@"gama10_09.png"];        //とり(大)
        Movie_Sprite[6] = [CCSprite spriteWithSpriteFrameName:@"gama10_tori_sm01.png"];   //ここからとり(小)
        Movie_Sprite[7] = [CCSprite spriteWithSpriteFrameName:@"gama10_tori_sm02.png"];
        Movie_Sprite[8] = [CCSprite spriteWithSpriteFrameName:@"gama10_tori_sm03.png"];
        Movie_Sprite[9] = [CCSprite spriteWithSpriteFrameName:@"gama10_tori_sm04.png"];
        Movie_Sprite[10] = [CCSprite spriteWithSpriteFrameName:@"gama10_tori_sm05.png"];
        Movie_Sprite[11] = [CCSprite spriteWithSpriteFrameName:@"gama10_tori_sm06.png"];
        Movie_Sprite[12] = [CCSprite spriteWithSpriteFrameName:@"gama10_tori_sm07.png"];    //ここまでとり(小)
        Movie_Sprite[13] = [CCSprite spriteWithSpriteFrameName:@"gama10_tori_mid01.png"];   //ここからとり(中)
        Movie_Sprite[14] = [CCSprite spriteWithSpriteFrameName:@"gama10_tori_mid02.png"];
        Movie_Sprite[15] = [CCSprite spriteWithSpriteFrameName:@"gama10_tori_mid03.png"];
        Movie_Sprite[16] = [CCSprite spriteWithSpriteFrameName:@"gama10_tori_mid04.png"];
        Movie_Sprite[17] = [CCSprite spriteWithSpriteFrameName:@"gama10_tori_mid05.png"];
        Movie_Sprite[18] = [CCSprite spriteWithSpriteFrameName:@"gama10_tori_mid06.png"];
        Movie_Sprite[19] = [CCSprite spriteWithSpriteFrameName:@"gama10_tori_mid07.png"];   //ここまでとり(中)
        Movie_Sprite[20] = [CCSprite spriteWithSpriteFrameName:@"gama10_tori_big01.png"];   //ここからとり(大)
        Movie_Sprite[21] = [CCSprite spriteWithSpriteFrameName:@"gama10_tori_big02.png"];
        Movie_Sprite[22] = [CCSprite spriteWithSpriteFrameName:@"gama10_tori_big03.png"];
        Movie_Sprite[23] = [CCSprite spriteWithSpriteFrameName:@"gama10_tori_big04.png"];
        Movie_Sprite[24] = [CCSprite spriteWithSpriteFrameName:@"gama10_tori_big05.png"];
        Movie_Sprite[25] = [CCSprite spriteWithSpriteFrameName:@"gama10_tori_big06.png"];
        Movie_Sprite[26] = [CCSprite spriteWithSpriteFrameName:@"gama10_tori_big07.png"];   //ここまでとり(大)
        Movie_Sprite[27] = [CCSprite spriteWithSpriteFrameName:@"gama10_07.png"];
        Movie_Sprite[28] = [CCSprite spriteWithSpriteFrameName:@"gama10_08.png"];
        Movie_Sprite[29] = [CCSprite spriteWithSpriteFrameName:@"gama10_09.png"];
        Movie_Sprite[30] = [CCSprite spriteWithSpriteFrameName:@"gama10_10.png"];
        
        Movie_Sprite[0].position = ccp(10000.0f, 100000.0f);        //ぐおー
        Movie_Sprite[1].position = ccp(483.0f, 110.0f);
        Movie_Sprite[2].position = ccp(516.0f, 303.0f);
        Movie_Sprite[3].position = ccp(496.0f, 399.0f);
        Movie_Sprite[4].position = ccp(699.0f, 269.0f);
        Movie_Sprite[5].position = ccp(542.0f, 336.0f);
        //Movie_Sprite[6].position = ccp(6750.0f, 3850.0f);       //とり
        //Movie_Sprite[7].position = ccp(2170.0f, 4680.0f);       //とり
        //Movie_Sprite[8].position = ccp(2520.0f, 3600.0f);       //とり(大)
        Movie_Sprite[6].position = ccp(6790.0f, 4640.0f);           //ここからとり(小)
        Movie_Sprite[7].position = ccp(6500.0f, 4400.0f);
        Movie_Sprite[8].position = ccp(7080.0f, 4470.0f);
        Movie_Sprite[9].position = ccp(6870.0f, 4300.0f);
        Movie_Sprite[10].position = ccp(6610.0f, 4140.0f);
        Movie_Sprite[11].position = ccp(6960.0f, 4070.0f);
        Movie_Sprite[12].position = ccp(6740.0f, 3930.0f);         //ここまでとり(小)
        //Movie_Sprite[13].position = ccp(7320.0f, 4900.0f);           //ここからとり(中)
        Movie_Sprite[14].position = ccp(6770.0f, 4580.0f);
        Movie_Sprite[15].position = ccp(8030.0f, 4730.0f);
        Movie_Sprite[16].position = ccp(7440.0f, 4540.0f);
        Movie_Sprite[17].position = ccp(7030.0f, 4240.0f);
        Movie_Sprite[18].position = ccp(7780.0f, 4280.0f);
        Movie_Sprite[19].position = ccp(7620.0f, 4040.0f);          //ここまでとり(中)
        Movie_Sprite[20].position = ccp(6990.0f, 5010.0f);          //ここからとり(大)
        Movie_Sprite[21].position = ccp(5840.0f, 4380.0f);
        Movie_Sprite[22].position = ccp(8240.0f, 4720.0f);
        Movie_Sprite[23].position = ccp(7260.0f, 4300.0f);
        Movie_Sprite[24].position = ccp(7900.0f, 3940.0f);
        Movie_Sprite[25].position = ccp(6750.0f, 3920.0f);
        Movie_Sprite[26].position = ccp(7640.0f, 3420.0f);          //ここまでとり(大)
        Movie_Sprite[27].position = ccp(215.0f, 320.0f);
        Movie_Sprite[28].position = ccp(415.0f, 352.0f);
        Movie_Sprite[29].position = ccp(476.0f, 355.0f);
        Movie_Sprite[30].position = ccp(508.0f, 317.0f);
        
        [self addChild:Movie_Sprite[30]];
        [self addChild:Movie_Sprite[29]];
        [self addChild:Movie_Sprite[28]];
        [self addChild:Movie_Sprite[27]];
        [self addChild:Movie_Sprite[5]];
        [self addChild:Movie_Sprite[4]];
        [self addChild:Movie_Sprite[3]];
        [self addChild:Movie_Sprite[19]];                        //とり(中)はここから
        [self addChild:Movie_Sprite[18]];
        [self addChild:Movie_Sprite[17]];
        [self addChild:Movie_Sprite[16]];
        [self addChild:Movie_Sprite[15]];
        [self addChild:Movie_Sprite[14]];
        [self addChild:Movie_Sprite[13]];                       //ここまでとり(中)
        [self addChild:Movie_Sprite[12]];                         //とり(小)はここから
        [self addChild:Movie_Sprite[11]];
        [self addChild:Movie_Sprite[10]];
        [self addChild:Movie_Sprite[9]];
        [self addChild:Movie_Sprite[8]];
        [self addChild:Movie_Sprite[7]];
        [self addChild:Movie_Sprite[6]];                          //ここまでとり(小)
        [self addChild:Movie_Sprite[26]];                        //とり(大)はここから
        [self addChild:Movie_Sprite[25]];
        [self addChild:Movie_Sprite[24]];
        [self addChild:Movie_Sprite[23]];
        [self addChild:Movie_Sprite[22]];
        [self addChild:Movie_Sprite[21]];
        [self addChild:Movie_Sprite[20]];                       //ここまでとり(大)
        [self addChild:Movie_Sprite[2]];                            
        [self addChild:Movie_Sprite[1]];
        [self addChild:Movie_Sprite[0] z:1];
        
        [self scene10_1];
        
        touchFlag = true;
        
    } else if (page == 11) {
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"gama11.plist" textureFile:@"gama11.pvr.gz"];
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"gama11.pvr.gz"];
        [self addChild:spriteSheet];
        
        Movie_Sprite[0] = [CCSprite spriteWithSpriteFrameName:@"gama11_01.png"];
        Movie_Sprite[1] = [CCSprite spriteWithSpriteFrameName:@"gama11_02.png"];
        Movie_Sprite[2] = [CCSprite spriteWithSpriteFrameName:@"gama11_03.png"];
        Movie_Sprite[3] = [CCSprite spriteWithSpriteFrameName:@"gama11_04.png"];
        Movie_Sprite[4] = [CCSprite spriteWithSpriteFrameName:@"gama11_05.png"];
        
        Movie_Sprite[0].position = ccp(-500.0f, 618.0f);
        Movie_Sprite[1].position = ccp(675.0f, 385.0f);
        Movie_Sprite[2].position = ccp(217.0f, 468.0f);
        Movie_Sprite[3].position = ccp(252.0f, 360.0f);
        Movie_Sprite[4].position = ccp(900.0f, 396.0f);
        
        [self addChild:Movie_Sprite[4]];
        [self addChild:Movie_Sprite[3]];
        [self addChild:Movie_Sprite[2]];
        [self addChild:Movie_Sprite[1]];
        [self addChild:Movie_Sprite[0]];
        
        //[self scene11_1];
        //[self scene11_2];
        
        touchFlag = true;
        
    } else if (page == 12) {
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"gama12_1.plist" textureFile:@"gama12_1.pvr.gz"];
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"gama12_1.pvr.gz"];
        [self addChild:spriteSheet];
        
        frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"gama12_2.plist" textureFile:@"gama12_2.pvr.gz"];
        spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"gama12_2.pvr.gz"];
        [self addChild:spriteSheet];
        
        Movie_Sprite[0] = [CCSprite spriteWithSpriteFrameName:@"gama12_01.png"];
        Movie_Sprite[1] = [CCSprite spriteWithSpriteFrameName:@"gama12_02.png"];
        Movie_Sprite[2] = [CCSprite spriteWithSpriteFrameName:@"gama12_03.png"];
        Movie_Sprite[3] = [CCSprite spriteWithSpriteFrameName:@"gama12_04.png"];
        Movie_Sprite[4] = [CCSprite spriteWithSpriteFrameName:@"gama12_05.png"];
        
        Movie_Sprite[5] = [CCSprite spriteWithSpriteFrameName:@"gama12_06.png"];
        Movie_Sprite[6] = [CCSprite spriteWithSpriteFrameName:@"gama12_07.png"];
        Movie_Sprite[7] = [CCSprite spriteWithSpriteFrameName:@"gama12_08.png"];
        Movie_Sprite[8] = [CCSprite spriteWithSpriteFrameName:@"gama12_09.png"];
        
        Movie_Sprite[0].position = ccp(510.0f, 386.0f);
        Movie_Sprite[1].position = ccp(485.0f, 90.0f);
        Movie_Sprite[2].position = ccp(525.0f, 134.0f);
        Movie_Sprite[3].position = ccp(513.0f, 190.0f);
        Movie_Sprite[4].position = ccp(545.0f, 199.0f);
        
        Movie_Sprite[5].position = ccp(512.0f, 209.0f);
        Movie_Sprite[6].position = ccp(497.0f, 219.0f);
        Movie_Sprite[7].position = ccp(866.0f, 370.0f);
        Movie_Sprite[8].position = ccp(530.0f, 215.0f);
        
        
        [self addChild:Movie_Sprite[8]];
        [self addChild:Movie_Sprite[7]];
        [self addChild:Movie_Sprite[6]];
        [self addChild:Movie_Sprite[5]];
        [self addChild:Movie_Sprite[4]];
        [self addChild:Movie_Sprite[3]];
        [self addChild:Movie_Sprite[2]];
        [self addChild:Movie_Sprite[1]];
        [self addChild:Movie_Sprite[0]];
        
        [self scene12];
        [self scene12_2];
        
    } else if (page == 13) {
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"gama13.plist" textureFile:@"gama13.pvr.gz"];
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"gama13.pvr.gz"];
        [self addChild:spriteSheet];
        
        Movie_Sprite[0] = [CCSprite spriteWithSpriteFrameName:@"gama13_01.png"];
        Movie_Sprite[1] = [CCSprite spriteWithSpriteFrameName:@"gama13_02.png"];
        Movie_Sprite[2] = [CCSprite spriteWithSpriteFrameName:@"gama13_03.png"];
        
        Movie_Sprite[0].position = ccp(248.0f, 425.0f);
        Movie_Sprite[1].position = ccp(344.0f, 652.0f);
        //Movie_Sprite[2].position = ccp(549.0f, 361.0f);
        Movie_Sprite[2].position = ccp(1049.0f, -261.0f);
        
        [self addChild:Movie_Sprite[0]];
        [self addChild:Movie_Sprite[1]];
        [self addChild:Movie_Sprite[2]];
        
        touchFlag = false;
        
        [self scene13];

    } else if (page == 14) {
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"gama14.plist" textureFile:@"gama14.pvr.gz"];
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"gama14.pvr.gz"];
        [self addChild:spriteSheet];
        
        Movie_Sprite[0] = [CCSprite spriteWithSpriteFrameName:@"gama14_01.png"];
        Movie_Sprite[1] = [CCSprite spriteWithSpriteFrameName:@"gama14_02.png"];
        Movie_Sprite[2] = [CCSprite spriteWithSpriteFrameName:@"gama14_03.png"];
        Movie_Sprite[3] = [CCSprite spriteWithSpriteFrameName:@"gama14_04.png"];
        Movie_Sprite[4] = [CCSprite spriteWithSpriteFrameName:@"gama14_05.png"];
        Movie_Sprite[5] = [CCSprite spriteWithSpriteFrameName:@"gama14_06.png"];
        Movie_Sprite[6] = [CCSprite spriteWithSpriteFrameName:@"gama14_07.png"];
        Movie_Sprite[7] = [CCSprite spriteWithSpriteFrameName:@"gama14_08.png"];
        
        Movie_Sprite[0].position = ccp(183.0f, 282.0f);
        Movie_Sprite[1].position = ccp(824.0f, 324.0f);
        Movie_Sprite[2].position = ccp(502.0f, 509.0f);
        Movie_Sprite[3].position = ccp(502.0f, 211.0f);
        Movie_Sprite[4].position = ccp(243.0f, 597.0f);
        Movie_Sprite[5].position = ccp(808.0f, 574.0f);
        Movie_Sprite[6].position = ccp(736.0f, 596.0f);
        Movie_Sprite[7].position = ccp(215.0f, 536.0f);
        
        [self addChild:Movie_Sprite[7] z:4];
        [self addChild:Movie_Sprite[6] z:3];
        [self addChild:Movie_Sprite[5] z:2];
        [self addChild:Movie_Sprite[4] z:1];
        [self addChild:Movie_Sprite[3]];
        [self addChild:Movie_Sprite[2]];
        [self addChild:Movie_Sprite[1]];
        [self addChild:Movie_Sprite[0]];
        
        // 追加の涙
        Tear_Sprite[1] = [CCSprite spriteWithSpriteFrameName:@"gama14_05.png"];
        Tear_Sprite[1].position = Movie_Sprite[4].position;
        [self addChild:Tear_Sprite[1]];
        [Tear_Sprite[1] runAction:[CCFadeOut actionWithDuration:0.0f]];
        
        Tear_Sprite[2] = [CCSprite spriteWithSpriteFrameName:@"gama14_06.png"];
        Tear_Sprite[2].position = Movie_Sprite[5].position;
        [self addChild:Tear_Sprite[2]];
        [Tear_Sprite[2] runAction:[CCFadeOut actionWithDuration:0.0f]];
        
        Tear_Sprite[3] = [CCSprite spriteWithSpriteFrameName:@"gama14_07.png"];
        Tear_Sprite[3].position = Movie_Sprite[6].position;
        [self addChild:Tear_Sprite[3]];
        [Tear_Sprite[3] runAction:[CCFadeOut actionWithDuration:0.0f]];
        
        Tear_Sprite[4] = [CCSprite spriteWithSpriteFrameName:@"gama14_08.png"];
        Tear_Sprite[4].position = Movie_Sprite[7].position;
        [self addChild:Tear_Sprite[4]];
        [Tear_Sprite[4] runAction:[CCFadeOut actionWithDuration:0.0f]];
        
        touchCount = 0;
        touchFlag = true;
        [self cdSoundSourceSE:@"piyu2.mp3" :0]; // SE 事前に読み込み
        myEffect[0].looping = YES;
        [self scene14_init]; // 初期化
    } else if (page == 15) {
        
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"gama15-hd.plist" textureFile:@"gama15-hd.pvr.gz"];
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"gama15-hd.pvr.gz"]; // !!!: Couldn't add PVRImage:gama-hd15.pvr.gz in CCTextureCache
        [self addChild:spriteSheet];
        
        Movie_Sprite[0] = [CCSprite spriteWithSpriteFrameName:@"gama015_01_01.png"];
        Movie_Sprite[1] = [CCSprite spriteWithSpriteFrameName:@"gama015_01.png"];
        Movie_Sprite[2] = [CCSprite spriteWithSpriteFrameName:@"gama015_02_01.png"];
        Movie_Sprite[3] = [CCSprite spriteWithSpriteFrameName:@"gama015_02.png"];
        Movie_Sprite[4] = [CCSprite spriteWithSpriteFrameName:@"gama015_03.png"];
        Movie_Sprite[5] = [CCSprite spriteWithSpriteFrameName:@"gama015_04.png"];
        Movie_Sprite[6] = [CCSprite spriteWithSpriteFrameName:@"gama015_05.png"];
        Movie_Sprite[7] = [CCSprite spriteWithSpriteFrameName:@"gama015_06.png"];
        
        Movie_Sprite[0].position = ccp(340.0f, 255.0f);
        Movie_Sprite[1].position = ccp(275.0f, 225.0f);
        Movie_Sprite[2].position = ccp(613.0f, 230.0f);
        Movie_Sprite[3].position = ccp(725.0f, 196.0f);
        Movie_Sprite[4].position = ccp(500.0f, 395.0f);
        Movie_Sprite[5].position = ccp(450.0f, 200.0f);
        Movie_Sprite[6].position = ccp(700.0f, 500.0f);
        Movie_Sprite[7].position = ccp(500.0f, 400.0f);
        
        Movie_Sprite[0].anchorPoint = CGPointMake(0.3f, 0.4f);
        
        particles[2] = [CCParticleSystemQuad particleWithFile:@"gama15_particle5.plist"];
        particles[2].autoRemoveOnFinish = YES;
        
        [self addChild:Movie_Sprite[7]];
        [self addChild:Movie_Sprite[5]];
        [self addChild:Movie_Sprite[6]];
        [self addChild:particles[2]];
        [self addChild:Movie_Sprite[1]];
        [self addChild:Movie_Sprite[3]];
        [self addChild:Movie_Sprite[4]];
        [self addChild:Movie_Sprite[2]];
        [self addChild:Movie_Sprite[0]];
        
        
        touchFlag = true;
        [self scene15_2];
        
    } else if (page == 16) {
        
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"gama16.plist" textureFile:@"gama16.pvr.gz"];
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"gama16.pvr.gz"];
        [self addChild:spriteSheet];
        
        Movie_Sprite[0] = [CCSprite spriteWithSpriteFrameName:@"gama016_01.png"];//ネコ本体
        Movie_Sprite[1] = [CCSprite spriteWithSpriteFrameName:@"gama016_02.png"];//ネコ尻尾
        Movie_Sprite[2] = [CCSprite spriteWithSpriteFrameName:@"gama016_03.png"];//与吉の手
        Movie_Sprite[3] = [CCSprite spriteWithSpriteFrameName:@"gama016_04.png"];//与吉本体
        Movie_Sprite[4] = [CCSprite spriteWithSpriteFrameName:@"gama016_05.png"];//ネコB
        
        Movie_Sprite[0].position = ccp(805.0f, 140.0f);
        Movie_Sprite[1].anchorPoint = CGPointMake(0.45f, 0.1f);
        Movie_Sprite[1].position = ccp(838.0f, 85.0f);
        Movie_Sprite[2].anchorPoint = CGPointMake(0.75f, 0.58f);
        Movie_Sprite[2].position = ccp(448.0f, 205.0f);
        Movie_Sprite[3].position = ccp(470.0f, 202.0f);
        Movie_Sprite[4].position = ccp(382.0f, 540.0f);
        
        [self addChild:Movie_Sprite[1]];
        [self addChild:Movie_Sprite[0]];
        [self addChild:Movie_Sprite[3]];
        [self addChild:Movie_Sprite[4]];
        [self addChild:Movie_Sprite[2]];
        
        touchCount = 0;
        touchFlag = true;
        
    } else if (page == 17) {
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"gama17.plist" textureFile:@"gama17.pvr.gz"];
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"gama17.pvr.gz"];
        [self addChild:spriteSheet];
        
        Movie_Sprite[0] = [CCSprite spriteWithSpriteFrameName:@"gama17_01.png"];
        Movie_Sprite[1] = [CCSprite spriteWithSpriteFrameName:@"gama17_03.png"];
        Movie_Sprite[2] = [CCSprite spriteWithSpriteFrameName:@"gama17_04.png"];
        Movie_Sprite[3] = [CCSprite spriteWithSpriteFrameName:@"gama17_05.png"];
        Movie_Sprite[4] = [CCSprite spriteWithSpriteFrameName:@"gama17_06.png"];
        Movie_Sprite[5] = [CCSprite spriteWithSpriteFrameName:@"gama17_07.png"];
        Movie_Sprite[6] = [CCSprite spriteWithSpriteFrameName:@"gama17_08.png"];
        Movie_Sprite[7] = [CCSprite spriteWithSpriteFrameName:@"gama17_09.png"];
        Movie_Sprite[8] = [CCSprite spriteWithSpriteFrameName:@"gama17_02.png"]; // 頬赤後
        
        Movie_Sprite[0].position = ccp(344.0f, 164.0f);
        Movie_Sprite[1].position = ccp(638.0f, 415.0f);
        Movie_Sprite[2].position = ccp(254.0f, 618.0f);
        Movie_Sprite[3].position = ccp(817.0f, 23.0f);
        Movie_Sprite[4].position = ccp(763.0f, 148.0f);
        Movie_Sprite[5].position = ccp(887.0f, 142.0f);
        Movie_Sprite[6].position = ccp(663.0f, 140.0f);
        Movie_Sprite[7].position = ccp(1004.0f, 126.0f);
        Movie_Sprite[8].position = ccp(344.0f, 164.0f);
        
        [self addChild:Movie_Sprite[8] z:1];
        [self addChild:Movie_Sprite[7]];
        [self addChild:Movie_Sprite[6]];
        [self addChild:Movie_Sprite[4]];
        [self addChild:Movie_Sprite[5]];
        [self addChild:Movie_Sprite[3]];
        [self addChild:Movie_Sprite[2]];
        [self addChild:Movie_Sprite[1]];
        [self addChild:Movie_Sprite[0]];
        
        touchCount = 0;
        touchFlag = true;
        touchFlag2 = true;
        soundPlayFlag = false;
        [Movie_Sprite[2] runAction:[CCFadeOut actionWithDuration:0.0f]]; // ハートをフェードアウトしておく
        [Movie_Sprite[8] runAction:[CCFadeOut actionWithDuration:0.0f]]; // 嫁の頬赤後をフェードアウトしておく
        soundCount = 0;

        [self scene17];
    } else if (page == 18) {
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"gama18.plist" textureFile:@"gama18.pvr.gz"];
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"gama18.pvr.gz"];
        [self addChild:spriteSheet];
        
        Movie_Sprite[0] = [CCSprite spriteWithSpriteFrameName:@"gama18_01.png"];
        Movie_Sprite[1] = [CCSprite spriteWithSpriteFrameName:@"gama18_02.png"];
        Movie_Sprite[2] = [CCSprite spriteWithSpriteFrameName:@"gama18_03.png"];
        Movie_Sprite[3] = [CCSprite spriteWithSpriteFrameName:@"gama18_04.png"];
        Movie_Sprite[4] = [CCSprite spriteWithSpriteFrameName:@"gama18_05.png"];
        Movie_Sprite[5] = [CCSprite spriteWithSpriteFrameName:@"gama18_06.png"];
        Movie_Sprite[6] = [CCSprite spriteWithSpriteFrameName:@"gama18_07.png"];
        
        Movie_Sprite[0].position = ccp(355.0f, 200.0f);
        Movie_Sprite[1].position = ccp(425.0f, 30.0f);
        Movie_Sprite[2].position = ccp(600.0f, 254.0f);
        Movie_Sprite[3].position = ccp(504.0f, 239.0f);
        Movie_Sprite[4].position = ccp(871.0f, 197.0f);
        Movie_Sprite[5].position = ccp(747.0f, 423.0f);
        Movie_Sprite[6].position = ccp(511.0f, 384.0f);
        
        [self addChild:Movie_Sprite[6]];
        [self addChild:Movie_Sprite[5]];        
        [self addChild:Movie_Sprite[3]];
        [self addChild:Movie_Sprite[4]];
        [self addChild:Movie_Sprite[2]];
        [self addChild:Movie_Sprite[1]];
        [self addChild:Movie_Sprite[0]];
        [self scene18];
        [self scene18_Sound];
    }
}

// にわとり
-(void) addNewSpriteWithCoords
{
    //動的ボディの定義
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
    
	bodyDef.position.Set(265.0f/PTM_RATIO, 64.0f/PTM_RATIO);
	bodyDef.userData = Movie_Sprite[0];
	body = world->CreateBody(&bodyDef);
	
    //ボックスシェイプを定義
	b2CircleShape circleShape;
    float boxwsize =  Movie_Sprite[0].contentSize.width / PTM_RATIO;
    circleShape.m_radius = boxwsize * 0.4;
    
	// Define the dynamic body fixture.
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &circleShape;	
	fixtureDef.density = 1.0f; // 密度
	fixtureDef.friction = 1.0f; // 摩擦
    fixtureDef.restitution = 0.25f; // 反発係数
    //初期加速度、位置
    // b2Vec2 force = b2Vec2(100, -1000);
    // body->ApplyLinearImpulse(force, bodyDef.position);
    
	_paddleFixture[0] = body->CreateFixture(&fixtureDef);
    
    //動的ボディの定義
	bodyDef.type = b2_dynamicBody;
    
	bodyDef.position.Set(595.0f/PTM_RATIO, 64.0f/PTM_RATIO);
	bodyDef.userData = Movie_Sprite[1];
	body = world->CreateBody(&bodyDef);
	
    //ボックスシェイプを定義
    boxwsize =  Movie_Sprite[1].contentSize.width / PTM_RATIO;
    circleShape.m_radius = boxwsize * 0.4;
    
	// Define the dynamic body fixture.
	fixtureDef.shape = &circleShape;	
	fixtureDef.density = 1.0f; // 密度
	fixtureDef.friction = 1.0f; // 摩擦
    fixtureDef.restitution = 0.25f; // 反発係数
    
	_paddleFixture[1] = body->CreateFixture(&fixtureDef);
}

/* ちらつき処理用
 * @param on zバッファのON/OFF
 */
-(void)zbuffer :(BOOL)on {
    //[[CCDirector sharedDirector] setProjection:kCCDirectorProjection2D];  // 2Dプロジェクションを使い、必要に応じて3Dプロジェクションに切り替える。
    [[CCDirector sharedDirector] setDepthTest: on];  // zバッファ使わない
}

-(void)scene1{
    // 約1-2秒後に右から平行移動
    id a1 = [CCMoveBy actionWithDuration:7.0f position:ccp(-900,0)];
    [Movie_Sprite[0] runAction:a1];
}

//引っ張られてる手前の草の動き
-(void)scene2_TouchMoveFront: (float)R{
    //id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    //右上
    R = R/75;
    float volume = 0.4;
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.4;
    } else { // 停止中
        volume = 0.75;
    }
    [self playSound :1 :@"scene2_kusa1.mp3" :volume];
    Movie_Sprite[2].rotation = -R;//左下
    Movie_Sprite[3].rotation = R;//右下
    //Movie_Sprite[2].position = ccp(X,Y);
}
//引っ張られてる手前の右の木の動き
-(void)scene2_TouchMoveFrontRightWood: (float)R{
    //id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    //右上
    R = R/75;
    float volume = 0.4;
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.4;
    } else { // 停止中
        volume = 0.75;
    }
    [self playSound :1 :@"scene2_kusa1.mp3" :volume];
    Movie_Sprite[1].rotation = R;//右上
}
//引っ張られてる手前の左の木の動き
-(void)scene2_TouchMoveFrontLeftWood: (float)R{
    //id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    //右上
    R = R/75;
    float volume = 0.4;
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.4;
    } else { // 停止中
        volume = 0.75;
    }
    [self playSound :1 :@"scene2_kusa1.mp3" :volume];
    Movie_Sprite[0].rotation = -R;//左上
}
//それなりに引っ張られた手前の草の動き
-(void)scene2_MoveEndedKusaFront: (float)R{
    id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    R = R/180;
    float volume = 0.4;
    
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.4;
    } else { // 停止中
        volume = 0.75;
    }
    [self playSound :1 :@"scene2_kusa1.mp3" :volume];
    //左下
    id a3_1 = [CCRotateTo actionWithDuration:0.8f angle:R];
    id a3_2 = [CCRotateTo actionWithDuration:1.2f angle:-R];
    id a3_3 = [CCRotateTo actionWithDuration:1.0f angle:0];
    id s3 = [CCSequence actions:a3_1,a3_2,a3_1,a3_2,a3_3,nil];
    id elasticInAction3 = [CCEaseElasticOut actionWithAction:s3 period:6.0f];
    [Movie_Sprite[2] runAction:elasticInAction3];
    
    //右下
    id a4_1 = [CCRotateTo actionWithDuration:0.7f angle:R];
    id a4_2 = [CCRotateTo actionWithDuration:1.0f angle:-R];
    id a4_3 = [CCRotateTo actionWithDuration:0.8f angle:0];
    id s4 = [CCSequence actions:a4_1,a4_2,a4_1,a4_2,a4_3,reset,nil];
    id elasticInAction4 = [CCEaseElasticOut actionWithAction:s4 period:6.0f];
    [Movie_Sprite[3] runAction:elasticInAction4];
    
}
//それなりに引っ張られた手前の右の木の動き
-(void)scene2_MoveEndedKusaFrontRightWood: (float)R{
    id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    R = R/45;
    float volume = 0.4;
    
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.4;
    } else { // 停止中
        volume = 0.75;
    }
    [self playSound :1 :@"scene2_kusa1.mp3" :volume];
    //右上
    id a1_1 = [CCRotateTo actionWithDuration:1.0f angle:-R];
    id a1_2 = [CCRotateTo actionWithDuration:0.8f angle:R];
    id a1_3 = [CCRotateTo actionWithDuration:1.0f angle:0];
    id s1 = [CCSequence actions:a1_1,a1_2,a1_1,a1_2,a1_3,reset,nil];
    id elasticInAction1 = [CCEaseElasticOut actionWithAction:s1 period:8.0f];
    [Movie_Sprite[0] runAction:elasticInAction1];
}
//それなりに引っ張られた手前の左の木の動き
-(void)scene2_MoveEndedKusaFrontLeftWood: (float)R{
    id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    R = R/55;
    float volume = 0.4;
    
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.4;
    } else { // 停止中
        volume = 0.75;
    }
    [self playSound :1 :@"scene2_kusa1.mp3" :volume];
    //左上
    id a2_1 = [CCRotateTo actionWithDuration:0.8f angle:-R];
    id a2_2 = [CCRotateTo actionWithDuration:1.0f angle:R];
    id a2_3 = [CCRotateTo actionWithDuration:0.8f angle:0];
    id s2 = [CCSequence actions:a2_1,a2_2,a2_1,a2_3,reset,nil];
    id elasticInAction2 = [CCEaseElasticOut actionWithAction:s2 period:8.0f];
    [Movie_Sprite[1] runAction:elasticInAction2];
}

//限界まで引っ張られた手前の草の動き
-(void)scene2_TouchKusaFrontMax: (float)R{
    float volume = 0.4;
    
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.4;
    } else { // 停止中
        volume = 0.75;
    }
    [self playSound :1 :@"scene2_kusa2.mp3" :volume];
    // 草かき分け(手前右側)
    id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    //id touchMoveFlagreset = [CCCallFunc actionWithTarget:self selector:@selector(touchMoveFlagEnable)];
    //右上
    id move1 = [CCMoveBy actionWithDuration:3.0f position:ccp(700,150)];
    id yure1 = [CCRotateTo actionWithDuration:2.0f angle:30];
    id spawn1 = [CCSpawn actions:yure1, move1, nil];
    [Movie_Sprite[0] runAction:spawn1];
    
    //左上
    id move2 = [CCRotateTo actionWithDuration:3.0f angle:-30];
    id yure2 = [CCMoveBy actionWithDuration:2.0f position:ccp(-700,150)];
    id spawn2 = [CCSpawn actions:move2, yure2, nil];
    [Movie_Sprite[1] runAction:spawn2];
    
    //右下
    id move3 = [CCSpawn actions:[CCMoveBy actionWithDuration:2.5f position:ccp(780,-150)],
                [CCRotateTo actionWithDuration:3.0f angle:45],
                nil];
    id sequense3 = [CCSequence actions:move3,reset,nil];
    [Movie_Sprite[3] runAction:[CCSpawn actions:sequense3,nil]];
    
    //左下
    id move4 =[CCSpawn actions:[CCMoveBy actionWithDuration:3.5f position:ccp(-600,0)],
               [CCRotateTo actionWithDuration:2.0f angle:-45],
               nil];
    [Movie_Sprite[2] runAction:[CCSpawn actions:move4,nil]];
}
//限界まで引っ張られた手前の草の動き
-(void)scene2_MoveKusaFrontMax: (float)R{
    float volume = 0.4;
    
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.4;
    } else { // 停止中
        volume = 0.75;
    }
    [self playSound :1 :@"scene2_kusa2.mp3" :volume];
    // 草かき分け(手前右側)
    id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    id touchMoveFlagreset = [CCCallFunc actionWithTarget:self selector:@selector(touchMoveFlagEnable)];
    //右上
    id a1_1 = [CCRotateTo actionWithDuration:0.2f angle:-2];
    id a1_2 = [CCRotateTo actionWithDuration:0.3f angle:2];
    id move1 = [CCMoveBy actionWithDuration:3.0f position:ccp(700,150)];
    id yure1 = [CCRotateTo actionWithDuration:2.0f angle:30];
    id spawn1 = [CCSpawn actions:yure1, move1, nil];
    id kusayureRight1 = [CCSequence actions:a1_1,a1_2,a1_1,a1_2,nil];
    id s3 = [CCSequence actions:kusayureRight1,spawn1,nil];
    [Movie_Sprite[0] runAction:s3];
    
    //左上
    id a2_1 = [CCRotateTo actionWithDuration:0.2f angle:-2];
    id a2_2 = [CCRotateTo actionWithDuration:0.3f angle:2];
    id move2 = [CCRotateTo actionWithDuration:3.0f angle:-30];
    id yure2 = [CCMoveBy actionWithDuration:2.0f position:ccp(-700,150)];
    id spawn2 = [CCSpawn actions:move2, yure2, nil];
    id kusayureLeft = [CCSequence actions:a2_1,a2_2,a2_1,a2_2,nil];
    id s2 = [CCSequence actions:kusayureLeft,spawn2,nil];///
    [Movie_Sprite[1] runAction:s2];
    
    //右下
    id move3 = [CCSpawn actions:[CCMoveBy actionWithDuration:2.5f position:ccp(780,0)],
                [CCRotateTo actionWithDuration:3.0f angle:15],
                nil];
    id a3_1 = [CCRotateTo actionWithDuration:0.7f angle: 1];
    id a3_2 = [CCRotateTo actionWithDuration:0.5f angle:-1];
    id a3_3 = [CCRotateTo actionWithDuration:1.5f angle: 2];
    id yure3 = [CCSequence actions:a3_3,a3_2,a3_1,a3_2,nil];
    id e3 = [CCEaseElasticOut actionWithAction:yure3 period:3.0f];
    id delay3 = [CCDelayTime actionWithDuration:1.0];
    id sequense3 = [CCSequence actions:delay3,move3,reset,nil];
    [Movie_Sprite[3] runAction:[CCSpawn actions:e3,sequense3,nil]];
    
    //左下
    id move4 =[CCSpawn actions:[CCMoveBy actionWithDuration:3.5f position:ccp(-600,0)],
               [CCRotateTo actionWithDuration:2.0f angle:-15],
               nil];
    id a4_1 = [CCRotateTo actionWithDuration:0.3f angle:-1];
    id a4_2 = [CCRotateTo actionWithDuration:0.5f angle: 1];
    id a4_3 = [CCRotateTo actionWithDuration:1.5f angle:-2];
    id yure4 = [CCSequence actions:a4_3,a4_2,a4_1,a4_2,nil];
    id e4 = [CCEaseElasticOut actionWithAction:yure4 period:3.0f];
    id delay4 = [CCDelayTime actionWithDuration:0.9];
    id sequense4 = [CCSequence actions:delay4,move4,reset,touchMoveFlagreset,nil];
    [Movie_Sprite[2] runAction:[CCSpawn actions:e4,sequense4,nil]];
}
//引っ張られてる後ろの草の動き
-(void)scene2_TouchMoveBack : (float)R{
    R = R/220;
    float volume = 0.4;
    
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.4;
    } else { // 停止中
        volume = 0.75;
    }
    [self playSound :1 :@"scene2_kusa3.mp3" :volume];
    Movie_Sprite[4].rotation = -R;//左上
    Movie_Sprite[5].rotation = R;//右上
    Movie_Sprite[6].rotation = -R;//左下
}

//それなりに引っ張られた後ろの草の動き
-(void)scene2_MoveEndedKusaBack: (float)R{
    R = R/220;
    float volume = 0.4;
    
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.4;
    } else { // 停止中
        volume = 0.75;
    }
    // 草揺れ(奥側)
    id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    [self playSound :1 :@"scene2_kusa3.mp3" :volume];
    //左手前
    id a1_1 = [CCRotateTo actionWithDuration:0.8f angle:R];
    id a1_2 = [CCRotateTo actionWithDuration:0.5f angle:-R];
    id a1_3 = [CCRotateTo actionWithDuration:0.2f angle:0];
    id s1 = [CCSequence actions:a1_1,a1_2,a1_1,a1_2,a1_3,reset,nil];
    id elasticInAction1 = [CCEaseElasticOut actionWithAction:s1 period:3.0f];
    [Movie_Sprite[4] runAction:elasticInAction1];
    
    //右
    id a2_1 = [CCRotateTo actionWithDuration:0.7f angle:R];
    id a2_2 = [CCRotateTo actionWithDuration:0.5f angle:-R];
    id a2_3 = [CCRotateTo actionWithDuration:0.2f angle:0];
    id s2 = [CCSequence actions:a2_1,a2_2,a2_1,a2_2,a2_3,nil];
    id elasticInAction2 = [CCEaseElasticOut actionWithAction:s2 period:3.0f];
    [Movie_Sprite[5] runAction:elasticInAction2];
    
    //奥左
    id a3_1 = [CCRotateTo actionWithDuration:0.7f angle:R];
    id a3_2 = [CCRotateTo actionWithDuration:0.5f angle:-R];
    id a3_3 = [CCRotateTo actionWithDuration:0.2f angle:0];
    id s3 = [CCSequence actions:a3_1,a3_2,a3_1,a3_2,a3_3,nil];
    id elasticInAction3 = [CCEaseElasticOut actionWithAction:s3 period:3.0f];
    [Movie_Sprite[6] runAction:elasticInAction3];
}
//限界まで引っ張られた後ろの草の動き
-(void)scene2_MoveKusaBackMax: (float)R{
    float volume = 0.4;
    R = R/220;
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.4;
    } else { // 停止中
        volume = 0.75;
    }
    [self playSound :1 :@"scene2_kusa4.mp3" :volume];
    //草かきわけ(左奥側)
    id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    id touchMoveFlagreset = [CCCallFunc actionWithTarget:self selector:@selector(touchMoveFlagEnable)];
    
    //左手前
    id move1 =[CCSpawn actions:[CCMoveBy actionWithDuration:6.0f position:ccp(-1400,-200)],
               [CCRotateTo actionWithDuration:4.0f angle:-10],
               nil];
    id sequense1 = [CCSequence actions:move1,nil];
    [Movie_Sprite[4] runAction:[CCSpawn actions:sequense1,nil]];
    
    //右
    id move2 =[CCSpawn actions:[CCMoveBy actionWithDuration:5.0f position:ccp(850,-230)],
               [CCRotateTo actionWithDuration:4.0f angle:-10],
               nil];
    id delay2 = [CCDelayTime actionWithDuration:0.1];
    id sequense2 = [CCSequence actions:delay2,move2,nil];
    [Movie_Sprite[5] runAction:[CCSpawn actions:sequense2,nil]];
    
    //奥左
    id move3 =[CCSpawn actions:[CCMoveBy actionWithDuration:4.0f position:ccp(0,-530)],
               [CCRotateTo actionWithDuration:4.0f angle:-10],
               nil];
    id delay3 = [CCDelayTime actionWithDuration:0.2];
    id sequense3 = [CCSequence actions:delay3,move3,reset,touchMoveFlagreset,nil];
    [Movie_Sprite[6] runAction:[CCSpawn actions:sequense3,nil]];
}
//限界まで引っ張られた後ろの草の動き
-(void)scene2_TouchKusaBackMax: (float)R{
    float volume = 0.4;
    R = R/220;
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.4;
    } else { // 停止中
        volume = 0.75;
    }
    [self playSound :1 :@"scene2_kusa4.mp3" :volume];
    //草かきわけ(左奥側)
    id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    id touchMoveFlagreset = [CCCallFunc actionWithTarget:self selector:@selector(touchMoveFlagEnable)];
    
    //左手前
    id move1 =[CCSpawn actions:[CCMoveBy actionWithDuration:6.0f position:ccp(-1400,-200)],
               [CCRotateTo actionWithDuration:4.0f angle:-10],
               nil];
    id a1_1 = [CCRotateTo actionWithDuration:0.7f angle:-1];
    id a1_2 = [CCRotateTo actionWithDuration:0.9f angle: 1];
    id yure1 = [CCSequence actions:a1_1,a1_2,a1_1,a1_2,nil];
    id e1 = [CCEaseElasticOut actionWithAction:yure1 period:3.0f];
    id delay1 = [CCDelayTime actionWithDuration:1.0];
    id sequense1 = [CCSequence actions:delay1,move1,nil];
    [Movie_Sprite[4] runAction:[CCSpawn actions:e1,sequense1,nil]];
    
    //右
    id move2 =[CCSpawn actions:[CCMoveBy actionWithDuration:5.0f position:ccp(850,-230)],
               [CCRotateTo actionWithDuration:4.0f angle:-10],
               nil];
    id a2_1 = [CCRotateTo actionWithDuration:0.8f angle:-1];
    id a2_2 = [CCRotateTo actionWithDuration:0.6f angle: 1];
    id yure2 = [CCSequence actions:a2_1,a2_2,a2_1,a2_2,nil];
    id e2 = [CCEaseElasticOut actionWithAction:yure2 period:3.0f];
    id delay2 = [CCDelayTime actionWithDuration:1.2];
    id sequense2 = [CCSequence actions:delay2,move2,nil];
    [Movie_Sprite[5] runAction:[CCSpawn actions:e2,sequense2,nil]];
    
    //奥左
    id move3 =[CCSpawn actions:[CCMoveBy actionWithDuration:4.0f position:ccp(0,-530)],
               [CCRotateTo actionWithDuration:4.0f angle:-10],
               nil];
    id a3_1 = [CCRotateTo actionWithDuration:0.8f angle:-1];
    id a3_2 = [CCRotateTo actionWithDuration:0.6f angle: 1];
    id yure3 = [CCSequence actions:a3_1,a3_2,a3_1,a3_2,nil];
    id e3 = [CCEaseElasticOut actionWithAction:yure3 period:3.0f];
    id delay3 = [CCDelayTime actionWithDuration:1.5];
    id sequense3 = [CCSequence actions:delay3,move3,reset,touchMoveFlagreset,nil];
    [Movie_Sprite[6] runAction:[CCSpawn actions:e3,sequense3,nil]];
}

//石を運ぶ人の動き
-(void)scene3{
    id a1 = [CCRotateTo actionWithDuration:0.75f angle:2];
    id a2 = [CCRotateTo actionWithDuration:0.5f angle:0];
    id a3 = [CCMoveBy actionWithDuration:0.75 position:ccp(8,0)];
    id a4 = [CCMoveBy actionWithDuration:0.5 position:ccp(0, 0)];
    id a5 = [CCSequence actions:a1, a2, nil];
    id a6 = [CCSequence actions:a3, a4, nil];
    id a7 = [CCSpawn actions:a5, a6, nil];
    id action = [CCRepeat actionWithAction:a7 times:60];
    [Movie_Sprite[9] runAction:action];
}

// 木こり
-(void)scene3_1{
    NSString *path;
    path = [NSString stringWithFormat:@"scene3_ono.mp3"];
    id se = [CCCallFuncND actionWithTarget:self selector:@selector(playSE:data:) data:[path retain]];

    Movie_Sprite[7].anchorPoint = CGPointMake(0.4f, 0.35f);
    Movie_Sprite[7].position = ccp(105, 295);
    
    id a1 = [CCRotateTo actionWithDuration:0.75f angle:60];
    id a2 = [CCRotateTo actionWithDuration:1.0f angle:0];
    id action = [CCSequence actions:a1, se, a2, nil];
    
    id action2 = [CCRepeatForever actionWithAction:[CCSequence actions:action, nil]];
    [Movie_Sprite[7] runAction:action2];
}

//ギコギコ
-(void)scene3_2{
    id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    id a1, a2, fade1, fade2, fade3, fade4, action1, action2, action3;
    if (touchCount <= 3) {
        if (touchCount % 2) {
            a1 = [CCMoveBy actionWithDuration:0.5f position:ccp(40,0)];
            action1 = [CCSequence actions: [[a1 copy] autorelease], [a1 reverse], reset, nil];
            action2 = [CCSequence actions: [[a1 copy] autorelease], [a1 reverse], nil];
            action3 = [CCSequence actions: [[a1 copy] autorelease], [a1 reverse], nil];
        } else {
            a2 = [CCMoveBy actionWithDuration:0.5f position:ccp(-30,0)];
            action1 = [CCSequence actions: [[a2 copy] autorelease], [a2 reverse], reset, nil];
            action2 = [CCSequence actions: [[a2 copy] autorelease], [a2 reverse], nil];
            action3 = [CCSequence actions: [[a2 copy] autorelease], [a2 reverse], nil];
        }
    } else if (touchCount == 4) {
        a1 = [CCMoveBy actionWithDuration:0.5f position:ccp(-30,0)];
        fade1 = [CCFadeOut actionWithDuration:4.0f];
        fade2 = [CCFadeOut actionWithDuration:4.0f];
        fade3 = [CCFadeOut actionWithDuration:2.5f];
        fade4 = [CCFadeOut actionWithDuration:1.75f];
        action1 = [CCSequence actions: [[a1 copy] autorelease], [a1 reverse], fade1, nil];
        action2 = [CCSequence actions: [[a1 copy] autorelease], [a1 reverse], fade2, nil];
        action3 = [CCSequence actions: [[a1 copy] autorelease], [a1 reverse], fade3, nil];
        
        [Movie_Sprite[2] runAction:fade4];
    } else {
        return;
    }
    
    //SE
    /*if(optionnum3[1] == 0){
        [[SimpleAudioEngine sharedEngine] playEffect:@"scene3_nokogiri.mp3"];
    }*/
    // ナレーション再生時の音量調整
    float volume;
    
    if(player.isPlaying == true) { // 再生中
        volume = 0.3;
    } else { // 停止中
        volume = 0.5;
    }
    
    [self playSound :1 :@"scene3_nokogiri.mp3" :volume];
    
    [Movie_Sprite[3] runAction:action1];
    [Movie_Sprite[4] runAction:action2];
    [Movie_Sprite[5] runAction:action3];
    
    touchCount++;
}

//琵琶ベベンベン

-(void)scene4{
    
    //Movie_Sprite[3].anchorPoint = CGPointMake(0.2f, 0.5f);
    //Movie_Sprite[3].position = ccp(525.0f, 200.0f);
    ///1琵琶2lefthand3456
    id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    
    touchCount = rand();
    if(touchCount % 3 == 0){
        
        id a1 = [CCRotateTo actionWithDuration:0.75f angle:30];
        id a2 = [CCRotateTo actionWithDuration:0.75f angle:0];
        id action = [CCSequence actions:a1, a2, nil];
        id easeInAction = [CCEaseExponentialOut actionWithAction:action];
        [Movie_Sprite[3] runAction:easeInAction];
        
        id a4 = [CCMoveTo actionWithDuration:0.25f position:ccp(795.0f, 330.0f)];
        id a5 = [CCMoveTo actionWithDuration:0.55f position:ccp(814.0f, 367.0f)];
        id action3 = [CCSequence actions:a4, a5, reset, nil];
        [Movie_Sprite[2] runAction:action3];
        
        [self playSound :1 :@"scene4_biwa1.mp3" :0.75];
    }
    else if(touchCount % 3 == 1){
        
        id a1 = [CCRotateTo actionWithDuration:0.25f angle:15];
        id a2 = [CCRotateTo actionWithDuration:0.25f angle:30];        
        id a3 = [CCRotateTo actionWithDuration:0.55f angle:0];
        id action = [CCSequence actions:a1, a2, a3, nil];
        id easeInAction = [CCEaseExponentialOut actionWithAction:action];       
        [Movie_Sprite[3] runAction:easeInAction];
        
        id a4 = [CCMoveTo actionWithDuration:0.15f position:ccp(795.0f, 330.0f)];
        id a5 = [CCMoveTo actionWithDuration:0.55f position:ccp(814.0f, 367.0f)];
        id action3 = [CCSequence actions:a4, a5, reset, nil];
        [Movie_Sprite[2] runAction:action3];
        
        [self playSound :1 :@"scene4_biwa2.mp3" :0.75];
    }
    else if(touchCount % 3 == 2){
        
        id a1 = [CCRotateTo actionWithDuration:0.75f angle:3];
        id a2 = [CCRotateTo actionWithDuration:0.65f angle:25];
        id a3 = [CCRotateTo actionWithDuration:1.15f angle:0];
        id action = [CCSequence actions:a1, a2, a3, nil];
        id easeInAction = [CCEaseExponentialOut actionWithAction:action];
        [Movie_Sprite[3] runAction:easeInAction];
        
        id a4 = [CCMoveTo actionWithDuration:0.55f position:ccp(795.0f, 330.0f)];
        id a5 = [CCMoveTo actionWithDuration:0.55f position:ccp(814.0f, 367.0f)];
        id action3 = [CCSequence actions:a4, a5, reset, nil];
        [Movie_Sprite[2] runAction:action3];
        [self playSound :1 :@"scene4_biwa3.mp3" :0.75];
    }
    
    //Movie_Sprite[2].position = ccp(814.0f, 367.0f);   
    
}


// シーン5 波と船と俊成さん
-(void)scene5_Sound{
    float volume = 0.8;
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.8;
    } else { // 停止中
        volume = 1.2;
    }
    [self playSound :1 :@"scene5_nami.mp3" :volume];
}
-(void)scene5_kaniright{
    float volume = 0.8;
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.8;
    } else { // 停止中
        volume = 1.2;
    }
    [self playSound :1 :@"scene5_kaniright.mp3" :volume];
}
-(void)scene5_kanileft{
    float volume = 0.8;
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.8;
    } else { // 停止中
        volume = 1.2;
    }
    [self playSound :1 :@"scene5_kanileft.mp3" :volume];
    //id play = [CCSequence actions: delay, [self playSound :1 :@"scene5_kanileft.mp3" :volume],  nil];
    
}
-(void)scene5_1{
    id delay = [CCDelayTime actionWithDuration:0.3];
    
    id a5 = [CCMoveBy actionWithDuration:3.2f position:ccp(0.0f,-3.0f)];
    id a6 = [CCSequence actions: [[a5 copy] autorelease], [a5 reverse], nil];
    id repeat3 = [CCRepeatForever actionWithAction:a6];
    [Movie_Sprite[10] runAction:repeat3];// 波4最奥
    id a3 = [CCMoveBy actionWithDuration:3.0f position:ccp(0.0f,-5.0f)];
    id a4 = [CCSequence actions: [[a3 copy] autorelease], [a3 reverse], nil];
    id repeat2 = [CCRepeatForever actionWithAction:a4];
    [Movie_Sprite[9]  runAction:repeat2];// 波3
    id a1 = [CCMoveBy actionWithDuration:2.3f position:ccp(0.0f,-15.0f)];
    id a2 = [CCSequence actions: [[a1 copy] autorelease], [a1 reverse],delay, nil];
    id repeat1 = [CCRepeatForever actionWithAction:a2];
    [Movie_Sprite[7]  runAction:repeat1];// 波2
    id a7 = [CCMoveBy actionWithDuration:2.3f position:ccp(0.0f,-18.0f)];
    id a8 = [CCSequence actions: delay,[[a7 copy] autorelease], [a7 reverse], nil];
    id repeat4 = [CCRepeatForever actionWithAction:a8];
    [Movie_Sprite[8]  runAction:repeat4];// 俊成さんと船
    id a9 = [CCMoveBy actionWithDuration:2.0f position:ccp(0.0f,-33.0f)];
    id a10 = [CCSequence actions: [[a9 copy] autorelease], [a9 reverse], nil];
    id repeat5 = [CCRepeatForever actionWithAction:a10];
    [Movie_Sprite[6]  runAction:repeat5];// 波1最前面
    
    id a11 = [CCJumpBy actionWithDuration:3.0 position:ccp(16,  0) height:2 jumps:8];
    id action1 = [CCSequence actions: [[a11 copy] autorelease], [a11 reverse],  nil];
    id repeat6 = [CCRepeatForever actionWithAction:action1];
    [Movie_Sprite[2]  runAction:repeat6];// カニさんレフト
    id a12 = [CCJumpBy actionWithDuration:2.5 position:ccp(16,  0) height:2 jumps:8];
    id action2 = [CCSequence actions: [[a12 copy] autorelease], [a12 reverse],  nil];
    id repeat7 = [CCRepeatForever actionWithAction:action2];
    [Movie_Sprite[3]  runAction:repeat7];// カニさんライト
}
// シーン5 カニさんレフト　タッチ
-(void)scene5_2{
    float volume = 0.8;
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.8;
    } else { // 停止中
        volume = 1.2;
    }
    id delay = [CCDelayTime actionWithDuration:0.3];
    int kaniRand = rand() % 10;
    id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    if      (kaniRand <= 1){// カニさんレフト左へ
        id a1 = [CCJumpBy actionWithDuration:0.5 position:ccp(16,  4) height:2 jumps:4];
        id action = [CCSequence actions: [[a1 copy] autorelease], [a1 reverse], reset, nil];
        [self playSound :1 :@"scene7_crab.mp3" :volume];
        [Movie_Sprite[2]  runAction:action];
    }else if(kaniRand <= 2){// カニさんレフトガタガタ
        id a1 = [CCJumpBy actionWithDuration:0.5 position:ccp( 0,  0) height:2 jumps:4];
        id action = [CCSequence actions: [[a1 copy] autorelease], [a1 reverse], reset, nil];
        [self playSound :1 :@"scene7_crab.mp3" :volume];
        [Movie_Sprite[2]  runAction:action];
    }else if(kaniRand <= 3){// カニさんレフト下へ
        id a1 = [CCJumpBy actionWithDuration:0.5 position:ccp(-4,-16) height:2 jumps:4];
        id action = [CCSequence actions: [[a1 copy] autorelease], [a1 reverse], reset, nil];
        [self playSound :1 :@"scene7_crab.mp3" :volume];
        [Movie_Sprite[2]  runAction:action];
    }else if(kaniRand <= 4){// カニさんレフト左へ
        id a1 = [CCJumpBy actionWithDuration:0.5 position:ccp(-16,-4) height:2 jumps:4];
        id action = [CCSequence actions: [[a1 copy] autorelease], [a1 reverse], reset, nil];
        [self playSound :1 :@"scene7_crab.mp3" :volume];
        [Movie_Sprite[2]  runAction:action];
    }else{// カニさんでっかくなる
        [self scene5_kanileft];
        id a1 = [CCScaleBy actionWithDuration:0.3 scaleX:2.0f scaleY:2.0f];
        id action1 = [CCSequence actions:  delay,[[a1 copy] autorelease], [a1 reverse], reset, nil];
        [Movie_Sprite[3]  runAction:action1];// カニさんライト
        id action2 = [CCSequence actions: [[a1 copy] autorelease], [a1 reverse], nil];
        [Movie_Sprite[2]  runAction:action2];// カニさんレフト
        
        [self playSound :1 :@"scene5_kanileft.mp3" :volume];
        
        //id a4 = [CCRotateTo actionWithDuration:0.75f angle:60];
        id a2 = [CCJumpBy actionWithDuration:0.3 position:ccp(-16,  4) height:2 jumps:8];
        id action0 = [CCSequence actions: [[a2 copy] autorelease], [a2 reverse], nil];
        [Movie_Sprite[0]  runAction:action0];//右の草
        id a3 = [CCJumpBy actionWithDuration:0.3 position:ccp(16,  -4) height:2 jumps:8];
        id action3 = [CCSequence actions: [[a3 copy] autorelease], [a3 reverse], nil];
        [Movie_Sprite[1]  runAction:action3];//左の草
    }
    
}
// シーン5 カニさんライト　タッチ
-(void)scene5_3{
    float volume = 0.8;
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.8;
    } else { // 停止中
        volume = 1.2;
    }
    id delay = [CCDelayTime actionWithDuration:0.5];
    int kaniRand = rand() % 10;
    id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    if      (kaniRand <= 1){// カニさんライト左へ
        id a1 = [CCJumpBy actionWithDuration:0.5 position:ccp(16,4) height:2 jumps:4];
        id action = [CCSequence actions: [[a1 copy] autorelease], [a1 reverse], reset, nil];
        [self playSound :1 :@"scene7_crab.mp3" :volume];
        [Movie_Sprite[3]  runAction:action];
    }else if(kaniRand <= 2){// カニさんライト上へ
        id a1 = [CCJumpBy actionWithDuration:0.5 position:ccp(0,0) height:2 jumps:4];
        id action = [CCSequence actions: [[a1 copy] autorelease], [a1 reverse], reset, nil];
        [self playSound :1 :@"scene7_crab.mp3" :volume];
        [Movie_Sprite[3]  runAction:action];
    }else if(kaniRand <= 3){// カニさんライト下へ
        id a1 = [CCJumpBy actionWithDuration:0.5 position:ccp(-4,-16) height:2 jumps:4];
        id action = [CCSequence actions: [[a1 copy] autorelease], [a1 reverse], reset, nil];
        [self playSound :1 :@"scene7_crab.mp3" :volume];
        [Movie_Sprite[3]  runAction:action];
    }else if(kaniRand <= 4){// カニさんライト右へ
        id a1 = [CCJumpBy actionWithDuration:0.5 position:ccp(-16,-4) height:2 jumps:4];
        id action = [CCSequence actions: [[a1 copy] autorelease], [a1 reverse], reset, nil];
        [self playSound :1 :@"scene7_crab.mp3" :volume];
        [Movie_Sprite[3]  runAction:action];
    }else{// カニさん跳ぶ
        [self scene5_kaniright];
        id a1 = [CCJumpBy actionWithDuration:0.5 position:ccp(0,0) height:300 jumps:1];
        id action1 = [CCSequence actions: [[a1 copy] autorelease], nil];
        [Movie_Sprite[3]  runAction:action1];// カニさんライト
        id action2 = [CCSequence actions: delay,[[a1 copy] autorelease], reset, nil];
        [Movie_Sprite[2]  runAction:action2];// カニさんレフト
        
        [self playSound :1 :@"scene5_kaniright.mp3" :volume];
        
        id a2 = [CCJumpBy actionWithDuration:0.3 position:ccp(-16,  4) height:2 jumps:8];
        id action0 = [CCSequence actions: [[a2 copy] autorelease], [a2 reverse], nil];
        [Movie_Sprite[0]  runAction:action0];//右の草
        id a3 = [CCJumpBy actionWithDuration:0.3 position:ccp(16,  -4) height:2 jumps:8];
        id action3 = [CCSequence actions: [[a3 copy] autorelease], [a3 reverse], nil];
        [Movie_Sprite[1]  runAction:action3];//左の草
    }
}
//右の草タッチ
-(void)scene5_4{
    //id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    id a1_1 = [CCRotateTo actionWithDuration:1.2f angle:-2];
    id a1_2 = [CCRotateTo actionWithDuration:1.2f angle: 2];
    id yure1 = [CCSequence actions:a1_1,a1_2,a1_1,a1_2,a1_1,a1_2,a1_1,a1_2,nil];
    id e1 = [CCEaseElasticOut actionWithAction:yure1 period:3.0f];
    id s1 = [CCSequence actions: e1, nil];
    [Movie_Sprite[0]  runAction:s1];//右の草
}
//左の草タッチ
-(void)scene5_5{
    //id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    id a1_1 = [CCRotateTo actionWithDuration:1.2f angle: 2];
    id a1_2 = [CCRotateTo actionWithDuration:1.2f angle:-2];
    id yure1 = [CCSequence actions:a1_1,a1_2,a1_1,a1_2,a1_1,a1_2,a1_1,a1_2,nil];
    id e1 = [CCEaseElasticOut actionWithAction:yure1 period:3.0f];
    id s1 = [CCSequence actions: e1, nil];
    [Movie_Sprite[1]  runAction:s1];//左の草
}

//俊成さん出現
-(void)scene6_1{
    NSString *path;
    path = [NSString stringWithFormat:@"scene6_appear.mp3"];
    id se = [CCCallFuncND actionWithTarget:self selector:@selector(playSE:data:) data:[path retain]];
    
    
    //[self playSound :1 :@"scene3_nokogiri.mp3" :0.75];
    //Movie_Sprite[0].position = ccp(0.0f, 400.0f);    
    
    id a1 = [CCMoveTo actionWithDuration:3.00f position:ccp(800.0f, 250.0f)];
    id a2 = [CCFadeIn actionWithDuration:3.00f ];
    id a3 = [CCScaleTo actionWithDuration:3.00f scale:1.4f];
    id a4 = [CCSpawn  actions:a1, a2, a3, se, nil ];
    id action1 = [CCSequence actions:a4,  nil];
    
    
    //    id action4 = [CCRepeatForever actionWithAction:[CCSequence actions:action3, nil]];
    [Movie_Sprite[0] runAction:action1];
    
}

//波の動き
-(void)scene6_2{
    //NSString *path =[[NSString alloc] init];
    //path = [NSString stringWithFormat:@"scene6_bgm.mp3"];
    //id se = [CCCallFuncND actionWithTarget:self selector:@selector(playSE:data:) data:[path retain]];
    
    id a1 = [CCMoveBy actionWithDuration:1.0f position:ccp(0.0f,10.0f)];
    id a2 = [CCSequence actions: [[a1 copy] autorelease], [a1 reverse], [a1 reverse], [[a1 copy] autorelease], nil];
    id action1 = [CCRepeatForever actionWithAction:[CCSequence actions:a2, nil]];
    [Movie_Sprite[5] runAction:action1];
    id a3 = [CCMoveBy actionWithDuration:1.4f position:ccp(0.0f,10.0f)];
    id a4 = [CCSequence actions: [[a3 copy] autorelease], [a3 reverse], [a3 reverse], [[a3 copy] autorelease], nil];
    id action2 = [CCRepeatForever actionWithAction:[CCSequence actions:a4, nil]];
    [Movie_Sprite[4] runAction:action2];    
    id a5 = [CCMoveBy actionWithDuration:1.8f position:ccp(0.0f,10.0f)];
    id a6 = [CCSequence actions: [[a5 copy] autorelease], [a5 reverse], [a5 reverse], [[a5 copy] autorelease], nil];
    id action3 = [CCRepeatForever actionWithAction:[CCSequence actions:a6, nil]];
    [Movie_Sprite[3] runAction:action3];     
    id a7 = [CCMoveBy actionWithDuration:2.4f position:ccp(0.0f,10.0f)];
    id a8 = [CCSequence actions: [[a7 copy] autorelease], [a7 reverse], [a7 reverse], [[a7 copy] autorelease], nil];
    id action4 = [CCRepeatForever actionWithAction:[CCSequence actions:a8, nil]];
    [Movie_Sprite[2] runAction:action4];
}

// 与吉ぱたぱた
-(void)scene7{
    id a1 = [CCRotateBy actionWithDuration:1.0f angle:-20.0f];
    id action1 = [CCRepeatForever actionWithAction:[CCSequence actions:a1, [a1 reverse], nil]];
    id a2 = [CCRotateBy actionWithDuration:1.0f angle:20.0f];
    id action2 = [CCRepeatForever actionWithAction:[CCSequence actions:a2, [a2 reverse], nil]];
    [Movie_Sprite[17] runAction:action2];
    [Movie_Sprite[18] runAction:action1];
}

//与吉が持っている魚
-(void)scene7_1{
    id a1 = [CCRotateBy actionWithDuration:1.0f angle:15];
    id a2 = [CCMoveBy actionWithDuration:1.0f position:ccp(-30.0f, -30.0f)];
    id a3 = [CCScaleBy actionWithDuration:0.1f scaleX:0.9f scaleY:1.0f];
    id a4 = [CCScaleBy actionWithDuration:0.1f scaleX:1.0f scaleY:0.9f];
    id a5 = [CCSpawn actions:[CCSequence actions:a3, [a3 reverse],[CCSequence actions:a4, [a4 reverse], nil], nil],nil];
    id a6 = [CCSequence actions:a5, a5, a5, a5, a5, nil];
    id action3 = [CCRepeatForever actionWithAction:[CCSpawn actions:[CCSequence actions:a1, [a1 reverse], nil],
                                                    [CCSequence actions:a2, [a2 reverse], nil], a6, nil]];
    [Movie_Sprite[0] runAction:action3];
}

// 初期配置の魚跳ね
-(void)scene7_2{
    id action = [CCJumpBy actionWithDuration:0.7f position:ccp(0,0) height:70.0f jumps:1];
    id a1 = [CCScaleBy actionWithDuration:0.1f scaleX:0.9f scaleY:1.0f];
    id action2 = [CCSequence actions:a1, [a1 reverse], nil];
    [Movie_Sprite[4] runAction:[CCRepeatForever actionWithAction:action]];
    [Movie_Sprite[3] runAction:[CCRepeatForever actionWithAction:action2]];
}

// 飛び出る魚
-(void)scene7_3{
    int fishRand = rand() % 100;
    
    if (touchCount == 10) {
        [self scene7_3_10];
    } else if (touchCount == 30) {
        [self scene7_3_11];
    } else {
        if (fishRand < 2 && (touchFlag == true)) {
            touchFlag = false;
            [self scene7_3_1];
        } else if (fishRand < 4 && touchFlag == true) {
            touchFlag = false;
            [self scene7_3_2];
        } else if (fishRand < 6 && touchFlag == true) {
            touchFlag = false;
            [self scene7_3_3];
        } else if (fishRand < 8 && touchFlag == true) {
            touchFlag = false;
            [self scene7_3_4];
        } else if (fishRand < 10 && touchFlag == true) {
            touchFlag = false;
            [self scene7_3_5];
        } else if (fishRand < 12 && touchFlag == true) {
            touchFlag = false;
            [self scene7_3_6];
        } else if (fishRand < 14 && touchFlag == true) {
            touchFlag = false;
            [self scene7_3_7];
        } else if (fishRand < 16 && touchFlag == true) {
            touchFlag = false;
            [self scene7_3_8];

        } else {
            [self scene7_3_9];
        }
    }
    
    touchCount++;
}

// 熱帯魚
-(void)scene7_3_1 {
    id action;
    id reset =  [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    float volume;
    
    if (rand() % 2) {
        Movie_Sprite[21].position = ccp(410.0f, 420.0f);
        action = [CCSequence actions:[CCSpawn actions:[CCJumpTo actionWithDuration:1.0f position:ccp(100.0f-rand()%50, -85.0f) height:450.0f+rand()%50 jumps:1], 
                                      [CCRotateBy actionWithDuration:1.0f angle:-150.0f-rand()%50], nil], reset, nil];
    } else {
        Movie_Sprite[21].position = ccp(650.0f, 420.0f);
        action = [CCSequence actions:[CCSpawn actions:[CCJumpTo actionWithDuration:1.0f position:ccp(1094.0f+rand()%50, 200.0f) height:350.0f+rand()%50 jumps:1], 
                                      [CCRotateBy actionWithDuration:1.0f angle:240.0f+rand()%50], nil], reset, nil];
    }
    
    [Movie_Sprite[21] runAction:action];
    
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.3;
    } else { // 停止中
        volume = 0.5;
    }
    
    [self playSound:1 :@"scene7_biyo1.mp3" :volume];
}

// タコ
-(void)scene7_3_2 {
    id action;
    id reset =  [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    float volume;
    
    NSString *path;
    path = [NSString stringWithFormat:@"scene7_biyo1.mp3"];
    id se = [CCCallFuncND actionWithTarget:self selector:@selector(playSE:data:) data:[path retain]];
    
    Movie_Sprite[22].position = ccp(510.0f, 600.0f);
    
    action = [CCSequence actions:[CCJumpTo actionWithDuration:1.0f position:ccp(510.0f, 260.0f) height:550.0f jumps:1], [CCDelayTime actionWithDuration:1.0f],
              se, [CCJumpTo actionWithDuration:1.0f position:ccp(510.0f, -102.0f) height:260.0f jumps:1], reset, nil];
    [Movie_Sprite[22] runAction:action];
    
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.3;
    } else { // 停止中
        volume = 0.5;
    }
    
    [self playSound:1 :@"scene7_biyo1.mp3" :volume];
}

// イカ
-(void)scene7_3_3 {
    id action;
    id reset =  [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    float volume;
    
    Movie_Sprite[23].position = ccp(510.0f, 600.0f);
    action = [CCSequence actions:[CCMoveTo actionWithDuration:0.5f position:ccp(955.0f, 270.0f)],[CCDelayTime actionWithDuration:1.3f], 
              [CCMoveTo actionWithDuration:0.5f position:ccp(955.0f, 900.0f)], reset, nil];
    
    [Movie_Sprite[23] runAction:action];
    
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.3;
    } else { // 停止中
        volume = 0.5;
    }
    
    [self playSound:1 :@"scene7_piyu4.mp3" :volume];
}

// シャチ
-(void)scene7_3_4{
    id action;
    id reset =  [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    float volume;
    
    if (rand() % 2) {
        Movie_Sprite[24].position = ccp(410.0f, 420.0f);
        action = [CCSequence actions:[CCSpawn actions:[CCJumpTo actionWithDuration:1.0f position:ccp(100.0f-rand()%50, -160.0f) height:450.0f+rand()%50 jumps:1], 
                                      [CCRotateBy actionWithDuration:1.0f angle:-150.0f-rand()%50], nil], reset, nil];
    } else {
        Movie_Sprite[24].position = ccp(650.0f, 420.0f);
        action = [CCSequence actions:[CCSpawn actions:[CCJumpTo actionWithDuration:1.0f position:ccp(1200.0f+rand()%50, 200.0f) height:350.0f+rand()%50 jumps:1], 
                                      [CCRotateBy actionWithDuration:1.0f angle:240.0f+rand()%50], nil], reset, nil];
    }
    
    [Movie_Sprite[24] runAction:action];
    
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.3;
    } else { // 停止中
        volume = 0.5;
    }
    
    [self playSound:1 :@"scene7_orca1.mp3" :volume];
    
}

// ウツボ？
-(void)scene7_3_5{
    id action;
    id reset =  [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    float volume;
    
    Movie_Sprite[25].position = ccp(650.0f, 420.0f);
    action = [CCSequence actions:[CCSpawn actions:[CCRotateBy actionWithDuration:2.0f angle:1800.0f], 
                                  [CCMoveTo actionWithDuration:2.0f position:ccp(1200.0f, 1200.0f)], nil], reset, nil];
    
    [Movie_Sprite[25] runAction:action];
    
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.3;
    } else { // 停止中
        volume = 0.5;
    }
    
    [self playSound:1 :@"scene7_comical2.mp3" :volume];
}

// カニ
-(void)scene7_3_6{
    id action;
    id reset =  [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    NSString *path;
    path = [NSString stringWithFormat:@"scene7_crab.mp3"];
    id se = [CCCallFuncND actionWithTarget:self selector:@selector(playSE:data:) data:[path retain]];
    
    Movie_Sprite[26].position = ccp(510.0f, 600.0f);
    action = [CCSequence actions:[CCJumpTo actionWithDuration:1.0f position:ccp(510.0f, 130.0f) height:450.0f jumps:1], 
              [CCDelayTime actionWithDuration:0.2f], se, [CCMoveTo actionWithDuration:0.3f position:ccp(710.0f, 130.0f)],
              [CCMoveTo actionWithDuration:0.2f position:ccp(710.0f, -100.0f)], reset, nil];
    
    [Movie_Sprite[26] runAction:action];
}

// イルカ
-(void)scene7_3_7{
    id action;
    id reset =  [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    float volume;
    
    if (rand() % 2) {
        Movie_Sprite[27].position = ccp(410.0f, 420.0f);
        action = [CCSequence actions:[CCSpawn actions:[CCJumpTo actionWithDuration:1.0f position:ccp(100.0f-rand()%50, -160.0f) height:450.0f+rand()%50 jumps:1], 
                                      [CCRotateBy actionWithDuration:1.0f angle:-150.0f-rand()%50], nil], reset, nil];
    } else {
        Movie_Sprite[27].position = ccp(650.0f, 420.0f);
        action = [CCSequence actions:[CCSpawn actions:[CCJumpTo actionWithDuration:1.0f position:ccp(1200.0f+rand()%50, 200.0f) height:350.0f+rand()%50 jumps:1], 
                                      [CCRotateBy actionWithDuration:1.0f angle:240.0f+rand()%50], nil], reset, nil];
    }
    
    [Movie_Sprite[27] runAction:action];
    
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.3;
    } else { // 停止中
        volume = 0.5;
    }
    
    [self playSound:1 :@"scene7_orca2.mp3" :volume];
}

// ネコ？
-(void)scene7_3_8{
    id action;
    id reset =  [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    float volume;
    
    if (rand() % 2) {
        Movie_Sprite[28].position = ccp(410.0f, 420.0f);
        action = [CCSequence actions:[CCRotateBy actionWithDuration:0.0f angle:-90.0f], 
                  [CCMoveTo actionWithDuration:1.0f position:ccp(-60.0f, 830.0f)], [CCRotateBy actionWithDuration:0.0f angle:90.0f], reset, nil];
    } else {
        Movie_Sprite[28].position = ccp(650.0f, 420.0f);
        action = [CCSequence actions:[CCMoveTo actionWithDuration:1.0f position:ccp(1200.0f, 830.0f)], reset, nil];
    }
    
    [Movie_Sprite[28] runAction:action];
    
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.3;
    } else { // 停止中
        volume = 0.5;
    }
    
    [self playSound:1 :@"scene7_cat.mp3" :volume];
}

// 普通の魚
-(void)scene7_3_9{
    id action;
    float volume;
    
    if (rand() % 2) {
        Fish_Sprite[touchCount%20].position = ccp(410.0f, 420.0f);
        action = [CCSpawn actions:[CCJumpTo actionWithDuration:1.0f position:ccp(100.0f-rand()%50, -75.0f) height:450.0f+rand()%50 jumps:1], 
                  [CCRotateBy actionWithDuration:1.0f angle:-150.0f-rand()%50], nil];
        [Fish_Sprite[touchCount % 20] runAction:action];
    } else {
        Fish_Sprite[touchCount%20].position = ccp(650.0f, 420.0f);
        action = [CCSpawn actions:[CCJumpTo actionWithDuration:1.0f position:ccp(1094.0f+rand()%50, 200.0f) height:350.0f+rand()%50 jumps:1], 
                  [CCRotateBy actionWithDuration:1.0f angle:240.0f+rand()%50], nil];
        [Fish_Sprite[touchCount % 20] runAction:action];
    }
    
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.3;
    } else { // 停止中
        volume = 0.5;
    }
    
    [self playSound:0 :@"scene7_sakana.mp3" :volume];
}

-(void)scene7_3_10{
    float volume;
    id action, action2;
    
    Movie_Sprite[2].position = ccp(410.0f, 420.0f);
    action = [CCSpawn actions:[CCJumpTo actionWithDuration:1.0f position:ccp(100.0f, 320.0f) height:450.0f jumps:1], 
              [CCRotateBy actionWithDuration:1.0f angle:-360.0f], nil];
    action2 = [CCSequence actions:[CCDelayTime actionWithDuration:0.7f], [CCMoveBy actionWithDuration:0.3f position:ccp(0.0f, 5.0f)], nil];
    [Movie_Sprite[12] runAction:action2];
    [Movie_Sprite[15] runAction:[[action2 copy] autorelease]];
    [Movie_Sprite[2] runAction:action];
    
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.3;
    } else { // 停止中
        volume = 0.5;
    }
    
    [self playSound:0 :@"scene7_sakana.mp3" :volume];
}

-(void)scene7_3_11{
    id action, action2;
    float volume;
    
    Movie_Sprite[1].position = ccp(650.0f, 420.f);
    action = [CCSpawn actions:[CCJumpTo actionWithDuration:1.0f position:ccp(825.0f, 420.0f) height:450.0f jumps:1], 
              [CCRotateBy actionWithDuration:1.0f angle:-330.0f], nil];
    action2 = [CCSequence actions:[CCDelayTime actionWithDuration:0.7f], [CCMoveBy actionWithDuration:0.3f position:ccp(0.0f, 5.0f)], nil];
    [Movie_Sprite[5] runAction:action2];
    [Movie_Sprite[7] runAction:[[action2 copy] autorelease]];
    [Movie_Sprite[1] runAction:action];
    
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.3;
    } else { // 停止中
        volume = 0.5;
    }
    
    [self playSound:0 :@"scene7_sakana.mp3" :volume];
}

//シーン8の海と船の動き
-(void)scene8_1{
    //船ぷかぷか
    id a1 = [CCMoveBy actionWithDuration:1.0f position:ccp(0.0f,10.0f)];
    id a2 = [CCSequence actions: [[a1 copy] autorelease], [a1 reverse], [a1 reverse], [[a1 copy] autorelease], nil];
    id action1 = [CCRepeatForever actionWithAction:[CCSequence actions:a2, nil]];
    id action2 = [[action1 copy] autorelease];
    id action3 = [[action1 copy] autorelease];
    id action4 = [[action1 copy] autorelease];
    id action5 = [[action1 copy] autorelease];
    id action6 = [[action1 copy] autorelease];
    id action7 = [[action1 copy] autorelease];
    [Movie_Sprite[9] runAction:action1];
    [Movie_Sprite[8] runAction:action2];
    [Movie_Sprite[7] runAction:action3];
    [Movie_Sprite[6] runAction:action4];
    [Movie_Sprite[5] runAction:action5];
    [Movie_Sprite[4] runAction:action6];
    [Movie_Sprite[3] runAction:action7];
    
    //波
    id a3 = [CCMoveBy actionWithDuration:2.0f position:ccp(0.0f,10.0f)];
    id a4 = [CCSequence actions: [[a3 copy] autorelease], [a3 reverse], [a3 reverse], [[a3 copy] autorelease], nil];
    id action8 = [CCRepeatForever actionWithAction:[CCSequence actions:a4, nil]];
    [Movie_Sprite[15] runAction:action8];
    id a5 = [CCMoveBy actionWithDuration:1.8f position:ccp(0.0f,10.0f)];
    id a6 = [CCSequence actions: [[a5 copy] autorelease], [a5 reverse], [a5 reverse], [[a5 copy] autorelease], nil];
    id action9 = [CCRepeatForever actionWithAction:[CCSequence actions:a6, nil]];
    [Movie_Sprite[13] runAction:action9];
    id a7 = [CCMoveBy actionWithDuration:1.6f position:ccp(0.0f,10.0f)];
    id a8 = [CCSequence actions: [[a7 copy] autorelease], [a7 reverse], [a7 reverse], [[a7 copy] autorelease], nil];
    id action10 = [CCRepeatForever actionWithAction:[CCSequence actions:a8, nil]];
    [Movie_Sprite[12] runAction:action10];
    id a9 = [CCMoveBy actionWithDuration:1.4f position:ccp(0.0f,10.0f)];
    id a10 = [CCSequence actions: [[a9 copy] autorelease], [a9 reverse], [a9 reverse], [[a9 copy] autorelease], nil];
    id action11 = [CCRepeatForever actionWithAction:[CCSequence actions:a10, nil]];
    [Movie_Sprite[11] runAction:action11];
    id a11 = [CCMoveBy actionWithDuration:1.2f position:ccp(0.0f,10.0f)];
    id a12 = [CCSequence actions: [[a11 copy] autorelease], [a11 reverse], [a11 reverse], [[a11 copy] autorelease], nil];
    id action12 = [CCRepeatForever actionWithAction:[CCSequence actions:a12, nil]];
    [Movie_Sprite[10] runAction:action12];
}

//ぐおー
-(void)scene8_2{
    NSString *path1;
    path1 = [NSString stringWithFormat:@"scene8_guo.mp3"];
    id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    id se1 = [CCCallFuncND actionWithTarget:self selector:@selector(playSE:data:) data:[path1 retain]];
    id a1 = [CCRotateBy actionWithDuration:1.0f angle:-30];
    id a2 = [CCScaleBy actionWithDuration:0.0f scale:0.1f];
    id a3 = [CCMoveTo actionWithDuration:0.0f position:ccp(340.0f,360.0f)];
    id a4 = [CCFadeIn actionWithDuration:2.0f];
    id a5 = [CCMoveBy actionWithDuration:2.0f position:ccp(300.0f, 145.f)];
    id a6 = [CCScaleBy actionWithDuration:2.0f scale:10.0f];
    id a7 = [CCSpawn actions:a4, a5, a6, nil];
    id action2 = [CCSequence actions:a2, a3, a7, [a4 reverse], reset, nil];
    id action1 = [CCSequence actions:a1, se1,[a1 reverse], nil];
    [Movie_Sprite[4] runAction:action1];
    [Movie_Sprite[0] runAction:action2];
}

//あひゃひゃorくすぐったいな
-(void)scene8_3{
    
    NSString *path2;
    NSString *path3;
    
    path2 = [NSString stringWithFormat:@"scene8_hyahya.mp3"];
    path3 = [NSString stringWithFormat:@"scene8_kusuguttai.mp3"];
    
    id se2 = [CCCallFuncND actionWithTarget:self selector:@selector(playSE:data:) data:[path2 retain]];
    id se3 = [CCCallFuncND actionWithTarget:self selector:@selector(playSE:data:) data:[path3 retain]];
    id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    id a1 = [CCRotateBy actionWithDuration:0.1f angle:10];
    id a2 = [CCScaleBy actionWithDuration:0.0f scale:0.1f];
    id a3 = [CCMoveTo actionWithDuration:0.0f position:ccp(340.0f,360.0f)];
    id a4 = [CCFadeIn actionWithDuration:1.5f];
    id a5 = [CCMoveBy actionWithDuration:1.5f position:ccp(300.0f, 145.f)];
    id a6 = [CCScaleBy actionWithDuration:1.5f scale:10.0f];
    id a7 = [CCSpawn actions:a4, a5, a6, nil];
    id a8 = [CCRotateBy actionWithDuration:0.1f angle:-10];
    id action2 = [CCSequence actions:a2, a3, a7, [a4 reverse], reset, nil];
    id action1 = [CCSequence actions:a1, se3,[a1 reverse], nil];
    id action3 = [CCSequence actions:a8, [a8 reverse], nil];
    id action4 = [CCSequence actions:a1, se2,[a1 reverse], nil];
    
    //ひゃひゃとくすぐったいな交互用
    touchCount++;
    if(touchCount % 3 == 0){
        [Movie_Sprite[1] runAction:action2];
        [Movie_Sprite[3] runAction:action1];
        [Movie_Sprite[6] runAction:action3];
    }else{
        [Movie_Sprite[2] runAction:action2];
        [Movie_Sprite[3] runAction:action4];
        [Movie_Sprite[6] runAction:action3];
    }
    
}

// シーン9ぐおー
-(void)scene9:(float) x:(float) y{
    id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    // ぐおーの重力リセット
    id greset = [CCCallFunc actionWithTarget:self selector:@selector(gravityReset)];
    
    // ぐおーの位置をタッチされた場所へ
    Movie_Sprite[10].position = ccp(x, y);
    
    double x1, y1;
    
    // タッチされた座標と対象の角へ座標をセット
    if (512 > x) {
        x1 = 1024 + 768;
    } else {
        x1 = -768;
    }
    
    if (384 > y) {
        y1 = 768 + 230;
    } else {
        y1 = -230;
    }
    
    // ぐおーを飛ばす
    id a1 = [CCMoveTo actionWithDuration:1.0f position:ccp(x1, y1)];
    id action = [CCSequence actions:[CCShow action], a1, greset, nil];
    
    double x2 = 512.0 - x;
    double y2 = 384.0 - y;
    
    // ぐおー中は重力を変更
    b2Vec2 gravity(x2, y2);
    world->SetGravity(gravity);
    
    // 背景の鶏の足伸ばし
    id a2 = [CCMoveBy actionWithDuration:0.5f position:ccp(0.0f, 70.0f)];
    id action2 = [CCSequence actions:a2, [a2 reverse], reset, nil];
    
    float volume;
    
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.3;
    } else { // 停止中
        volume = 0.5;
    }
    
    [self playSound:0 :@"scene9_baby4.mp3" :volume];
    [self playSound:1 :@"scene9_ibiki_9.mp3" :volume];
    [self playSound:2 :@"scene9_niwatori1.mp3" :volume];
    [self playSound:3 :@"scene9_niwatori2.mp3" :volume];
    
    [Movie_Sprite[10] runAction:action];
    [Movie_Sprite[3] runAction:action2];
}

// 重力を元に戻す
-(void)gravityReset{
    b2Vec2 gravity(0.0f, -10.0f);
    world->SetGravity( gravity );
}

//10ページの波と船の動き
-(void)scene10_1{    
    //波
    id a1 = [CCRotateBy actionWithDuration:1.2f angle:5];
    id a2 = [CCRotateBy actionWithDuration:1.0f angle:10];
    id a3 = [CCMoveBy actionWithDuration:2.0f position:ccp(0.0f,10.0f)];
    id a4 = [CCSequence actions: [[a3 copy] autorelease], [a3 reverse], [a3 reverse], [[a3 copy] autorelease], nil];
    id action9 = [CCRepeatForever actionWithAction:[CCSequence actions:a4, nil]];
    [Movie_Sprite[30] runAction:action9];
    id a5 = [CCMoveBy actionWithDuration:1.8f position:ccp(0.0f,10.0f)];
    id a6 = [CCSequence actions: [[a5 copy] autorelease], [a5 reverse], [a5 reverse], [[a5 copy] autorelease], nil];
    id action10 = [CCRepeatForever actionWithAction:[CCSequence actions:a6, nil]];
    [Movie_Sprite[29] runAction:action10];
    id a7 = [CCMoveBy actionWithDuration:1.6f position:ccp(0.0f,10.0f)];
    id a8 = [CCSequence actions: [[a7 copy] autorelease], [a7 reverse], [a7 reverse], [[a7 copy] autorelease], nil];
    id action11 = [CCRepeatForever actionWithAction:[CCSequence actions:a8, nil]];
    [Movie_Sprite[28] runAction:action11];
    id a9 = [CCMoveBy actionWithDuration:1.4f position:ccp(0.0f,10.0f)];
    id a10 = [CCSequence actions: [[a9 copy] autorelease], [a9 reverse], [a9 reverse], [[a9 copy] autorelease], nil];
    id action12 = [CCRepeatForever actionWithAction:[CCSequence actions:a10, nil]];
    id action13 = [[action12 copy] autorelease];
    [Movie_Sprite[27] runAction:action12];
    [Movie_Sprite[5] runAction:action13];
    id a11 = [CCMoveBy actionWithDuration:1.2f position:ccp(0.0f,10.0f)];
    id spawn1 = [CCSpawn actions:a11, a1, nil];
    id a12 = [CCSequence actions: [[spawn1 copy] autorelease], [spawn1 reverse], [spawn1 reverse], [[spawn1 copy] autorelease], nil];
    id action14 = [CCRepeatForever actionWithAction:[CCSequence actions:a12, nil]];
    [Movie_Sprite[3] runAction:action14];
    id a13 = [CCMoveBy actionWithDuration:1.0f position:ccp(0.0f,10.0f)];
    id spawn2 = [CCSpawn actions:a13, a2, nil];
    id a14 = [CCSequence actions: [[spawn2 copy] autorelease], [spawn2 reverse], [spawn2 reverse], [[spawn2 copy] autorelease], nil];
    id action15 = [CCRepeatForever actionWithAction:[CCSequence actions:a14, nil]];
    [Movie_Sprite[2] runAction:action15];
    id a15 = [CCMoveBy actionWithDuration:0.8f position:ccp(0.0f,10.0f)];
    id a16 = [CCSequence actions: [[a15 copy] autorelease], [a15 reverse], [a15 reverse], [[a15 copy] autorelease], nil];
    id action16 = [CCRepeatForever actionWithAction:[CCSequence actions:a16, nil]];
    [Movie_Sprite[1] runAction:action16];
}


-(void)scene10_sound {
    float volume = 0.5;
    
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.5;
    } else { // 停止中
        volume = 1.0;
    }
    [self playSound:0 :@"scene8_guo.mp3" :volume];
}

-(void)scene10_2{
    //シーン10タッチしてぐおー
    
    id se1 = [CCCallFunc actionWithTarget:self selector:@selector(scene10_sound)];
    id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    id a2 = [CCScaleBy actionWithDuration:0.0f scale:0.1f];
    id a3 = [CCMoveTo actionWithDuration:0.0f position:ccp(280.0f,360.0f)];
    id a4 = [CCFadeIn actionWithDuration:1.0f];
    id a5 = [CCMoveBy actionWithDuration:1.0f position:ccp(1200.0f, 580.f)];
    id a6 = [CCScaleBy actionWithDuration:0.5f scale:10.0f];
    id a7 = [CCSpawn actions:a4, a5, a6, nil];
    id action1 = [CCSequence actions:a2, a3, se1, a7, nil];
    id action23 = [CCSequence actions:[CCDelayTime actionWithDuration:7.5f],[[action1 copy] autorelease], nil];
    id action24 = [CCSequence actions:[CCDelayTime actionWithDuration:10.5f],[[action1 copy] autorelease], nil];
    //id action25 = [CCSequence actions:[CCDelayTime actionWithDuration:21.0f],[[action1 copy] autorelease], nil];
    [Movie_Sprite[0] runAction:action1];
    [Movie_Sprite[0] runAction:action23];
    [Movie_Sprite[0] runAction:action24];
    //[Movie_Sprite[0] runAction:action25];
    //竹島ぶるぶる
    id a1 = [CCSequence actions:[CCDelayTime actionWithDuration:0.5],[CCShaky3D actionWithRange:3 shakeZ:NO grid:ccg(50, 20) duration:2.0f],nil];
    [Movie_Sprite[4] runAction:a1];
    id a52 = [CCSequence actions:[CCDelayTime actionWithDuration:7.7f],[[a1 copy] autorelease], nil];
    [Movie_Sprite[4] runAction:a52];
    id a53 = [CCSequence actions:[CCDelayTime actionWithDuration:10.7f],[[a1 copy] autorelease], nil];
    [Movie_Sprite[4] runAction:a53];
    /*id a54 = [CCSequence actions:[CCDelayTime actionWithDuration:20.5f],[[a1 copy] autorelease], reset, nil];
    [Movie_Sprite[4] runAction:a54];*/
    //鳥その1(小)の出現
    id a8 = [CCShow action];
    id a9 = [CCHide action];
    id fadein = [CCFadeIn actionWithDuration:0.0f];
    //id fadein2 = [CCFadeIn actionWithDuration:0.5f];
    //id a16 = [CCScaleBy actionWithDuration:0.0f scale:0.5f];        //拡大用に少し小さく表示しておく
    //id a17 = [CCScaleBy actionWithDuration:1.0f scale:1.5f];        //元の大きさにする
//    id a10 = [CCMoveTo actionWithDuration:0.0f position:ccp(674.0f, 393.0f)];
//    id fadeout = [CCFadeOut actionWithDuration:0.5];
//    id action2 = [CCSequence actions:[CCDelayTime actionWithDuration:0.2f], a9, a10, [CCBlink actionWithDuration:7.0f blinks:7], fadeout, a8, nil];
//    [Movie_Sprite[12] runAction:action2];
//    id a12 = [CCMoveTo actionWithDuration:0.0f position:ccp(696.0f, 407.0f)];
//    id action3 = [CCSequence actions:[CCDelayTime actionWithDuration:0.4f], [[a9 copy] autorelease], a12, [CCBlink actionWithDuration:7.0f blinks:6], [[fadeout copy] autorelease], [[a8 copy] autorelease], nil];
//    [Movie_Sprite[11] runAction:action3];
//    id a14 = [CCMoveTo actionWithDuration:0.0f position:ccp(661.0f, 414.0f)];
//    id action4 = [CCSequence actions:[CCDelayTime actionWithDuration:0.7f], [[a9 copy] autorelease], a14, [CCBlink actionWithDuration:7.0f blinks:5], [[fadeout copy] autorelease], [[a8 copy] autorelease], nil];
//    [Movie_Sprite[10] runAction:action4];
    //旧鳥その1(小)の出現
    id a10 = [CCMoveTo actionWithDuration:0.0f position:ccp(674.0f, 393.0f)];
    id a11 = [CCFadeOut actionWithDuration:0.5];
    id action2 = [CCSequence actions:[CCDelayTime actionWithDuration:0.2f], [[a9 copy] autorelease], a10, [[a8 copy] autorelease], a11, nil];
    [Movie_Sprite[12] runAction:action2];
    id a12 = [CCMoveTo actionWithDuration:0.0f position:ccp(696.0f, 407.0f)];
    id a13 = [CCFadeOut actionWithDuration:1.0];
    id action3 = [CCSequence actions:[CCDelayTime actionWithDuration:0.7f], [[a9 copy] autorelease], a12, [[a8 copy] autorelease], a13, nil];
    [Movie_Sprite[11] runAction:action3];
    id a14 = [CCMoveTo actionWithDuration:0.0f position:ccp(661.0f, 414.0f)];
    id a15 = [CCFadeOut actionWithDuration:1.0];
    id action4 = [CCSequence actions:[CCDelayTime actionWithDuration:1.2f], [[a9 copy] autorelease], a14, [[a8 copy] autorelease], a15, nil];
    [Movie_Sprite[10] runAction:action4];
    id a16 = [CCMoveTo actionWithDuration:0.0f position:ccp(687.0f, 430.0f)];
    id a17 = [CCFadeOut actionWithDuration:1.0];
    id action5 = [CCSequence actions:[CCDelayTime actionWithDuration:1.7f], [[a9 copy] autorelease], a16, [[a8 copy] autorelease], a17, nil];
    [Movie_Sprite[9] runAction:action5];
    id a18 = [CCMoveTo actionWithDuration:0.0f position:ccp(708.0f, 447.0f)];
    id a19 = [CCFadeOut actionWithDuration:1.0];
    id action6 = [CCSequence actions:[CCDelayTime actionWithDuration:2.2f], [[a9 copy] autorelease], a18, [[a8 copy] autorelease], a19, nil];
    [Movie_Sprite[8] runAction:action6];
    id a20 = [CCMoveTo actionWithDuration:0.0f position:ccp(650.0f, 440.0f)];
    id a21 = [CCFadeOut actionWithDuration:1.0];
    id action7 = [CCSequence actions:[CCDelayTime actionWithDuration:2.7f], [[a9 copy] autorelease], a20, [[a8 copy] autorelease], a21, nil];
    [Movie_Sprite[7] runAction:action7];
    id a22 = [CCMoveTo actionWithDuration:0.0f position:ccp(679.0f, 464.0f)];
    id a23 = [CCFadeOut actionWithDuration:1.0];
    id action8 = [CCSequence actions:[CCDelayTime actionWithDuration:3.2f], [[a9 copy] autorelease], a22, [[a8 copy] autorelease], a23, nil];
    [Movie_Sprite[6] runAction:action8];
    //鳥その1(中)の出現
    id a24 = [CCMoveTo actionWithDuration:0.0f position:ccp(762.0f, 404.0f)];
    id a25 = [CCFadeOut actionWithDuration:1.0];
    id action9 = [CCSequence actions:[CCDelayTime actionWithDuration:4.0f], [[a9 copy] autorelease], a24, [[a8 copy] autorelease], a25, nil];
    [Movie_Sprite[19] runAction:action9];
    id a26 = [CCMoveTo actionWithDuration:0.0f position:ccp(778.0f, 428.0f)];
    id a27 = [CCFadeOut actionWithDuration:1.0];
    id action10 = [CCSequence actions:[CCDelayTime actionWithDuration:4.5f], [[a9 copy] autorelease], a26, [[a8 copy] autorelease], a27, nil];
    [Movie_Sprite[18] runAction:action10];
    id a28 = [CCMoveTo actionWithDuration:0.0f position:ccp(703.0f, 424.0f)];
    id a29 = [CCFadeOut actionWithDuration:1.0];
    id action11 = [CCSequence actions:[CCDelayTime actionWithDuration:5.0f], [[a9 copy] autorelease], a28, [[a8 copy] autorelease], a29, nil];
    [Movie_Sprite[17] runAction:action11];
    id a30 = [CCMoveTo actionWithDuration:0.0f position:ccp(744.0f, 454.0f)];
    id a31 = [CCFadeOut actionWithDuration:1.0];
    id action12 = [CCSequence actions:[CCDelayTime actionWithDuration:5.5f], [[a9 copy] autorelease], a30, [[a8 copy] autorelease], a31, nil];
    [Movie_Sprite[16] runAction:action12];
    id a32 = [CCMoveTo actionWithDuration:0.0f position:ccp(803.0f, 473.0f)];
    id a33 = [CCFadeOut actionWithDuration:1.0];
    id action13 = [CCSequence actions:[CCDelayTime actionWithDuration:6.0f], [[a9 copy] autorelease], a32, [[a8 copy] autorelease], a33, nil];
    [Movie_Sprite[15] runAction:action13];
    id a34 = [CCMoveTo actionWithDuration:0.0f position:ccp(677.0f, 458.0f)];
    id a35 = [CCFadeOut actionWithDuration:1.0];
    id action14 = [CCSequence actions:[CCDelayTime actionWithDuration:6.5f], [[a9 copy] autorelease], a34, [[a8 copy] autorelease], a35, nil];
    [Movie_Sprite[14] runAction:action14];
    id a36 = [CCMoveTo actionWithDuration:0.0f position:ccp(732.0f, 490.0f)];
    id a37 = [CCFadeOut actionWithDuration:1.0];
    id action15 = [CCSequence actions:[CCDelayTime actionWithDuration:7.0f], [[a9 copy] autorelease], a36, [[a8 copy] autorelease], a37, nil];
    [Movie_Sprite[13] runAction:action15];
    //鳥その1(大)の出現
    id a38 = [CCMoveTo actionWithDuration:0.0f position:ccp(699.0f, 501.0f)];
    id a39 = [CCFadeOut actionWithDuration:1.0];
    id action16 = [CCSequence actions:[CCDelayTime actionWithDuration:7.5f], [[a9 copy] autorelease], a38, [[a8 copy] autorelease], a39, nil];
    [Movie_Sprite[26] runAction:action16];
    id a40 = [CCMoveTo actionWithDuration:0.0f position:ccp(584.0f, 438.0f)];
    id a41 = [CCFadeOut actionWithDuration:1.0];
    id action17 = [CCSequence actions:[CCDelayTime actionWithDuration:8.0f], [[a9 copy] autorelease], a40, [[a8 copy] autorelease], a41, nil];
    [Movie_Sprite[25] runAction:action17];
    id a42 = [CCMoveTo actionWithDuration:0.0f position:ccp(824.0f, 472.0f)];
    id a43 = [CCFadeOut actionWithDuration:1.0];
    id action18 = [CCSequence actions:[CCDelayTime actionWithDuration:8.5f], [[a9 copy] autorelease], a42, [[a8 copy] autorelease], a43, nil];
    [Movie_Sprite[24] runAction:action18];
    id a44 = [CCMoveTo actionWithDuration:0.0f position:ccp(726.0f, 430.0f)];
    id a45 = [CCFadeOut actionWithDuration:1.0];
    id action19 = [CCSequence actions:[CCDelayTime actionWithDuration:9.0f], [[a9 copy] autorelease], a44, [[a8 copy] autorelease], a45, nil];
    [Movie_Sprite[23] runAction:action19];
    id a46 = [CCMoveTo actionWithDuration:0.0f position:ccp(790.0f, 394.0f)];
    id a47 = [CCFadeOut actionWithDuration:1.0];
    id action20 = [CCSequence actions:[CCDelayTime actionWithDuration:9.5f], [[a9 copy] autorelease], a46, [[a8 copy] autorelease], a47, nil];
    [Movie_Sprite[22] runAction:action20];
    id a48 = [CCMoveTo actionWithDuration:0.0f position:ccp(675.0f, 392.0f)];
    id a49 = [CCFadeOut actionWithDuration:1.0];
    id action21 = [CCSequence actions:[CCDelayTime actionWithDuration:10.0f], [[a9 copy] autorelease], a48, [[a8 copy] autorelease], a49, nil];
    [Movie_Sprite[21] runAction:action21];
    id a50 = [CCMoveTo actionWithDuration:0.0f position:ccp(764.0f, 342.0f)];
    id a51 = [CCEaseExponentialIn actionWithAction:[CCMoveTo actionWithDuration:1.0f position:ccp(764.0f, -1000.0f)]];
    id a56 = [CCRotateBy actionWithDuration:1.0f angle:90];
    id a57 = [CCSpawn actions:a51, a56, nil];
    id action22 = [CCSequence actions:[CCDelayTime actionWithDuration:10.5f], [[a9 copy] autorelease], a50, [[a8 copy] autorelease], [CCDelayTime actionWithDuration:0.1f], a57,[a56 reverse], nil];
    [Movie_Sprite[20] runAction:action22];
    //鳥その2(小)の出現
    id a58 = [CCMoveTo actionWithDuration:0.0f position:ccp(624.0f, 343.0f)];
    id a59 = [CCFadeOut actionWithDuration:1.0];
    id action26 = [CCSequence actions:[CCDelayTime actionWithDuration:4.5f], [[a9 copy] autorelease], a58, [[a8 copy] autorelease], a59, nil];
    [Movie_Sprite[12] runAction:action26];
    id a60 = [CCMoveTo actionWithDuration:0.0f position:ccp(646.0f, 357.0f)];
    id a61 = [CCFadeOut actionWithDuration:1.0];
    id action27 = [CCSequence actions:[CCDelayTime actionWithDuration:5.0f], [[a9 copy] autorelease], a60, [[a8 copy] autorelease], a61, nil];
    [Movie_Sprite[11] runAction:action27];
    id a62 = [CCMoveTo actionWithDuration:0.0f position:ccp(611.0f, 364.0f)];
    id a63 = [CCFadeOut actionWithDuration:1.0];
    id action28 = [CCSequence actions:[CCDelayTime actionWithDuration:5.5f], [[a9 copy] autorelease], a62, [[a8 copy] autorelease], a63, nil];
    [Movie_Sprite[10] runAction:action28];
    id a64 = [CCMoveTo actionWithDuration:0.0f position:ccp(637.0f, 380.0f)];
    id a65 = [CCFadeOut actionWithDuration:1.0];
    id action29 = [CCSequence actions:[CCDelayTime actionWithDuration:6.0f], [[a9 copy] autorelease], a64, [[a8 copy] autorelease], a65, nil];
    [Movie_Sprite[9] runAction:action29];
    id a66 = [CCMoveTo actionWithDuration:0.0f position:ccp(658.0f, 397.0f)];
    id a67 = [CCFadeOut actionWithDuration:1.0];
    id action30 = [CCSequence actions:[CCDelayTime actionWithDuration:6.5f], [[a9 copy] autorelease], a66, [[a8 copy] autorelease], a67, nil];
    [Movie_Sprite[8] runAction:action30];
    id a68 = [CCMoveTo actionWithDuration:0.0f position:ccp(600.0f, 390.0f)];
    id a69 = [CCFadeOut actionWithDuration:1.0];
    id action31 = [CCSequence actions:[CCDelayTime actionWithDuration:7.0f], [[a9 copy] autorelease], a68, [[a8 copy] autorelease], a69, nil];
    [Movie_Sprite[7] runAction:action31];
    id a70 = [CCMoveTo actionWithDuration:0.0f position:ccp(629.0f, 414.0f)];
    id a71 = [CCFadeOut actionWithDuration:1.0];
    id action32 = [CCSequence actions:[CCDelayTime actionWithDuration:7.5f], [[a9 copy] autorelease], a70, [[a8 copy] autorelease], a71, nil];
    [Movie_Sprite[6] runAction:action32];
    //鳥その２(中)の出現
    id a72 = [CCMoveTo actionWithDuration:0.0f position:ccp(712.0f, 354.0f)];
    id a73 = [CCFadeOut actionWithDuration:1.0];
    id action33 = [CCSequence actions:[CCDelayTime actionWithDuration:8.0f], [[a9 copy] autorelease], a72, [[a8 copy] autorelease], a73, nil];
    [Movie_Sprite[19] runAction:action33];
    id a74 = [CCMoveTo actionWithDuration:0.0f position:ccp(728.0f, 378.0f)];
    id a75 = [CCFadeOut actionWithDuration:1.0];
    id action34 = [CCSequence actions:[CCDelayTime actionWithDuration:8.5f], [[a9 copy] autorelease], a74, [[a8 copy] autorelease],  a75, nil];
    [Movie_Sprite[18] runAction:action34];
    id a76 = [CCMoveTo actionWithDuration:0.0f position:ccp(653.0f, 374.0f)];
    id a77 = [CCFadeOut actionWithDuration:1.0];
    id action35 = [CCSequence actions:[CCDelayTime actionWithDuration:9.0f], [[a9 copy] autorelease], a76, [[a8 copy] autorelease], a77, nil];
    [Movie_Sprite[17] runAction:action35];
    id a78 = [CCMoveTo actionWithDuration:0.0f position:ccp(694.0f, 404.0f)];
    id a79 = [CCFadeOut actionWithDuration:1.0];
    id action36 = [CCSequence actions:[CCDelayTime actionWithDuration:9.5f], [[a9 copy] autorelease], a78, [[a8 copy] autorelease], a79, nil];
    [Movie_Sprite[16] runAction:action36];
    id a80 = [CCMoveTo actionWithDuration:0.0f position:ccp(753.0f, 423.0f)];
    id a81 = [CCFadeOut actionWithDuration:1.0];
    id action37 = [CCSequence actions:[CCDelayTime actionWithDuration:10.0f], [[a9 copy] autorelease], a80, [[a8 copy] autorelease], a81, nil];
    [Movie_Sprite[15] runAction:action37];
    id a82 = [CCMoveTo actionWithDuration:0.0f position:ccp(627.0f, 408.0f)];
    id a83 = [CCFadeOut actionWithDuration:1.0];
    id action38 = [CCSequence actions:[CCDelayTime actionWithDuration:10.5f], [[a9 copy] autorelease], a82, [[a8 copy] autorelease], a83, nil];
    [Movie_Sprite[14] runAction:action38];
    id a84 = [CCMoveTo actionWithDuration:0.0f position:ccp(682.0f, 440.0f)];
    //id a85 = [CCFadeOut actionWithDuration:1.0];
    id a86 = [CCEaseExponentialIn actionWithAction:[CCMoveTo actionWithDuration:1.0f position:ccp(682.0f, -1000.0f)]];
    id hall = [CCRotateBy actionWithDuration:1.0f angle:-90];
    id a87 = [CCSpawn actions:a86, hall, nil];
    //id a000 = [CCCallFunc actionWithTarget:self selector:@selector(scenelog)];
    id action39 = [CCSequence actions:[CCDelayTime actionWithDuration:11.0f], [[a9 copy] autorelease], a84,  fadein, [[a8 copy] autorelease], [CCDelayTime actionWithDuration:0.1f], a87, [hall reverse], nil];    
    [Movie_Sprite[13] runAction:action39];
    //鳥その3(小)の出現
    id a88 = [CCMoveTo actionWithDuration:0.0f position:ccp(724.0f, 443.0f)];
    id a89 = [CCFadeOut actionWithDuration:1.0];
    id action40 = [CCSequence actions:[CCDelayTime actionWithDuration:7.0f], [[a9 copy] autorelease], a88, [[a8 copy] autorelease], a89, nil];
    [Movie_Sprite[12] runAction:action40];
    id a90 = [CCMoveTo actionWithDuration:0.0f position:ccp(746.0f, 457.0)];
    id a91 = [CCFadeOut actionWithDuration:1.0];
    id action41 = [CCSequence actions:[CCDelayTime actionWithDuration:7.5f], [[a9 copy] autorelease], a90, [[a8 copy] autorelease], a91, nil];
    [Movie_Sprite[11] runAction:action41];
    id a92 = [CCMoveTo actionWithDuration:0.0f position:ccp(711.0f, 464.0f)];
    id a93 = [CCFadeOut actionWithDuration:1.0];
    id action42 = [CCSequence actions:[CCDelayTime actionWithDuration:8.0f], [[a9 copy] autorelease], a92, [[a8 copy] autorelease], a93, nil];
    [Movie_Sprite[10] runAction:action42];
    id a94 = [CCMoveTo actionWithDuration:0.0f position:ccp(737.0f, 480.0f)];
    id a95 = [CCFadeOut actionWithDuration:1.0];
    id action43 = [CCSequence actions:[CCDelayTime actionWithDuration:8.5f], [[a9 copy] autorelease], a94, [[a8 copy] autorelease], a95, nil];
    [Movie_Sprite[9] runAction:action43];
    id a96 = [CCMoveTo actionWithDuration:0.0f position:ccp(758.0f, 497.0f)];
    id a97 = [CCFadeOut actionWithDuration:1.0];
    id action44 = [CCSequence actions:[CCDelayTime actionWithDuration:9.0f], [[a9 copy] autorelease], a96, [[a8 copy] autorelease], a97, nil];
    [Movie_Sprite[8] runAction:action44];
    id a98 = [CCMoveTo actionWithDuration:0.0f position:ccp(700.0f, 490.0f)];
    id a99 = [CCFadeOut actionWithDuration:1.0];
    id action45 = [CCSequence actions:[CCDelayTime actionWithDuration:9.5f], [[a9 copy] autorelease], a98, [[a8 copy] autorelease], a99, nil];
    [Movie_Sprite[7] runAction:action45];
    id a100 = [CCMoveTo actionWithDuration:0.0f position:ccp(729.0f, 514.0f)];
    //id a85 = [CCFadeOut actionWithDuration:1.0];
    //id a101 = [CCEaseExponentialIn actionWithAction:[CCMoveTo actionWithDuration:1.0f position:ccp(682.0f, -1000.0f)]];
    //id a102 = [CCFadeOut actionWithDuration:1.0];
    //id a000 = [CCCallFunc actionWithTarget:self selector:@selector(scenelog)];
    id action46 = [CCSequence actions:[CCDelayTime actionWithDuration:10.0f], [[a9 copy] autorelease], a100,  [[fadein copy] autorelease], [[a8 copy] autorelease], [CCDelayTime actionWithDuration:0.7f], [CCMoveBy actionWithDuration:0.8 position:ccp(1000,1000)], reset, nil];
    [Movie_Sprite[6] runAction:action46];
    //鳥その4(中)出現
    id a103 = [CCMoveTo actionWithDuration:0.0f position:ccp(762.0f, 404.0f)];
    id a104 = [CCFadeOut actionWithDuration:1.0];
    id action47 = [CCSequence actions:[CCDelayTime actionWithDuration:0.8f], [[a9 copy] autorelease], a103, [[a8 copy] autorelease], a104, nil];
    [Movie_Sprite[19] runAction:action47];
    id a105 = [CCMoveTo actionWithDuration:0.0f position:ccp(778.0f, 428.0f)];
    id a106 = [CCFadeOut actionWithDuration:1.0];
    id action48 = [CCSequence actions:[CCDelayTime actionWithDuration:1.3f], [[a9 copy] autorelease], a105, [[a8 copy] autorelease], a106, nil];
    [Movie_Sprite[18] runAction:action48];
    id a107 = [CCMoveTo actionWithDuration:0.0f position:ccp(703.0f, 424.0f)];
    id a108 = [CCFadeOut actionWithDuration:1.0];
    id action49 = [CCSequence actions:[CCDelayTime actionWithDuration:1.8f], [[a9 copy] autorelease], a107, [[a8 copy] autorelease], a108, nil];
    [Movie_Sprite[17] runAction:action49];
    id a109 = [CCMoveTo actionWithDuration:0.0f position:ccp(744.0f, 454.0f)];
    id a110 = [CCFadeOut actionWithDuration:1.0];
    id action50 = [CCSequence actions:[CCDelayTime actionWithDuration:2.3f], [[a9 copy] autorelease], a109, [[a8 copy] autorelease], a110, nil];
    [Movie_Sprite[16] runAction:action50];
    id a111 = [CCMoveTo actionWithDuration:0.0f position:ccp(803.0f, 473.0f)];
    id a112 = [CCFadeOut actionWithDuration:1.0];
    id action51 = [CCSequence actions:[CCDelayTime actionWithDuration:2.8f], [[a9 copy] autorelease], a111, [[a8 copy] autorelease], a112, nil];
    [Movie_Sprite[15] runAction:action51];
    id a113 = [CCMoveTo actionWithDuration:0.0f position:ccp(677.0f, 458.0f)];
    id a114 = [CCFadeOut actionWithDuration:1.0];
    id action52 = [CCSequence actions:[CCDelayTime actionWithDuration:3.3f], [[a9 copy] autorelease], a113, [[a8 copy] autorelease], a114, nil];
    [Movie_Sprite[14] runAction:action52];
    id a115 = [CCMoveTo actionWithDuration:0.0f position:ccp(732.0f, 490.0f)];
    id a116 = [CCFadeOut actionWithDuration:1.0];
    id action53 = [CCSequence actions:[CCDelayTime actionWithDuration:3.8f], [[a9 copy] autorelease], a115, [[a8 copy] autorelease], a116, nil];
    [Movie_Sprite[13] runAction:action53];
    //鳥その４(大)出現
    id a117 = [CCMoveTo actionWithDuration:0.0f position:ccp(612.0f, 404.0f)];
    id a118 = [CCFadeOut actionWithDuration:1.0];
    id action54 = [CCSequence actions:[CCDelayTime actionWithDuration:4.3f], [[a9 copy] autorelease], a117, [[a8 copy] autorelease], a118, nil];
    [Movie_Sprite[26] runAction:action54];
    id a119 = [CCMoveTo actionWithDuration:0.0f position:ccp(628.0f, 428.0f)];
    id a120 = [CCFadeOut actionWithDuration:1.0];
    id action55 = [CCSequence actions:[CCDelayTime actionWithDuration:4.8f], [[a9 copy] autorelease], a119, [[a8 copy] autorelease], a120, nil];
    [Movie_Sprite[25] runAction:action55];
    id a121 = [CCMoveTo actionWithDuration:0.0f position:ccp(553.0f, 424.0f)];
    id a122 = [CCFadeOut actionWithDuration:1.0];
    id action56 = [CCSequence actions:[CCDelayTime actionWithDuration:5.3f], [[a9 copy] autorelease], a121, [[a8 copy] autorelease], a122, nil];
    [Movie_Sprite[24] runAction:action56];
    id a123 = [CCMoveTo actionWithDuration:0.0f position:ccp(594.0f, 454.0f)];
    id a124 = [CCFadeOut actionWithDuration:1.0];
    id action57 = [CCSequence actions:[CCDelayTime actionWithDuration:5.8f], [[a9 copy] autorelease], a123, [[a8 copy] autorelease], a124, nil];
    [Movie_Sprite[23] runAction:action57];
    id a125 = [CCMoveTo actionWithDuration:0.0f position:ccp(653.0f, 473.0f)];
    id a126 = [CCFadeOut actionWithDuration:1.0];
    id action58 = [CCSequence actions:[CCDelayTime actionWithDuration:6.3f], [[a9 copy] autorelease], a125, [[a8 copy] autorelease], a126, nil];
    [Movie_Sprite[22] runAction:action58];
    id a127 = [CCMoveTo actionWithDuration:0.0f position:ccp(527.0f, 458.0f)];
    id a128 = [CCFadeOut actionWithDuration:1.0];
    id action59 = [CCSequence actions:[CCDelayTime actionWithDuration:6.8f], [[a9 copy] autorelease], a127, [[a8 copy] autorelease], a128, nil];
    [Movie_Sprite[21] runAction:action59];
    id a129 = [CCMoveTo actionWithDuration:0.0f position:ccp(582.0f, 490.0f)];
    id a130 = [CCEaseExponentialIn actionWithAction:[CCMoveBy actionWithDuration:1.0f position:ccp(0.0f, -1000.0f)]];
    id a132 = [CCSpawn actions:a130,hall, nil];
    id action60 = [CCSequence actions:[CCDelayTime actionWithDuration:7.3f], [[a9 copy] autorelease], a129,  [[fadein copy] autorelease], [[a8 copy] autorelease], [CCDelayTime actionWithDuration:0.5f], a132, [hall reverse], nil];
    [Movie_Sprite[20] runAction:action60];
}

//ぐおー通過
-(void)scene11_1:(float)touchY{
    float volume;
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.5;
    } else { // 停止中
        volume = 0.8;
    }
    id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    id a1 = [CCMoveBy actionWithDuration:1.5f position:ccp(2000,0)];
    id a2 = [CCShaky3D actionWithRange:30 shakeZ:NO grid:ccg(5, 10) duration:1.5f];
    id a3 = [CCSpawn actions: a1, a2, nil];
    Movie_Sprite[0].position = ccp(-500.0f, touchY);
    [Movie_Sprite[0] runAction:a3];
    [self playSound :1 :@"scene11_ibiki.mp3" :volume];
    //蛇と琵琶・弁天様ぷるぷる
    id action1 = [CCSequence actions:[CCDelayTime actionWithDuration:0.5], [CCShaky3D actionWithRange:3 shakeZ:NO grid:ccg(5, 20) duration:1.3f], nil];
    id action2 = [CCShaky3D actionWithRange:3 shakeZ:NO grid:ccg(50, 20) duration:1.3f];
    id action3 = [CCSequence actions:[CCDelayTime actionWithDuration:1.0], [CCShaky3D actionWithRange:3 shakeZ:NO grid:ccg(5, 20) duration:1.3f],reset, nil];
    [Movie_Sprite[1] runAction:action1];
    [Movie_Sprite[3] runAction:action2];
    [Movie_Sprite[4] runAction:action3];
}


//蛇と琵琶ぷるぷる
-(void)scene11_2{
    id action1 = [CCShaky3D actionWithRange:3 shakeZ:NO grid:ccg(50, 20) duration:3.0f];
    id action2 = [CCSequence actions:[CCDelayTime actionWithDuration:1.5], [CCShaky3D actionWithRange:3 shakeZ:NO grid:ccg(5, 20) duration:3.0f], nil];    
    [Movie_Sprite[3] runAction:action1];
    [Movie_Sprite[4] runAction:action2];
}

-(void)scene12{
    //アニメーションを使用するときはまず変数を宣言してそこに動きなどの命令を入れておく
    //その後にアニメーションを呼び出す命令でまとめて呼び出す
    id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    //id a1 = [CCScaleBy actionWithDuration:0.1f scale:1.5f];
    id a2 = [CCScaleBy actionWithDuration:6 scale:0.5];
    id a3 = [CCHide action];  // アクション非表示
    id func = [CCCallFunc actionWithTarget:self selector:@selector(scene12_1)];
    id spawn = [CCSpawn actions:a3, func, nil];
    
    id action1 = [CCSequence actions:a2, spawn,reset, nil];
    [Movie_Sprite[0] runAction:action1];
}

//消失の時のパーティクル
-(void)scene12_1{
    particles[3] = [CCParticleSystemQuad particleWithFile:@"gama13_particle.plist"];
    [particles[3] setPosition:(ccp(510, 386))];
    particles[3].autoRemoveOnFinish = YES;
    [self addChild:particles[3]];
    //SE
    if(optionnum3[1] == 0){
        [[SimpleAudioEngine sharedEngine] playEffect:@"scene12_warp.mp3"];
    }
}

//12ページの船と波の動き
-(void)scene12_2{
    //船の動き
    id a1 = [CCMoveBy actionWithDuration:1.8f position:ccp(0.0f,-12.0f)];
    id a2 = [CCSequence actions: [[a1 copy] autorelease], [a1 reverse], [a1 reverse], [[a1 copy] autorelease], nil];
    id action1 = [CCRepeatForever actionWithAction:[CCSequence actions:a2, nil]];
    [Movie_Sprite[1] runAction:action1];
    
    //以下波の動き(手前から)
    id a3 = [CCMoveBy actionWithDuration:1.8f position:ccp(0.0f,-12.0f)];
    id a4 = [CCSequence actions: [[a3 copy] autorelease], [a3 reverse], [a3 reverse], [[a3 copy] autorelease], nil];
    id action2 = [CCRepeatForever actionWithAction:[CCSequence actions:a4, nil]];
    [Movie_Sprite[2] runAction:action2];
    
    id a5 = [CCMoveBy actionWithDuration:1.6f position:ccp(0.0f,-10.0f)];
    id a6 = [CCSequence actions: [[a5 copy] autorelease], [a5 reverse], [a5 reverse], [[a5 copy] autorelease], nil];
    id action3 = [CCRepeatForever actionWithAction:[CCSequence actions:a6, nil]];
    [Movie_Sprite[3] runAction:action3];
    
    id a7 = [CCMoveBy actionWithDuration:1.4f position:ccp(0.0f,-8.0f)];
    id a8 = [CCSequence actions: [[a7 copy] autorelease], [a7 reverse], [a7 reverse], [[a7 copy] autorelease], nil];
    id action4 = [CCRepeatForever actionWithAction:[CCSequence actions:a8, nil]];
    [Movie_Sprite[4] runAction:action4];
    
    id a9 = [CCMoveBy actionWithDuration:1.2f position:ccp(0.0f,-6.0f)];
    id a10 = [CCSequence actions: [[a9 copy] autorelease], [a9 reverse], [a9 reverse], [[a9 copy] autorelease], nil];
    id action5 = [CCRepeatForever actionWithAction:[CCSequence actions:a10, nil]];
    [Movie_Sprite[5] runAction:action5];
    
    id a11 = [CCMoveBy actionWithDuration:1.0f position:ccp(0.0f,-4.0f)];
    id a12 = [CCSequence actions: [[a11 copy] autorelease], [a11 reverse], [a11 reverse], [[a11 copy] autorelease], nil];
    id action6 = [CCRepeatForever actionWithAction:[CCSequence actions:a12, nil]];
    [Movie_Sprite[6] runAction:action6];
    
    id a13 = [CCMoveBy actionWithDuration:0.8f position:ccp(0.0f,-2.0f)];
    id a14 = [CCSequence actions: [[a13 copy] autorelease], [a13 reverse], [a13 reverse], [[a13 copy] autorelease], nil];
    id action7 = [CCRepeatForever actionWithAction:[CCSequence actions:a14, nil]];
    [Movie_Sprite[8] runAction:action7];
}

// 弁天様ゴゴゴゴ
-(void)scene13{
    float volume;
    
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.3;
    } else { // 停止中
        volume = 0.5;
    }
    
    [self playSound:0 :@"scene13_angry.mp3" :volume];

    id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    id a1 = [CCEaseIn actionWithAction:[CCMoveTo actionWithDuration:4.0f position:ccp(549.0f, 361.0f)] rate:4.0f];
    id action1 = [CCSequence actions:a1, reset, nil];
    id action2 = [CCFadeOut actionWithDuration:0.0f];
    
    [Movie_Sprite[1] runAction:action2]; // 汗は消しておく
    [Movie_Sprite[2] runAction:action1];
}

// 与吉ぷるぷる
-(void)scene13_1{
    id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    id a1 = [CCFadeIn actionWithDuration:0.0f];
    id a2 = [CCMoveTo actionWithDuration:0.0f position:ccp(304, 502)];
    id a3 = [CCMoveTo actionWithDuration:0.7f position:ccp(344, 552)];
    id a4 = [CCFadeOut actionWithDuration:0.7f];
    id a5 = [CCSpawn actions:a3, a4, nil];
    id action2 = [CCSequence actions:a1, a2, a5, reset, nil];
    /*id action1 = [CCSequence actions:
                 [CCDelayTime actionWithDuration:2.2f],
                 [CCShaky3D actionWithRange:20 shakeZ:NO grid:ccg(8,8) duration:1.0f],
                 [CCShaky3D actionWithRange:1 shakeZ:NO grid:ccg(1,1) duration:0.0f],
                 nil];*/
    id action1 = [CCShaky3D actionWithRange:3 shakeZ:NO grid:ccg(5, 1) duration:0.5f];
    
    [Movie_Sprite[0] runAction:action1];
    [Movie_Sprite[1] runAction:action2];
}

#pragma mark -
#pragma mark scene14 与吉泣く
-(void)scene14_init{
    // 画像サイズの保存
    float h = [Movie_Sprite[2] contentSize].height;
    float w = [Movie_Sprite[2] contentSize].width;
    spriteSize = CGSizeMake(w, h);
    
    // 与吉の涙 最初に消しとく
    id action = [CCFadeOut actionWithDuration:0.0f];
    [Movie_Sprite[4] runAction:action];
    [Movie_Sprite[5] runAction:[[action copy] autorelease]];
    [Movie_Sprite[6] runAction:[[action copy] autorelease]];
    [Movie_Sprite[7] runAction:[[action copy] autorelease]];
}

// 与吉の左腕の動き
-(void)scene14_1{
    Movie_Sprite[1].anchorPoint = CGPointMake(0.5f, 0.2f);
    Movie_Sprite[1].position = ccp(828, 165);
    
    id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    id a1 = [CCRotateTo actionWithDuration:0.4f angle:-30];
    id a2 = [CCRotateTo actionWithDuration:0.3f angle:0];
    id action = [CCSequence actions:a1, a2, reset, nil];
    
    [Movie_Sprite[1] runAction:action];
}

// 与吉の右腕の動き
-(void)scene14_2{
    Movie_Sprite[0].anchorPoint = CGPointMake(0.5f, 0.2f);
    Movie_Sprite[0].position = ccp(185, 135);
    
    id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    id a1 = [CCRotateTo actionWithDuration:0.4f angle:30];
    id a2 = [CCRotateTo actionWithDuration:0.3f angle:0];
    id action = [CCSequence actions:a1, a2, reset, nil];
    
    [Movie_Sprite[0] runAction:action];
}

// 与吉の涙
-(void)scene14_3{
    [Movie_Sprite[4] runAction:[self tearMove :Movie_Sprite[4] :4 :CGPointMake(243 + 200, 597) :CGPointMake(-100, 100)]];
    [Movie_Sprite[5] runAction:[self tearMove :Movie_Sprite[5] :5 :CGPointMake(808 - 200, 574) :CGPointMake(40, 50)]];
    [Movie_Sprite[6] runAction:[self tearMove :Movie_Sprite[6] :6 :CGPointMake(736 - 200, 596) :CGPointMake(40, 50)]];
    [Movie_Sprite[7] runAction:[self tearMove :Movie_Sprite[7] :7 :CGPointMake(215 + 200, 536) :CGPointMake(-80, 100)]];

    // 少し遅れてくる涙
    const float DELAY_TIME = 0.2f;
    [Tear_Sprite[1]  runAction:[CCSequence actions:[CCDelayTime actionWithDuration:DELAY_TIME],
                                [self tearMove :Tear_Sprite[1] :4 :CGPointMake(243 + 200, 597) :CGPointMake(-100, 100)],nil]];
    [Tear_Sprite[2]  runAction:[CCSequence actions:[CCDelayTime actionWithDuration:DELAY_TIME],
                                [self tearMove :Tear_Sprite[2] :5 :CGPointMake(808 - 200, 574) :CGPointMake(40, 50)],nil]];
    [Tear_Sprite[3]  runAction:[CCSequence actions:[CCDelayTime actionWithDuration:DELAY_TIME],
                                [self tearMove :Tear_Sprite[3] :6 :CGPointMake(736 - 200, 596) :CGPointMake(40, 50)],nil]];
    [Tear_Sprite[4]  runAction:[CCSequence actions:[CCDelayTime actionWithDuration:DELAY_TIME],
                                [self tearMove :Tear_Sprite[4] :7 :CGPointMake(215 + 200, 536) :CGPointMake(-80, 100)],nil]];
}

// 与吉の顔 拡大
-(void)scene14_HeadZoom {
    Movie_Sprite[2].scale *= 1.005f;
}

// 与吉の顔 縮小
-(void)scene14_HeadPinch {
    id action1 = [CCScaleBy actionWithDuration:0.1f scaleX:1 / Movie_Sprite[2].scaleX
                                        scaleY:1 / Movie_Sprite[2].scaleY];
    [Movie_Sprite[2] runAction:action1];
    
    touchDrag = false;
    touchEnded = false;
}

// 与吉の泣き声
-(void)scene14_sound {
    int soundRand = rand() % 5; // 乱数
    float volume = 0.2;
    
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.2;
    } else { // 停止中
        volume = 0.5;
    }
    
    // サウンドをランダムに再生
    switch(soundRand) {
        case 0:
            [self playSound :0 :@"scene14_crying1.mp3" :volume];
            break;
        case 1:
            [self playSound :0 :@"scene14_crying2.mp3" :volume];
            break;
        case 2:
            [self playSound :0 :@"scene14_crying3.mp3" :volume];
            break;
        case 3:
            [self playSound :0 :@"scene14_crying4.mp3" :volume];
            break;
        case 4:
            [self playSound :0 :@"scene14_crying5.mp3" :volume];
            break;
        default:
            break;
    }

}

/** 
 * 涙の移動
 * @param   spriteIndex Movie_Spriteのインデックス
 * @param   spritePoint CCSpriteの座標
 * @param   offsetPoint 移動する座標量
 * @return  生成されたアクション
 */
-(id)tearMove :(CCSprite *) sprite :(int)spriteIndex :(CGPoint)spritePoint :(CGPoint)offsetPoint {
    sprite.position = spritePoint;  // 位置の初期化
    id a3 = nil;
    id a1 = [CCFadeIn actionWithDuration:0.0f];
    
    if (spriteIndex == 4) {  // 左側
        a3 = [CCSpawn actions:[CCJumpTo actionWithDuration:0.5f position:ccp(10.0f, 600.0f) height:150.0f jumps:1],
                    [CCRotateBy actionWithDuration:0.5f angle:0.0f],nil];
    } else if (spriteIndex == 5) { // 右側
        a3 = [CCSpawn actions:[CCJumpTo actionWithDuration:0.5f position:ccp(1000.0f, 600.0f) height:150.0f jumps:1],
              [CCRotateBy actionWithDuration:0.5f angle:0.0f],nil];
    } else if (spriteIndex == 6) { // 右側
        a3 = [CCSpawn actions:[CCJumpTo actionWithDuration:0.5f position:ccp(900.0f, 600.0f) height:150.0f jumps:1],
              [CCRotateBy actionWithDuration:0.5f angle:0.0f],nil];
    } else if (spriteIndex == 7) { // 左側
        a3 = [CCSpawn actions:[CCJumpTo actionWithDuration:0.5f position:ccp(100.0f, 600.0f) height:150.0f jumps:1],
              [CCRotateBy actionWithDuration:0.5f angle:0.0f],nil];
    }
    
    id a5 = [CCFadeOut actionWithDuration:0.0f];
    return [CCSequence actions:a1, a3, a5, nil];
}

// SEをループ再生する
-(void)scene14_HeadSound :(NSTimer* )timer {
    [self playSound:1 :@"piyu2.mp3" :0.5];
}

// 与吉顔拡大時の音
-(void)scene14_ZoomSound {

}

-(void)scene15_1{
    /*
     NSString *path =[[NSString alloc] init];
     path = [NSString stringWithFormat:@"scene3_ono.mp3"];
     id se = [CCCallFuncND actionWithTarget:self selector:@selector(playSE:data:) data:[path retain]];
     */
    NSString *path;
    path = [NSString stringWithFormat:@"scene15_pon.mp3"];
    id se = [CCCallFuncND actionWithTarget:self selector:@selector(playSE:data:) data:[path retain]];
    
    id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    id a1 = [CCRotateTo actionWithDuration:0.3f angle:-5];
    id a2 = [CCRotateTo actionWithDuration:0.7f angle:15];
    id e1 = [CCEaseInOut actionWithAction:a2 rate:3.0f];
    id a3 = [CCRotateTo actionWithDuration:0.3f angle:0];
    id e2 = [CCEaseInOut actionWithAction:a3 rate:2.0f];
    
    id action = [CCSequence actions:a1, e1, se, e2, reset, nil];
    
    id action2 = [CCSequence actions:action, nil];
    [Movie_Sprite[0] runAction:action2];
}

-(void)scene15_2{
    id a1 = [CCMoveBy actionWithDuration:3.0f position:ccp(50.0f,0.0f)];
    id a2 = [CCSequence actions: [[a1 copy] autorelease], [a1 reverse], [a1 reverse], [[a1 copy] autorelease], nil];
    id action1 = [CCRepeatForever actionWithAction:[CCSequence actions:a2, nil]];
    [Movie_Sprite[5] runAction:action1];
    id a3 = [CCMoveBy actionWithDuration:3.4f position:ccp(50.0f,0.0f)];
    id a4 = [CCSequence actions: [[a3 copy] autorelease], [a3 reverse], [a3 reverse], [[a3 copy] autorelease], nil];
    id action2 = [CCRepeatForever actionWithAction:[CCSequence actions:a4, nil]];
    [Movie_Sprite[6] runAction:action2];
    id a5 = [CCMoveBy actionWithDuration:2.6f position:ccp(-50.0f,0.0f)];
    id a6 = [CCSequence actions: [[a5 copy] autorelease], [a5 reverse], [a5 reverse], [[a5 copy] autorelease], nil];
    id action3 = [CCRepeatForever actionWithAction:[CCSequence actions:a6, nil]];
    [Movie_Sprite[7] runAction:action3];
}

-(void)scene16_1{//竹島弁天
    id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    id a1 = [CCRotateTo actionWithDuration:0.75f angle:25];
    id a2 = [CCRotateTo actionWithDuration:0.55f angle:0];
    id b1 = [CCRotateTo actionWithDuration:1.0f angle:25];
    id b2 = [CCRotateTo actionWithDuration:0.75f angle:0];
    id c1 = [CCRotateTo actionWithDuration:3.0f angle:25];
    id c2 = [CCRotateTo actionWithDuration:0.15f angle:18];
    id c3 = [CCRotateTo actionWithDuration:0.15f angle:25];
    id delay1 = [CCDelayTime actionWithDuration:0.5f];
    id benten1 = [CCSequence actions:a1, a2, delay1, reset, nil];
    id benten2 = [CCSequence actions:b1, b2, delay1, reset, nil];
    id benten3 = [CCSequence actions:c1, c2, c3, c2, c3, nil];
    id easeInAction02 = [CCEaseBounceOut actionWithAction:benten3];
    
    id benten4 = [CCSequence actions:easeInAction02, b2, delay1, reset, nil];
    
    float volume = 0.4;
    
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.75;
    } else { // 停止中
        volume = 0.75;
    }
    
    
    
    if(touchCount > 9)
    {
        switch(rand() % 5){
            case 0:
                [Movie_Sprite[2] runAction:benten2];
                [self playSound :1 :@"scene16_renda1.mp3" :volume];
                touchCount = 0;
                break;
            case 1:
                [Movie_Sprite[2] runAction:benten2];
                [self playSound :1 :@"scene16_renda2.mp3" :volume];
                touchCount = 0;
                break;
            case 2:
                [Movie_Sprite[2] runAction:benten2];
                [self playSound :1 :@"scene16_renda3.mp3" :volume];
                touchCount = 0;
                break;
            case 3:
                [Movie_Sprite[2] runAction:benten2];
                [self playSound :1 :@"scene16_renda4.mp3" :volume];
                touchCount = 0;
                break;
            case 4:
                [Movie_Sprite[2] runAction:benten2];
                [self playSound :1 :@"scene16_renda5.mp3" :volume];
                touchCount = 0;
                break;
            default:
                break;
                
        }
        
    }
    else if(touchCount >= 6 && (touchCount - 6) >= (rand() % 4))
    {
        [Movie_Sprite[2] runAction:benten4];
        [self playSound :1 :@"scene16_benten3.mp3" :volume];
        touchCount = 10;
    }
    
    else {
        switch(rand() % 2){
            case 0:
                [Movie_Sprite[2] runAction:benten1];
                [self playSound :1 :@"scene16_benten1.mp3" :volume];
                touchCount++;
                break;
            case 1:
                [Movie_Sprite[2] runAction:benten2];
                [self playSound :1 :@"scene16_benten2.mp3" :volume];
                touchCount++;
                break;
            default:
                break;
        }
    }
    
    
    
}-(void)scene16_2{//竹島弁天
    id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    
    
    id a1 = [CCRotateTo actionWithDuration:1.75f angle:25];
    id a2 = [CCRotateTo actionWithDuration:0.35f angle:0];
    //id a3 = [CCRotateTo actionWithDuration:1.0f angle:0];
    
    id action = [CCSequence actions:a1, a2, reset, nil];
    //id action2 = [CCSequence actions:a1, a3, a1, a2, reset, nil];
    id easeInAction = [CCEaseExponentialOut actionWithAction:action];
    //id easeInAction2 = [CCEaseExponentialOut actionWithAction:action2];
    
    float volume = 0.4;
    
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.4;
    } else { // 停止中
        volume = 0.75;
    }
    
    switch(rand() % 4){
            
        case 0:
            [Movie_Sprite[1] runAction:easeInAction];
            [self playSound :1 :@"scene16_cat4.mp3" :volume];
            break;            
        case 1:            
            [Movie_Sprite[1] runAction:easeInAction];
            [self playSound :1 :@"scene16_cat1.mp3" :volume];
            break;
        case 2:            
            [Movie_Sprite[1] runAction:easeInAction];
            [self playSound :1 :@"scene16_cat2.mp3" :volume];
            break;
        case 3:            
            [Movie_Sprite[1] runAction:easeInAction];
            [self playSound :1 :@"scene16_cat3.mp3" :volume];
            break;
        default:
            break;
    }
    
    
    
}-(void)scene16_3{//竹島弁天
    id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    id a1 = [CCRotateTo actionWithDuration:1.4f angle:0];
    id action = [CCSequence actions:a1, reset, nil];
    
    float volume = 0.4;
    
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.4;
    } else { // 停止中
        volume = 0.75;
    }
    
    
    
    
    switch(rand() % 4){
            
        case 0:
            [self playSound :1 :@"scene16_cat4.mp3" :volume];
            [Movie_Sprite[1] runAction:action];            
            break;            
        case 1:            
            [Movie_Sprite[1] runAction:action];
            [self playSound :1 :@"scene16_cat1.mp3" :volume];
            break;
        case 2:            
            [Movie_Sprite[1] runAction:action];
            [self playSound :1 :@"scene16_cat2.mp3" :volume];
            break;
        case 3:            
            [Movie_Sprite[1] runAction:action];
            [self playSound :1 :@"scene16_cat3.mp3" :volume];
            break;
        default:
            break;
    }
    
    
    
}

#pragma mark -
#pragma mark scene17 与吉ハッピーエンド
// scene17のアクション
-(void)scene17 {
    [self scene17_leftArmRepeat];
}

// ハート位置を元に戻す
-(void)scene17_initHeart {
    Movie_Sprite[2].position = ccp(254.0f, 618.0f);
}

// ハート吹き出し
-(void)scene17_heart {
    id a1 = [CCFadeIn actionWithDuration:1.0f];
    id a2 = [CCMoveTo actionWithDuration:1.51f position:ccp(254.0f, 688.0f)];
    id a3 = [CCFadeOut actionWithDuration:1.0f];
    id positionReset = [CCCallFunc actionWithTarget:self selector:@selector(scene17_initHeart)];
    id heartMove = [CCSpawn actions:a1, a2, a3, nil];
    
    [self scene17_heartSound]; // ハートの吹き出し音
    [Movie_Sprite[2] runAction:[CCSequence actions:heartMove, positionReset, nil]];
}

// 嫁が頬を赤らめる
-(void)scene17_blush {
    id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    id fadeIn = [CCFadeIn actionWithDuration:1.0f];
    id fadeOut = [CCFadeOut actionWithDuration:1.0f];
    id delay =  [CCDelayTime actionWithDuration:0.5f];
    id blush = [CCSequence actions:fadeIn, delay, fadeOut, reset, nil];
    
    [Movie_Sprite[8] runAction:blush];
}

// 与吉の左腕の繰り返し
-(void)scene17_leftArmRepeat {
    id rotate = [CCRotateBy actionWithDuration:0.7f angle:-10.0f];
    id move = [CCMoveBy actionWithDuration:0.7f position:ccp(-10.0f, 10.0f)];
    id spawn = [CCSpawn actions:rotate, move, nil];
    id action = [CCRepeatForever actionWithAction:[CCSequence actions:spawn, [spawn reverse], nil]];
    
    [Movie_Sprite[1] runAction:action];
}

// おじいちゃん チラッ
-(void)scene17_gentleman {
    id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlagEnable)];
    id move = [CCMoveBy actionWithDuration:0.7f position:ccp(10.0f, 0.0f)];
    [self playSound:1 :@"scene7_piyu2.mp3" :0.5];
    [Movie_Sprite[7] runAction:[CCSequence actions:move, [move reverse], reset, nil]];
}


// 子供の動き
-(void)scene17_child {
    // アクション中のタッチ防止
    const CGPoint DefaultPosition = ccp(817.0f,23.0f);
    if(Movie_Sprite[3].position.y >= DefaultPosition.y) {
        touchFlag2 = false;
        Movie_Sprite[3].position = DefaultPosition;
    }
    int touchRand = rand() % 100; // 乱数
    
    id reset = [CCCallFunc actionWithTarget:self selector:@selector(touchFlag2Enable)];
    id fallSound = [CCCallFunc actionWithTarget:self selector:@selector(scene17_fallSound)];
    id jump, easeAction, action;
    
    // 乱数によってジャンプを変化
    if (touchRand > 50) { // ランダムなジャンプ
        jump = [CCMoveBy actionWithDuration:0.2f position:ccp(0.0f, (float)touchRand)];
        easeAction = [CCEaseSineOut actionWithAction:jump];
        action = [CCSequence actions:easeAction, [easeAction reverse], reset, nil];
    } else if (touchRand < 10 && touchCount > 5) { // 大ジャンプ
        jump = [CCMoveBy actionWithDuration:0.3f position:ccp(0.0f, 400.0f)];
        //easeAction = [CCEaseSineOut actionWithAction:jump];
        
        // 転ぶアクション
        id rotate = [CCRotateBy actionWithDuration:0.2f angle:160.0f];
        id move = [CCMoveBy actionWithDuration:0.2f position:ccp(0.0f, -100.0f)];
        id fall = [CCSpawn actions:rotate, move, nil];
        action = [CCSequence actions:jump, [jump reverse], fall, fallSound,
                  [CCDelayTime actionWithDuration:1.0f], [fall reverse], reset, nil];
    } else { // 普通のジャンプ
        jump = [CCMoveBy actionWithDuration:0.2f position:ccp(0.0f, 30.0f)];
        easeAction = [CCEaseSineOut actionWithAction:jump];
        action = [CCSequence actions:easeAction, [easeAction reverse], reset, nil];
    }
    
    [self scene17_jumpSound]; // ジャンプ音
    [Movie_Sprite[3] runAction:action];
}

// SE:Jump
-(void)scene17_jumpSound {
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        [self playSound:0 :@"scene17_jump.mp3" :0.3];
    } else {
        [self playSound:0 :@"scene17_jump.mp3" :0.5];
    }
}

// SE:fall
-(void)scene17_fallSound {
    
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        [self playSound:1 :@"scene17_fall.mp3" :0.3];
    } else {
        [self playSound:1 :@"scene17_fall.mp3" :0.5];
    }
}

// SE:heart
-(void)scene17_heartSound {
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        [self playSound:2 :@"scene17_heart.mp3" :0.3];
    } else {
        [self playSound:2 :@"scene17_heart.mp3" :0.5];
    }
}

// 近所の噂話
-(void)scene17_neighbor {
    id flagReset = [CCCallFunc actionWithTarget:self selector:@selector(soundPlayFlagToggle)];
    id sound = [CCSequence actions:[CCCallFunc actionWithTarget:self selector:@selector(scene17_auntieSound)],
                flagReset, flagReset, nil];
    [Movie_Sprite[5] runAction:sound];
}

// SE:近所の噂話
-(void)scene17_auntieSound {
    float volume = 10.0f; // SE音量
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 1.0f;
    } else {
        volume = 10.0f;
    }
    switch (soundCount) {
        case 0:
            [self cdSoundSourceSE:@"scene17_yokattano1.mp3" :0];
            myEffect[0].gain = volume;
            [myEffect[0] play];
            break;
        case 1:
            [self cdSoundSourceSE:@"scene17_yokattano2.mp3" :0];
            myEffect[0].gain = volume;
            [myEffect[0] play];
            break;
        case 2:
            [self cdSoundSourceSE:@"scene17_yokattano3.mp3" :0];
            myEffect[0].gain = volume;
            [myEffect[0] play];
            break;
        case 3:
            [self cdSoundSourceSE:@"scene17_yokattano4.mp3" :0];
            myEffect[0].gain = volume;
            [myEffect[0] play];
            break;
        case 4:
            [self cdSoundSourceSE:@"scene17_yokattano5.mp3" :0];
            myEffect[0].gain = volume;
            [myEffect[0] play];
            break;
        case 5:
            [self cdSoundSourceSE:@"scene17_yokattano6.mp3" :0];
            myEffect[0].gain = volume;
            [myEffect[0] play];
            break;
        case 6:
            [self cdSoundSourceSE:@"scene17_yokattano7.mp3" :0];
            myEffect[0].gain = volume;
            [myEffect[0] play];
            break;
        default:
            soundCount = 0;
            break;
    }
    soundCount++;
}

// CDSoundSourceによるSE
-(void)cdSoundSourceSE :(NSString *)fileName :(int)index {
    SimpleAudioEngine *mySAE;
    mySAE = [SimpleAudioEngine sharedEngine];
    myEffect[index] = [[mySAE soundSourceForFile:fileName] retain]; 
}

// soundPlayFlagの切り替え
-(void)soundPlayFlagToggle {
    soundPlayFlag = !soundPlayFlag;
}
/*
 switch(rand() % 4){
 
 case 0:
 [Movie_Sprite[1] runAction:easeInAction];
 [self playSound :1 :@"scene16_cat4.mp3" :volume];
 break;            
 case 1:            
 [Movie_Sprite[1] runAction:easeInAction];
 [self playSound :1 :@"scene16_cat1.mp3" :volume];
 break;
 case 2:            
 [Movie_Sprite[1] runAction:easeInAction];
 [self playSound :1 :@"scene16_cat2.mp3" :volume];
 break;
 case 3:            
 [Movie_Sprite[1] runAction:easeInAction];
 [self playSound :1 :@"scene16_cat3.mp3" :volume];
 break;
 default:
 break;
 }
 */

// 18ページ
// SE:カモメ
-(void)scene18_kamomeSound{
    [self playSound:4 :@"scene18_kamome.mp3" :0.5];
}
-(void)scene18_Sound{
    float delaytime;
    switch(rand() % 4){
        case 0:
            delaytime=8.0;
            break;            
        case 1:            
            delaytime=8.5;
            break;
        case 2:            
            delaytime=9.0;
            break;
        case 3:            
            delaytime=9.5;
            break;
        default:
            delaytime=10;
            break;
    }
    id delay = [CCDelayTime actionWithDuration:delaytime];
    id sound = [CCSequence actions:[CCCallFunc actionWithTarget:self selector:@selector(scene18_kamomeSound)], delay, nil];
    id soundRepeat = [CCRepeatForever actionWithAction:sound];

    [Movie_Sprite[5] runAction:soundRepeat];
}
-(void)scene18{// 18ページ 波の動き
    id a1 = [CCMoveBy actionWithDuration:2.3f position:ccp(0.0f,-4.0f)];
    id a2 = [CCSequence actions: [[a1 copy] autorelease], [a1 reverse], nil];
    id repeat1 = [CCRepeatForever actionWithAction:a2];
    [Movie_Sprite[3] runAction:repeat1];// 波4
    id a7 = [CCMoveBy actionWithDuration:1.9f position:ccp(0.0f,-5.0f)];
    id a8 = [CCSequence actions: [[a7 copy] autorelease], [a7 reverse], nil];
    id repeat4 = [CCRepeatForever actionWithAction:a8];
    [Movie_Sprite[4] runAction:repeat4];// 波3
    id a3 = [CCMoveBy actionWithDuration:1.7f position:ccp(0.0f,-9.0f)];
    id a4 = [CCSequence actions: [[a3 copy] autorelease], [a3 reverse], nil];
    id repeat2 = [CCRepeatForever actionWithAction:a4];
    [Movie_Sprite[2] runAction:repeat2];// 波2
    id a5 = [CCMoveBy actionWithDuration:1.4f position:ccp(0.0f,-11.0f)];
    id a6 = [CCSequence actions: [[a5 copy] autorelease], [a5 reverse], nil];
    id repeat3 = [CCRepeatForever actionWithAction:a6];
    [Movie_Sprite[1] runAction:repeat3];// 波1最前面
}

#pragma mark -


-(void) draw
{
	// Default GL states: GL_TEXTURE_2D, GL_VERTEX_ARRAY, GL_COLOR_ARRAY, GL_TEXTURE_COORD_ARRAY
	// Needed states:  GL_VERTEX_ARRAY, 
	// Unneeded states: GL_TEXTURE_2D, GL_COLOR_ARRAY, GL_TEXTURE_COORD_ARRAY
	glDisable(GL_TEXTURE_2D);
	glDisableClientState(GL_COLOR_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	
	world->DrawDebugData();
	
	// restore default GL states
	glEnable(GL_TEXTURE_2D);
	glEnableClientState(GL_COLOR_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    
}

-(void) tick: (ccTime) dt
{
	//It is recommended that a fixed time step is used with Box2D for stability
	//of the simulation, however, we are using a variable time step here.
	//You need to make an informed choice, the following URL is useful
	//http://gafferongames.com/game-physics/fix-your-timestep/
	
	int32 velocityIterations = 8;
	int32 positionIterations = 1;
	
	// Instruct the world to perform a single step of simulation. It is
	// generally best to keep the time step and iterations fixed.
	world->Step(dt, velocityIterations, positionIterations);
    
	
	//Iterate over the bodies in the physics world
	for (b2Body* b = world->GetBodyList(); b; b = b->GetNext())
	{
		if (b->GetUserData() != NULL) {
			//Synchronize the AtlasSprites position and rotation with the corresponding body
			CCSprite *myActor = (CCSprite*)b->GetUserData();
			myActor.position = CGPointMake( b->GetPosition().x * PTM_RATIO, b->GetPosition().y * PTM_RATIO);
			myActor.rotation = -1 * CC_RADIANS_TO_DEGREES(b->GetAngle());
		}	
	}

    // Scene14
    // 与吉の顔 拡大
    const float MaxScale = 2.050749; // 最大倍率
    if (page == 14  && touchDrag == true
        && (Movie_Sprite[2].scale <  MaxScale) ) {
        [self scene14_HeadZoom];

    }
    
    // ナレーション再生が終了したとき
    //テキストOFFの時
    if(optionnum3[2] == 1){
        if (soundFlag == true && player.isPlaying != true) {
            CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
            [frameCache addSpriteFramesWithFile:@"arrow_box_right2-hd.plist" textureFile:@"arrow_box_right2-hd.pvr.gz"];
            CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"arrow_box_right2-hd.pvr.gz"];
            [self addChild:spriteSheet];
            
            menu_sprite = [CCSprite spriteWithSpriteFrameName:@"arrow_box_right2.png"];
            menu_sprite.position = ccp(576, 40);
            menu_sprite.opacity = 1.0;
            [self addChild:menu_sprite z:16];
            
            id a1 = [CCFadeIn actionWithDuration:1.0f];
            id a2 = [CCDelayTime actionWithDuration:1.0f];
            id a3 = [CCFadeOut actionWithDuration:1.0f];
            id action = [CCRepeatForever actionWithAction:[CCSequence actions:a1, a2, a3, nil]];
        
            [menu_sprite runAction:action];
            soundFlag = false;
        }
    }
}

//連続でタッチされないようにフラグを切り替える
-(void)touchFlagEnable{
    touchFlag = true;
}

//連続でタッチされないようにフラグを切り替える
-(void)touchFlag2Enable{
    touchFlag2 = true;
}
//連続でムーブされないようにフラグを切り替える
-(void)touchMoveFlagEnable{
    touchMoveFlag = true;
}
//Spriteの衝突判定用
-(CGRect)rectForSprite:(CCSprite *)sprite{
    CGRect rect;
    
    float h = [sprite contentSize].height;
    float w = [sprite contentSize].width;
    float x = sprite.position.x-w/2;
    float y = sprite.position.y-h/2;
    rect = CGRectMake(x,y,w,h);
    
    return rect; 
}

// callfuncでSEを再生
-(void)playSE:(id)node data:(NSString *)name{
    float volume;
        
    // ナレーション再生時の音量調整
    if(player.isPlaying == true) { // 再生中
        volume = 0.3;
    } else { // 停止中
        volume = 0.5;
    }
    
    [self playSound :0 :name :volume];
}

/** アクション中にSEを再生
 * @param count SE_Arrayの添字
 * @param sound サウンド変数
 * @param fileName 再生ファイル名
 * @param volume 再生音量
 */
-(void)playSound :(int)count :(NSString *)fileName :(float)volume {
    if(optionnum3[1] == 0) {
        [[SimpleAudioEngine sharedEngine] stopEffect:SE_Array[count]]; // 連続再生の防止
        [[SimpleAudioEngine sharedEngine] stopEffect:SE_Array[count]];
        SE_Array[count] = [[SimpleAudioEngine sharedEngine] playEffect:fileName];
        [SimpleAudioEngine sharedEngine].effectsVolume = volume;
        
    }
}

-(void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *myTouch = [touches anyObject];
    CGPoint location = [myTouch locationInView:[myTouch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];
    float touchX,touchY;
    touchX = location.x;
    touchY = location.y;
    
    if (page == 2) {
        touchDrag = true;
        if((touchY < 520)&&
           touchCount == 0){
            rotateMax = false;
            touchFlag = true;
            touchBeganFlag = true;
            touchBeganX = touchX;
            touchBeganY = touchY;
            kusaFlag = true;
        }else if((touchY > 576)&&
                 (touchX < 374)&&
                 (touchCount == 0)){
            rotateMax = false;
            touchFlag = true;
            //touchBeganFlag = true;
            touchBeganX = touchX;
            touchBeganY = touchY;
            leftWoodFlag = true;
            //MovedRange=90;
            touchMoveFlag = false;
            
        }else if((touchY > 595)&&
                 (touchX > 652)&&
                 (touchCount == 0)){
            rotateMax = false;
            touchFlag = true;
            //touchBeganFlag = true;
            touchBeganX = touchX;
            touchBeganY = touchY;
            rightWoodFlag = true;
            //MovedRange=90;
            touchMoveFlag = false;
            
        }else if((touchY < 500)&&
                 touchCount == 1){
            rotateMax = false;
            touchFlag = true;
            touchBeganFlag = true;
            touchBeganX = touchX;
            touchBeganY = touchY;
            
        }else if((touchY < 500)&&
                 touchCount == 2){
            rotateMax = false;
            touchFlag = true;
            touchBeganFlag = true;
            touchBeganX = touchX;
            touchBeganY = touchY;
            
        }
    }

    
    if (page == 3) {
        if (touchFlag && touchX >= 270 && touchX <= 790 && touchY >= 110 && touchY <= 400) {
            touchFlag = false;
            [self scene3_2];
        }
    }
    
    if (page == 4) { 
        
        if (touchFlag && touchX >= 440 && touchX <= 750 && touchY >= 80 && touchY <= 320) {
            touchFlag = false;
            [self scene4];
        }        
    }
    if (page == 5) {
        if (touchFlag && touchX >= 249 && touchX <= 414 && touchY >= 156 && touchY <= 267) {
            touchFlag = false;
            [self scene5_2];
        }
        if (touchFlag && touchX >= 693 && touchX <= 902 && touchY >= 85 && touchY <= 194) {
            touchFlag = false;
            [self scene5_3];
        }
        if (touchFlag && touchX >= 0 && touchX <= 281 && touchY >= 0 && touchY <= 383){
            //touchFlag = false;
            [self scene5_4];
        }
        if (touchFlag && touchX >= 910 && touchX <= 1080 && touchY >= 0 && touchY <= 210){
            //touchFlag = false;
            [self scene5_5];
        }
    }
    if (page == 7) {
        CGRect imageRect = [self rectForSprite:Movie_Sprite[19]];
        if(CGRectContainsPoint(imageRect, location)){
            [self scene7_3];
        }
    }
    
    if (page == 8 && touchFlag){
        CGRect imageRect1 = [self rectForSprite:Movie_Sprite[4]];
        CGRect imageRect2 = [self rectForSprite:Movie_Sprite[3]];
        CGRect imageRect3 = [self rectForSprite:Movie_Sprite[6]];
        if(CGRectContainsPoint(imageRect1, location)){
            touchFlag = false;
            [self scene8_2];
        }
        if((CGRectContainsPoint(imageRect2, location))||(CGRectContainsPoint(imageRect3, location))){
            touchFlag = false;
            [self scene8_3];
        }
    }
    
    if (page == 9) {
        //b2Vec2 gravity(100, -10);
        //world->SetGravity( gravity );
        if (touchFlag) {
            [self scene9:touchX :touchY];
            touchFlag = false;
        }
    }
    
    if(page == 10 && touchFlag){
        CGRect imageRect = [self rectForSprite:Movie_Sprite[27]];
        if (CGRectContainsPoint(imageRect, location)){
            touchFlag = false;
            [self scene10_2];
        }
        /*Movie_Sprite[26].position = ccp(touchX, touchY);
         NSLog(@"%f,%f", touchX, touchY);*/
    }
    
    if(page == 11 && touchFlag){
        [self scene11_1:touchY];
        touchFlag = false;
    }
    
    if (page == 13 && touchFlag) {
        if (touchX >= 205 && touchX <= 328 && touchY >= 350 && touchY <= 506) {
            touchFlag = false;
            [self scene13_1];
        }
    }
    
    if (page == 14 && touchFlag) {
        CGRect imageRect = [self rectForSprite:Movie_Sprite[2]];
        
        if (touchFlag && touchX >= 780 && touchX <= 950 && touchY >= 90 && touchY <= 600) { // 左腕を触ったとき
            touchFlag = false;
            [self scene14_1];
            [self scene14_2];
            [self scene14_3];
            [self scene14_sound];
        } else if (touchFlag && touchX >= 100 && touchX <= 300 && touchY >= 90 && touchY <= 600) { // 右腕を触ったとき
            touchFlag = false;
            [self scene14_1];
            [self scene14_2];
            [self scene14_3];
            [self scene14_sound];
        } else if (touchFlag && CGRectContainsPoint(imageRect, location)) { // 頭を触ったとき
            if([myEffect[0] isPlaying] == NO) {
                [myEffect[0] play];
            }
            touchDrag = true;
        }
    }
    
    if (page == 15 && touchFlag) {
        if (touchX >= 285 && touchX <= 740 && touchY >= 155 && touchY <= 400) {
            touchFlag = false;
            [self scene15_1];
        }
    }
    
    if (page == 16) { //竹島弁天
        if (touchFlag && touchX >= 320 && touchX <= 450 && touchY >= 500 && touchY <= 570){
            touchFlag = false;
            [self scene16_3];
        }        
        
        if (touchFlag && touchX >= 365 && touchX <= 530 && touchY >= 80 && touchY <= 328) {
            touchFlag = false;
            [self scene16_1];
        }
        
        if (touchFlag && touchX >= 788 && touchX <= 866 && touchY >= 70 && touchY <= 212) {
            touchFlag = false;
            [self scene16_2];
        }
        
        
        
    }

    if (page == 17) {
        CGRect imageCouple = [self rectForSprite:Movie_Sprite[0]]; // 与吉と嫁
        CGRect imageChild = [self rectForSprite:Movie_Sprite[3]]; // 子供
        CGRect imageGentleman = [self rectForSprite:Movie_Sprite[7]]; // おじいちゃん
        
        if (CGRectContainsPoint(imageCouple, location) && touchFlag) {
            touchCount++;
            touchFlag = false;
            [self scene17_heart];
            [self scene17_blush];
        } else if (CGRectContainsPoint(imageChild, location) && touchFlag2) {
            touchCount++;
            touchFlag2 = false;
            [self scene17_child];
            
        } else if (CGRectContainsPoint(imageGentleman, location) && touchFlag &&
                   (touchX >  Movie_Sprite[7].position.x + 10.0f) && 
                   (touchY <  30.0f) ) {
            touchCount++;
            touchFlag = false;
            [self scene17_gentleman];
        }
        
        // 近所の人の会話
        if(!soundPlayFlag) { // 再生されていないとき
            CGRect imageAuntie = [self rectForSprite:Movie_Sprite[5]]; // 近所のおばちゃん
            if (CGRectContainsPoint(imageAuntie, location) &&
                (touchY > Movie_Sprite[5].position.y * 1.3) ) { // 上半身をタッチしたとき
                touchCount++;
                if(myEffect[0].isPlaying == false) {
                    [self scene17_neighbor];
                }
            }
        }
        
    }
    

    
    /*
    if(page == 12){
        srand(time(NULL));
        if(iNumRand > 0){
            iRandKey = rand();
            iRandKey %=iNumRand;
            
            //配列に格納
            iRandVal = iaRandarray[iRandKey];
            iaRandarray[iRandKey] = iaRandarray[iNumRand - 1];
            [self scene12:iRandVal-1];
            CCLOG(@"%d",iRandVal-1);
            --iNumRand;

        }
            
    
        
    }else if(page == 15 && page15Count <5 && touchX > 350 && touchX < 720 && touchY > 150 && touchY < 607){
        if(optionnum3[1] == 0){
        [[SimpleAudioEngine sharedEngine] playEffect:@"kezuru.mp3"];
        }
        Movie_Sprite[0].opacity =  Movie_Sprite[0].opacity-51;
        [self scene15:touchX:touchY];
        page15Count++;
    }
     */
}

-(void) ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    /*if (_mouseJoint == NULL) return;
    UITouch *myTouch = [touches anyObject];
    CGPoint location = [myTouch locationInView:[myTouch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];
    b2Vec2 locationWorld = b2Vec2(location.x/PTM_RATIO, location.y/PTM_RATIO);
    _mouseJoint->SetTarget(locationWorld);*/
    touchMoveFlag = false;
    UITouch *touch = [touches anyObject];
    CGPoint location = [self convertTouchToNodeSpace: touch];
    float touchX,touchY;
    float touchMovedX,touchMovedY;
    touchX = location.x;
    touchY = location.y;
    touchMovedX = touchBeganX-touchX;
    touchMovedY = touchBeganY-touchY;
    //touchMovedX,Yを正の値に
    if(touchMovedX<0)touchMovedX = touchMovedX * -1;
    if(touchMovedY<0)touchMovedY = touchMovedY * -1;
    MovedRange = touchMovedX+touchMovedY;
    if(MovedRange>=450){
        touchMoveFlag = true;
        MovedRange=450;
    }
    //CCLOG(@"%d,%f",touchCount,MovedRange);
    //CCLOG(@"%f, %f,%f,%f", touchX, touchY,touchBeganX,touchBeganY);
    if (page == 2) {
        //Movie_Sprite[2].position = ccp(touchX,touchY); Check用
        if(touchCount == 0){
            if(touchMoveFlag ==true && touchY < 520){
                touchDrag = false;
                touchCount=1;
                [self scene2_TouchKusaFrontMax:MovedRange];
                MovedRange=0;
                touchMoveFlag = false;
            }else if(touchMoveFlag == false && touchX < 374 && touchY > 576){
                [self scene2_TouchMoveFrontRightWood:MovedRange];
            }else if(touchMoveFlag == false && touchX > 652 && touchY > 595){
                [self scene2_TouchMoveFrontLeftWood:MovedRange];
            }else if(touchMoveFlag == false && touchY < 520){
                [self scene2_TouchMoveFront:MovedRange];
            }
        }else if(touchCount == 1 && touchDrag == true){
            if(touchMoveFlag ==true && touchY < 500){
                touchCount=5;
                [self scene2_MoveKusaBackMax:MovedRange];
                touchMoveFlag = true;
                MovedRange = 0;
            }else if(touchMoveFlag == false && touchY < 500){
                [self scene2_TouchMoveBack:MovedRange];
            }
        }
        touchBeganFlag = false;
    }
}

- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *myTouch = [touches anyObject];
    CGPoint location = [myTouch locationInView:[myTouch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];
//    float touchX,touchY;
//    touchX = location.x;
//    touchY = location.y;
    
    if (_mouseJoint) {
        world->DestroyJoint(_mouseJoint);
        _mouseJoint = NULL;
    }

    if(page == 2){
        if(touchCount == 0){
            if(touchBeganFlag == true){
                [self scene2_MoveKusaFrontMax:MovedRange];
                MovedRange=0;
                touchMoveFlag = true;
                touchCount=1;
                touchBeganFlag = false;
            }
            if(touchMoveFlag == false && kusaFlag == true){//TouchMoveしていないとき
                [self scene2_MoveEndedKusaFront:MovedRange];
            }else if(touchMoveFlag == false && rightWoodFlag == true){
                [self scene2_MoveEndedKusaFrontRightWood:110];
            }else if(touchMoveFlag == false && leftWoodFlag ==true){
                [self scene2_MoveEndedKusaFrontLeftWood:110];
            }
            
        }else if(touchCount == 1){
            if(touchBeganFlag == true){
                touchCount=5;
                [self scene2_TouchKusaBackMax:MovedRange];
                MovedRange=0;
                touchMoveFlag = true;
                touchBeganFlag = false;
            }
            if(touchMoveFlag == false){//TouchMoveしていないとき
                [self scene2_MoveEndedKusaBack:MovedRange];
            }
        }else if(touchCount == 2){
            if(touchBeganFlag == true){
                touchCount=5;
                [self scene2_TouchKusaBackMax:MovedRange];
                MovedRange=0;
                touchMoveFlag = true;
                touchBeganFlag = false;
            }
            if(touchMoveFlag == false){//TouchMoveしていないとき
                [self scene2_MoveEndedKusaBack:MovedRange];
            }
        }
        kusaFlag = false;
        rightWoodFlag = false;
        leftWoodFlag = false;
        touchFlag = false;
    }
    
    // ドラッグやめたらとき 与吉の顔 縮小
    if (page == 14) {
        if (touchDrag) {
            // 拡大時SEを停止
            if([myEffect[0] isPlaying] == YES) {
                [myEffect[0] stop];
            }
            [self scene14_HeadPinch];
            [self playSound :1 :@"scene14_zoomout.mp3" :0.5];
            touchDrag = false;
        }
    }

}

-(void)ccTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    if (_mouseJoint) {
        world->DestroyJoint(_mouseJoint);
        _mouseJoint = NULL;
    }
    
    // タッチキャンセル時
    if (page == 14) {
        if (touchDrag) {
            // 拡大時SEを停止
            if([myEffect[0] isPlaying] == YES) {
                [myEffect[0] stop];
            }
            [self scene14_HeadPinch];
            [self playSound :1 :@"scene14_zoomout.mp3" :0.5];
            touchDrag = false;
        }
    }
}

//パーティクルのpreload
-(void)preloadParticleEffect:(NSString *)particleFile{
    [ARCH_OPTIMAL_PARTICLE_SYSTEM particleWithFile:particleFile];
}

- (void)accelerometer:(UIAccelerometer*)accelerometer didAccelerate:(UIAcceleration*)acceleration
{
    static float prevX=0, prevY=0;
    
    //#define kFilterFactor 0.05f
    #define kFilterFactor 1.0f	// don't use filter. the code is here just as an example
    
    float accelX = (float) acceleration.x * kFilterFactor + (1- kFilterFactor)*prevX;
    float accelY = (float) acceleration.y * kFilterFactor + (1- kFilterFactor)*prevY;
    
    prevX = accelX;
    prevY = accelY;
    
    // accelerometer values are in "Portrait" mode. Change them to Landscape left
    // multiply the gravity by 10
    b2Vec2 gravity( -accelY * 10, accelX * 10);
    
    world->SetGravity( gravity );
}

//設定読み込み
-(void)setteiRead{
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    optionnum3[0] = [userDefault integerForKey:@"OPTION1"];
    optionnum3[1] = [userDefault integerForKey:@"OPTION2"];
    optionnum3[2] = [userDefault integerForKey:@"OPTION3"];
    
    setteino[0] = optionnum3[0];
    setteino[1] = optionnum3[1];
    setteino[2] = optionnum3[2];
    
}
- (void) dealloc
{
    // in case you have something to dealloc, do it in this method
    delete world;
    world = NULL;
    
    delete m_debugDraw;
    
    // don't forget to call "super dealloc"
    [super dealloc];
}
@end
