#import "cocos2d.h"
#import "Box2D.h"
#import "GLES-Render.h"
#import "CCTransitionHelper.h"
#import "CDXPropertyModifierAction.h"
#import "SimpleAudioEngine.h"
#import "SoundPlayer.h"

// HelloWorldLayer
@interface MiddleScene : CCScene
{
	
}

// MiddleLayerを追加済みのシーンを返す
+ (CCScene *)sceneWithSceneId:(NSInteger)sceneId:(BOOL)nextPage;

@end

// MiddleLayerクラス
// MiddleSceneクラス上に表示するレイヤー。このレイヤー上に
// 背景やテキスト、選択肢を配置します。
@interface MiddleLayer : CCLayer {
    b2World* world;
	GLESDebugDraw *m_debugDraw;
    CCTexture2D *texture[20];
    int optionnum3[3];
    CCMenuItemImage *maryItem3;
    CCMenuItemImage *maryItem4;
    CCSprite *textBG_Sprite;
    int page;
    b2Body* m_bodies[7];
	b2Joint* m_joints[12];
    b2Fixture *_bottomFixture;
    CCSprite *Movie_Sprite[35];
    b2Fixture *_paddleFixture[7];
    
    b2MouseJoint *_mouseJoint;
    b2Body *body;
    b2Body* groundBody;
    
    CCMenu *scene2Menu;
    bool next;
    
    CDLongAudioSource *player;
    
    int page15Count;
    
    int iaRandarray[15]; /*乱数候補収納用変数*/
    int iCounter; /*ループカウンタ用変数*/
    int iNumRand; /*残り乱数候補数*/
    int iRandKey; /*乱数候補取得用変数*/
    int iRandVal; /*乱数の取得用変数*/
    
    int setteino[3];
    
    //プロ研で追加
    int touchCount;
    float touchBeganX,touchBeganY;
    bool rotateMax;
    bool touchFlag;
    bool touchFlag2;
    bool touchBeganFlag;
    bool touchMoveFlag;
    bool rightWoodFlag,leftWoodFlag,kusaFlag;
    bool soundFlag;
    float MovedRange;//移動距離
    CCSprite *Fish_Sprite[20];
    CCSprite *Tear_Sprite[40]; // 涙
    CCSprite *menu_sprite;
    ALuint SE_Array[5];
    int SE_ArrayCount;
    bool touchDrag; // タッチがドラッグ状態かどうか
    bool touchEnded; // タッチがエンド状態かどうか
    CGSize spriteSize; // スプライトサイズの一時的な保存
    id spriteAction;
    bool soundPlayFlag; // scene17で、サウンドが再生されているかどうか
    NSTimer *soundTimer; // timer用
    CDSoundSource *myEffect[3]; // CDSoundSource
    int soundCount; // CDSoundSource 順次再生用
    NSTimer *bufferTimer; // zbuffer用
    
    //パーティクル
    CCParticleSystem *particles[7];
    
    
@private
}

// 初期化処理を行った上でインスタンスを返すメソッド
+ (id)layerWithSceneId:(NSInteger)sceneId:(BOOL)nextPage;
- (id)initWithSceneId:(NSInteger)sceneId:(BOOL)nextPage;
-(void)PageEffect;
-(void)Playnarration;
-(void)setteiRead;
-(void)FadeOut;
+(void)lastReturn;
@end
