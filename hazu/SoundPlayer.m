//
//  SoundPlayer.m
//  kira
//
//  Created by mac on 11/08/31.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SoundPlayer.h"

SystemSoundID soundID[10];
ALuint narr;
ALuint se;
NSInteger sound_count = 0;
NSInteger sound_count2;
AVAudioPlayer *narrtion[23];
AVAudioPlayer *bgm[8];

@implementation SoundPlayer


//効果音有りの場合のみサウンドの読み込み 
+(void)sePlay:(NSInteger)switchNo:(NSInteger)Page{
    if(switchNo == 0){
        [self seLoad:@"monstar":0];
        [self seLoad:@"scene2_effect":1];
        [self seLoad:@"byon":2];
        
        
    }
}

+(void)bgmLoad:(NSInteger)switchNo{
    if (switchNo == 0) {
        [self bgmLoad2:@"scene1-music":0];
        [self bgmLoad2:@"scene7-music":1];
        [self bgmLoad2:@"scene8-music":2];
        [self bgmLoad2:@"scene10-music":3];
        [self bgmLoad2:@"scene11-music":4];
        [self bgmLoad2:@"scene2-music":5];
        [self bgmLoad2:@"scene5-music":6];
        [self bgmLoad2:@"scene15-music":7];

    }
}

+(void)bgmLoad2:(NSString *)Path:(NSInteger)num{
    NSString* a_file_path = [[NSBundle mainBundle] pathForResource:Path ofType:@"mp3"];
    NSURL *url = [NSURL fileURLWithPath:a_file_path];
    bgm[num] = [[AVAudioPlayer alloc] initWithContentsOfURL:url 
                                                      error:NULL];
}

+(void)bgmPlay:(NSInteger)switchNo:(NSInteger)num{
    if (switchNo == 0) {
        [self bgmStop:0 :sound_count];
        sound_count = num;
        //NSLog(@"num%d",sound_count);

        bgm[num].numberOfLoops = -1;
        if(num == 2){
            bgm[num].volume = 8.0f;
        }else if(num == 3){
            bgm[num].volume = 3.0f;
        }else if(num == 4){
            bgm[num].volume = 6.0f;
        }else if(num == 5){
            bgm[num].volume = 3.0f;
        }else if(num == 6){
            bgm[num].volume = 7.0f;
        }else if(num == 7){
            bgm[num].volume = 1.5f;
        }else{
            bgm[num].volume = 0.1f;
        }
        bgm[num].currentTime = 0;
        [bgm[num] play];
    }
}

+(void)bgmStop:(NSInteger)switchNo:(NSInteger)num{
    if (switchNo == 0) {
        if (bgm[num].isPlaying) {
            [bgm[num] stop];
        }
    }
}


+(void)bgmStop2:(NSInteger)switchNo{
    if (switchNo == 0) {
        //NSLog(@"num%d",sound_count);
        
        [bgm[sound_count] stop];
        [bgm[sound_count] release];
        bgm[sound_count] = nil;
    }
}

+(void)narraLoad:(NSInteger)switchNo:(NSInteger)Page3{
    if (switchNo == 0) {
        NSString *narrapath;
        narrapath  = [NSString stringWithFormat:@"scene%d",Page3];
        
        NSString* a_file_path = [[NSBundle mainBundle] pathForResource:narrapath ofType:@"caf"];
        NSURL *url = [NSURL fileURLWithPath:a_file_path];
        narrtion[Page3] = [[AVAudioPlayer alloc] initWithContentsOfURL:url 
                                                                 error:NULL];
    }
    
}


//サウンドエフェクト
+(void)seLoad:(NSString *)path:(NSInteger)num{
    NSString* a_file_path = [[NSBundle mainBundle] pathForResource:path ofType:@"caf"];
    NSURL *url = [NSURL fileURLWithPath:a_file_path];
    AudioServicesCreateSystemSoundID((CFURLRef)url, &soundID[num]);
}

+(void)PlaySE:(NSInteger)switchNo:(NSInteger)num{
    if(switchNo == 0){
        AudioServicesPlaySystemSound(soundID[num]);
    }
}

+(void)StopSE:(NSInteger)num{
    for (int i= 0; i<4; i++) {
        AudioServicesDisposeSystemSoundID(soundID[i]);
        
    }
}


+(void)PlayBG:(NSInteger)switchNo:(NSString *)path{
    if(switchNo == 0){
        [SimpleAudioEngine sharedEngine].backgroundMusicVolume = 0.6;
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:path];
    }
}

+(void)StopBG:(NSInteger)switchNo{
    if(switchNo == 0){
        [SimpleAudioEngine sharedEngine].backgroundMusicVolume = 0.6;
        [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
    }
}



+(void)PlayNarration:(NSInteger)Page{
    [narrtion[Page] play];
}

+(void)StopNarr:(NSInteger)switchNo:(NSInteger)Page{
    if(switchNo == 0){
        [narrtion[Page] stop];
        [narrtion[Page] release];
        narrtion[Page] = nil;
    }
}


+(void)StopSE2:(NSInteger)switchNo:(NSInteger)Page{
    [[SimpleAudioEngine sharedEngine] stopEffect:narr];
    [[SimpleAudioEngine sharedEngine] stopEffect:se];
    [SimpleAudioEngine sharedEngine].effectsVolume = 1.0;
}

+(void)countReset{
    sound_count = 0;
}



- (void) dealloc
{
	[super dealloc];
}

@end
