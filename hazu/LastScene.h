//
//  LastScene.h
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "CCTransitionHelper.h"
#import <UIKit/UIWebView.h>


@interface LastScene : CCScene {
    
}

// LastLayerを追加済みのシーンを返す
+ (CCScene *)sceneWithSceneId:(NSInteger)sceneId:(BOOL)nextPage;

@end

@interface LastLayer : CCLayer {
    UIWebView *webView_;
	UIToolbar *toolBar;
    UIActivityIndicatorView *activityInducator_;
    CCSprite *sprite2;
    CCSprite *sprite3;
    CCMenu *menu2;
@private
}

// 初期化処理を行った上でインスタンスを返すメソッド
+ (id)layerWithSceneId:(NSInteger)sceneId;
- (id) initWithSceneId:(NSInteger)sceneId;

// ボタンがタップされたときのイベントを記述するメソッド
- (void)tappedButton:(id)sender;
-(void)webBrowser:(NSString *)urltext;
@end

