#import "FirstScene.h"
#import "ScenesManager.h"

@implementation FirstScene
+ (CCScene *)sceneWithSceneId:(NSInteger)sceneId:(BOOL)nextPage {
    CCScene *scene = [FirstScene node];
    CCLayer *layer = [FirstLayer layerWithSceneId:sceneId];
    
    [scene addChild:layer];
    return scene;
}
@end

#pragma mark -
//FirstLayerクラス
@implementation FirstLayer  
#pragma mark インスタンスの初期化/解放

// インスタンスの初期化を行うメソッド。
// このシーン（レイヤー）で使用するオブジェクトを作成・初期化します。
+ (id)layerWithSceneId:(NSInteger)sceneId{
    return [[[self alloc] initWithSceneId:sceneId] autorelease];
}

-(id)initWithSceneId:(NSInteger)sceneId{
    self = [super init];
    if(self){
        self.tag = sceneId;
        self.isTouchEnabled = NO;
        
        //画面サイズ
        CGSize winSize = [[CCDirector sharedDirector] winSize];
        
        //説明１
        if(sceneId == 0){
            [[SimpleAudioEngine sharedEngine] preloadEffect:@"title.mp3"];
            CCTexture2D *background = [[CCTextureCache sharedTextureCache] addImage:@"setumei1.pvr.gz"];
            CCSprite *spriteBack = [CCSprite spriteWithTexture:background rect:CGRectMake(0,0,winSize.width,winSize.height)];
            spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);
            [self addChild: spriteBack];
            
                        
        //説明２   
        }else if(sceneId == 1){
            CCTexture2D *background = [[CCTextureCache sharedTextureCache] addImage:@"setumei2.pvr.gz"];
            CCSprite *spriteBack = [CCSprite spriteWithTexture:background rect:CGRectMake(0,0,winSize.width,winSize.height)];
            spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);
            [self addChild: spriteBack];
            
        //タイトル
        }else if(sceneId == 2){
                        
            self.isTouchEnabled = YES;
            CCTexture2D *background = [[CCTextureCache sharedTextureCache] addImage:@"gama_title.pvr.gz"];
            CCSprite *spriteBack = [CCSprite spriteWithTexture:background rect:CGRectMake(0,0,winSize.width,winSize.height)];
            spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);
            [self addChild: spriteBack];
            
                       
            
            CCMenuItemImage *systemmenu = [CCMenuItemImage 
                                           itemFromNormalImage:@"system.pvr.gz" 
                                           selectedImage: @"system.pvr.gz"
                                           target:self
                                           selector:@selector(choosedA:)];
            systemmenu.position = ccp(-450, 330);
            systemmenu.tag = 1;
            CCMenu *menu = [CCMenu menuWithItems:systemmenu, nil];
            [self addChild:menu];
            [self Storystart];
            // ちらつき防止用（すべてのページ）
            //[self zbuffer];
        }
        if(sceneId < 2){
            
            
            
            CCMenuItemImage *nextBTN = [CCMenuItemImage 
                                        itemFromNormalImage:@"title_next.pvr.gz" 
                                        selectedImage: @"title_next.pvr.gz"
                                        target:self
                                        selector:@selector(choosedA:)];
            nextBTN.position = ccp(0, -330);
            nextBTN.tag = 2;
            CCMenu *menu = [CCMenu menuWithItems:nextBTN, nil];
            [self addChild:menu];
        }
    }
    return self;
}
-(void)Storystart{
    [self setteiRead];
    CCTexture2D *background = [[CCTextureCache sharedTextureCache] addImage:@"start-hd.pvr.gz"];
    CCSprite *sprite3 = [CCSprite spriteWithTexture:background];
    sprite3.opacity =0.0f;
    sprite3.position = ccp(512, 100);
    [self addChild:sprite3]; 
    
    id Fade = [CCFadeIn actionWithDuration:1.0];
    id Fade2 = [CCFadeOut actionWithDuration:1.0];
    id action1 = [CCSequence actions:Fade,[CCDelayTime actionWithDuration:2.0f],Fade2,nil];
    id rep = [CCRepeatForever actionWithAction: [[action1 copy] autorelease]];
    [sprite3 runAction:rep];
    start = true;
    
    //タイトル音再生
    if(optionnum3[0] == 0){
        CDLongAudioSource *player = [[CDAudioManager sharedManager] audioSourceForChannel:kASC_Left];
        [player load:@"title.mp3"];
        player.volume = 1.0f;
        [player setNumberOfLoops:0];
        [player play];
    }
}

/* ちらつき処理用
 * @param on zバッファのON/OFF
 */
-(void)zbuffer {
    //[[CCDirector sharedDirector] setProjection:kCCDirectorProjection2D];  // 2Dプロジェクションを使い、必要に応じて3Dプロジェクションに切り替える。
    [[CCDirector sharedDirector] setDepthTest: YES];  // zバッファ使わない
}


#pragma mark タイトルイベント
-(void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    if(start){
        [self choosedA:nil];
    }
}

#pragma mark イベント処理
- (void)choosedA:(id)sender {
    if([sender tag] == 1){
        CCScene *newScene = [SceneManager nextSceneOfScene:self.parent choice:kSceneChoiseD next:true];
        [CCTransitionHelper replaceScene:newScene transitionType:TransitionTypePageForward duration:0.5];
        [self FadeOut];
        
    }else if([sender tag] == 2){
        CCScene *newScene = [SceneManager nextSceneOfScene:self.parent choice:kSceneChoiseA next:true];
        [CCTransitionHelper replaceScene:newScene transitionType:TransitionTypePageForward duration:0.5];
        
    }else{
        CCScene *newScene = [SceneManager nextSceneOfScene:self.parent choice:kSceneChoiseA next:true];
        [CCTransitionHelper replaceScene:newScene transitionType:TransitionTypePageForward duration:0.5];
        [self FadeOut];

    }
}

-(void)FadeOut{
    CDLongAudioSource *player = [[CDAudioManager sharedManager] audioSourceForChannel:kASC_Left];
    CDLongAudioSourceFader* fader = [[CDLongAudioSourceFader alloc] init:player interpolationType:kIT_Exponential startVal:player.volume endVal:0.0];
    [fader setStopTargetWhenComplete:YES];
    //Create a property modifier action to wrap the fader
    CDXPropertyModifierAction* action = [CDXPropertyModifierAction actionWithDuration:1.0 modifier:fader];
    [fader release];//Action will retain
    fader = nil;
    //id actionCallFuncN = [CCCallFuncN actionWithTarget:self selector:@selector(actionComplete:)];
    [[CCActionManager sharedManager] addAction:[CCSequence actions:action, nil] target:player paused:NO];
}


//設定読み込み
-(void)setteiRead{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    optionnum3[0] = [userDefault integerForKey:@"OPTION1"];
    optionnum3[1] = [userDefault integerForKey:@"OPTION2"];
    optionnum3[2] = [userDefault integerForKey:@"OPTION3"];
}


// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
    [self removeAllChildrenWithCleanup:YES];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    [[CCTextureCache sharedTextureCache] removeAllTextures];
	[super dealloc];
}
@end
