//
//  LastScene.m
//

#import "LastScene.h"
#import "ScenesManager.h"
#import "MiddleScene.h"

@implementation LastScene

// 各種のスプライトを表示するためのレイヤーを作成し、
// シーンに登録した上でシーン返します。
+ (CCScene *)sceneWithSceneId:(NSInteger)sceneId:(BOOL)nextPage {
	CCScene *scene = [LastScene node];
	CCLayer *layer = [LastLayer layerWithSceneId:sceneId];
	
    // 作成したLastSceneクラスのレイヤーをsceneに
    // 追加した上で、sceneの方を返しています。
	[scene addChild: layer];
    
	return scene;
}

@end

#pragma mark -

@implementation LastLayer
#pragma mark インスタンスの初期化/解放
// インスタンスの初期化を行うメソッド。
// このシーン（レイヤー）で使用するオブジェクトを作成・初期化します。
+ (id)layerWithSceneId:(NSInteger)sceneId {
    return [[[self alloc] initWithSceneId:sceneId] autorelease];
}

- (id)initWithSceneId:(NSInteger)sceneId {
    
    self = [super init];
	if (self) {
        self.tag = sceneId;
                
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"lastButton.plist" textureFile:@"lastButton.pvr.gz"];
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"lastButton.pvr.gz"];
        [self addChild:spriteSheet];
        
        CCSpriteFrameCache *frameCache2 = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache2 addSpriteFramesWithFile:@"karakuri.plist" textureFile:@"karakuri.pvr.gz"];
        CCSpriteBatchNode *spriteSheet2 = [CCSpriteBatchNode batchNodeWithFile:@"karakuri.pvr.gz"];
        [self addChild:spriteSheet2];
        
        CCSprite *buttonSprite1 = [CCSprite spriteWithSpriteFrameName:@"top_btn.png"];
        CCSprite *buttonSprite2 = [CCSprite spriteWithSpriteFrameName:@"top_btn_On.png"];
        CCSprite *buttonSprite3 = [CCSprite spriteWithSpriteFrameName:@"top_btn_On.png"];
        
        CCSprite *buttonSprite4 = [CCSprite spriteWithSpriteFrameName:@"credit_btn.png"];
        CCSprite *buttonSprite5 = [CCSprite spriteWithSpriteFrameName:@"credit_btn_On.png"];
        CCSprite *buttonSprite6 = [CCSprite spriteWithSpriteFrameName:@"credit_btn_On.png"];
        
        CCSprite *buttonSprite7 = [CCSprite spriteWithSpriteFrameName:@"stop01ButtonOff.png"];
        CCSprite *buttonSprite8 = [CCSprite spriteWithSpriteFrameName:@"stop01ButtonOn.png"];
        CCSprite *buttonSprite9 = [CCSprite spriteWithSpriteFrameName:@"stop01ButtonOn.png"];
        
        CCSprite *buttonSprite10 = [CCSprite spriteWithSpriteFrameName:@"stop02ButtonOff.png"];
        CCSprite *buttonSprite11 = [CCSprite spriteWithSpriteFrameName:@"stop02ButtonOn.png"];
        CCSprite *buttonSprite12 = [CCSprite spriteWithSpriteFrameName:@"stop02ButtonOn.png"];
        
        CCSprite *stop03ButtonOff = [CCSprite spriteWithSpriteFrameName:@"stop03ButtonOff.png"];
        CCSprite *stop03ButtonOn = [CCSprite spriteWithSpriteFrameName:@"stop03ButtonOn.png"];
        CCSprite *stop03ButtonOn2 = [CCSprite spriteWithSpriteFrameName:@"stop03ButtonOn.png"];
        
        CCSprite *buttonSprite13 = [CCSprite spriteWithSpriteFrameName:@"urlButtonOff.png"];
        CCSprite *buttonSprite14 = [CCSprite spriteWithSpriteFrameName:@"urlButtonOn.png"];
        CCSprite *buttonSprite15 = [CCSprite spriteWithSpriteFrameName:@"urlButtonOn.png"];
        
        CCSprite *buttonSprite16 = [CCSprite spriteWithSpriteFrameName:@"mapButtonOff.png"];
        CCSprite *buttonSprite17 = [CCSprite spriteWithSpriteFrameName:@"mapButtonOn.png"];
        CCSprite *buttonSprite18 = [CCSprite spriteWithSpriteFrameName:@"mapButtonOn.png"];
        
        CCSprite *buttonSprite19 = [CCSprite spriteWithSpriteFrameName:@"mapButtonOff.png"];
        CCSprite *buttonSprite20 = [CCSprite spriteWithSpriteFrameName:@"mapButtonOn.png"];
        CCSprite *buttonSprite21 = [CCSprite spriteWithSpriteFrameName:@"mapButtonOn.png"];
        
        CCTexture2D *texture;
        CCTexture2D *texture2;
        CCTexture2D *texture3;
        
        
       
        
        CCSprite *buttonSprite22 = [CCSprite spriteWithSpriteFrameName:@"karakuri1.png"];
        CCSprite *buttonSprite23 = [CCSprite spriteWithSpriteFrameName:@"karakuri1_on.png"];
        CCSprite *buttonSprite24 = [CCSprite spriteWithSpriteFrameName:@"karakuri1_on.png"];
        
        CCSprite *buttonSprite25 = [CCSprite spriteWithSpriteFrameName:@"karakuri2.png"];
        CCSprite *buttonSprite26 = [CCSprite spriteWithSpriteFrameName:@"karakuri2_on.png"];
        CCSprite *buttonSprite27 = [CCSprite spriteWithSpriteFrameName:@"karakuri2_on.png"];
        
        CCSprite *buttonSprite28 = [CCSprite spriteWithSpriteFrameName:@"karakuri3.png"];
        CCSprite *buttonSprite29 = [CCSprite spriteWithSpriteFrameName:@"karakuri3_on.png"];
        CCSprite *buttonSprite30 = [CCSprite spriteWithSpriteFrameName:@"karakuri3_on.png"];
        
        CCSprite *buttonSprite31 = [CCSprite spriteWithSpriteFrameName:@"karakuri4.png"];
        CCSprite *buttonSprite32 = [CCSprite spriteWithSpriteFrameName:@"karakuri4_on.png"];
        CCSprite *buttonSprite33 = [CCSprite spriteWithSpriteFrameName:@"karakuri4_on.png"];
        
        CCSprite *buttonSprite34 = [CCSprite spriteWithSpriteFrameName:@"pre.png"];
        CCSprite *buttonSprite35 = [CCSprite spriteWithSpriteFrameName:@"pre_on.png"];
        CCSprite *buttonSprite36 = [CCSprite spriteWithSpriteFrameName:@"pre_on.png"];
        
        if(sceneId == 21){
            [self zbufferOff];  // ちらつき防止
            texture = [[CCTextureCache sharedTextureCache] addImage:@"owari.pvr.gz"];
            CCSprite *sprite = [CCSprite spriteWithTexture:texture];
            sprite.anchorPoint = CGPointMake(0, 0);
            [self addChild:sprite];
            CCMenuItemImage *btn = [CCMenuItemImage 
                                    itemFromNormalImage:@"title_next.pvr.gz" 
                                    selectedImage: @"title_next.pvr.gz"
                                    target:self
                                    selector:@selector(pressPage:)];
            // 戻るボタン
            CCMenuItemImage *btnPre = [CCMenuItemImage 
                                       itemFromNormalImage:@"back_btn.pvr.gz" 
                                       selectedImage: @"back_btn.pvr.gz"
                                       target:self
                                       selector:@selector(pressPage:)];
            btn.tag = 1;
            btnPre.tag = 12;
            btn.position = ccp(110, -330);
            btnPre.position = ccp(-110, -330);
            CCMenu *menu = [CCMenu menuWithItems:btn, nil];
            CCMenu *menuPre = [CCMenu menuWithItems:btnPre, nil];
            [self addChild:menu];
            [self addChild:menuPre];
        }else if(sceneId == 22){
            texture = [[CCTextureCache sharedTextureCache] addImage:@"credit.pvr.gz"];
            CCSprite *sprite = [CCSprite spriteWithTexture:texture];
            sprite.anchorPoint = CGPointMake(0, 0);
            [self addChild:sprite];
            CCMenuItemImage *btn = [CCMenuItemImage 
                                    itemFromNormalImage:@"title_next.pvr.gz" 
                                    selectedImage: @"title_next.pvr.gz"
                                    target:self
                                    selector:@selector(pressPage:)];
            // 戻るボタン
            CCMenuItemImage *btnPre = [CCMenuItemImage 
                                       itemFromNormalImage:@"back_btn.pvr.gz" 
                                       selectedImage: @"back_btn.pvr.gz"
                                       target:self
                                       selector:@selector(pressPage:)];
            btn.tag = 1;
            btnPre.tag = 3;
            btn.position = ccp(110, -330);
            btnPre.position = ccp(-110, -330);
            CCMenu *menu = [CCMenu menuWithItems:btn, nil];
            CCMenu *menuPre = [CCMenu menuWithItems:btnPre, nil];
            [self addChild:menu];
            [self addChild:menuPre];
        }else if(sceneId == 23){
            texture = [[CCTextureCache sharedTextureCache] addImage:@"spot_bg.pvr.gz"];
            CCSprite *sprite = [CCSprite spriteWithTexture:texture];
            sprite.anchorPoint = CGPointMake(0, 0);
            [self addChild:sprite];
            
            texture2 = [[CCTextureCache sharedTextureCache] addImage:@"spot01.pvr.gz"];
            sprite2 = [CCSprite spriteWithTexture:texture2];
            sprite2.anchorPoint = CGPointMake(0, 0);
            sprite2.position = ccp(65, 275);
            [self addChild:sprite2];
            
            texture3 = [[CCTextureCache sharedTextureCache] addImage:@"spot02.pvr.gz"];
            sprite3 = [CCSprite spriteWithTexture:texture3];
            sprite3.anchorPoint = CGPointMake(0, 0);
            sprite3.position = ccp(1089, 275);
            [self addChild:sprite3];
            
            CCMenuItemImage *btn = [CCMenuItemImage 
                                    itemFromNormalImage:@"title_next.pvr.gz" 
                                    selectedImage: @"title_next.pvr.gz"
                                    target:self
                                    selector:@selector(pressPage:)];
            btn.tag = 2;
            btn.position = ccp(110, -330);
            
            CCMenuItemSprite *btn2 = [CCMenuItemSprite 
                                      itemFromNormalSprite:buttonSprite4
                                      selectedSprite:buttonSprite5
                                      disabledSprite:buttonSprite6
                                      target:self
                                      selector:@selector(pressPage:)];
            btn2.tag = 3;
            btn2.position = ccp(-110, -330);
            
            CCMenuItemSprite *btn3 = [CCMenuItemSprite 
                                      itemFromNormalSprite:buttonSprite13
                                      selectedSprite:buttonSprite14
                                      disabledSprite:buttonSprite15
                                      target:self
                                      selector:@selector(pressPage:)];
            btn3.tag = 4;
            btn3.position = ccp(275, -170);
            
            
            CCMenuItemSprite *spotPlace1 = [CCMenuItemSprite 
                                            itemFromNormalSprite:buttonSprite7
                                            selectedSprite:buttonSprite8
                                            disabledSprite:buttonSprite9
                                            target:self
                                            selector:@selector(pressPage:)];
            spotPlace1.position = ccp(-320, -170);
            spotPlace1.tag =5;
            
            
            CCMenuItemSprite *spotPlace2 = [CCMenuItemSprite 
                                            itemFromNormalSprite:buttonSprite10
                                            selectedSprite:buttonSprite11
                                            disabledSprite:buttonSprite12
                                            target:self
                                            selector:@selector(pressPage:)];
            spotPlace2.position = ccp(-45, -170);
            spotPlace2.tag =6;
            
            
            CCMenu *menu = [CCMenu menuWithItems:btn,btn2,btn3,spotPlace1,spotPlace2,nil];
            [self addChild:menu];
            
            CCMenuItemSprite *mapButton = [CCMenuItemSprite 
                                           itemFromNormalSprite:buttonSprite16
                                           selectedSprite:buttonSprite17
                                           disabledSprite:buttonSprite18
                                           target:self
                                           selector:@selector(mapPage:)];
            mapButton.position = ccp(400, -90);
            mapButton.tag = 1;
            CCMenuItemSprite *mapButton2 = [CCMenuItemSprite 
                                            itemFromNormalSprite:buttonSprite19
                                            selectedSprite:buttonSprite20
                                            disabledSprite:buttonSprite21
                                            target:self
                                            selector:@selector(mapPage:)];
            mapButton2.position = ccp(1424, -90);
            mapButton2.tag = 2;
            menu2 = [CCMenu menuWithItems:mapButton,mapButton2,nil];
            [self addChild:menu2];
        }else if(sceneId == 24){
            
            texture = [[CCTextureCache sharedTextureCache] addImage:@"karakuri_bg.pvr.gz"];
            CCSprite *sprite = [CCSprite spriteWithTexture:texture];
            sprite.anchorPoint = CGPointMake(0, 0);
            [self addChild:sprite];
            
            CCMenuItemSprite *btn = [CCMenuItemSprite 
                                     itemFromNormalSprite:buttonSprite1
                                     selectedSprite:buttonSprite2
                                     disabledSprite:buttonSprite3
                                     target:self
                                     selector:@selector(pressPage:)];
            btn.tag = 7;
            btn.position = ccp(110, -330);
            
            CCMenuItemSprite *btn2 = [CCMenuItemSprite 
                                      itemFromNormalSprite:buttonSprite34
                                      selectedSprite:buttonSprite35
                                      disabledSprite:buttonSprite36
                                      target:self
                                      selector:@selector(pressPage:)];
            btn2.tag = 3;
            btn2.position = ccp(-110, -330);

            
            CCMenuItemSprite *btn3 = [CCMenuItemSprite 
                                     itemFromNormalSprite:buttonSprite22
                                     selectedSprite:buttonSprite23
                                     disabledSprite:buttonSprite24
                                     target:self
                                     selector:@selector(pressPage:)];
            btn3.tag = 8;
            btn3.position = ccp(-270, 150);
            
            CCMenuItemSprite *btn4 = [CCMenuItemSprite 
                                      itemFromNormalSprite:buttonSprite25
                                      selectedSprite:buttonSprite26
                                      disabledSprite:buttonSprite27
                                      target:self
                                      selector:@selector(pressPage:)];
            btn4.tag = 9;
            btn4.position = ccp(-260, -50);
            
            CCMenuItemSprite *btn5 = [CCMenuItemSprite 
                                      itemFromNormalSprite:buttonSprite28
                                      selectedSprite:buttonSprite29
                                      disabledSprite:buttonSprite30
                                      target:self
                                      selector:@selector(pressPage:)];
            btn5.tag = 10;
            btn5.position = ccp(255, 150);
            
            CCMenuItemSprite *btn6 = [CCMenuItemSprite 
                                      itemFromNormalSprite:buttonSprite31
                                      selectedSprite:buttonSprite32
                                      disabledSprite:buttonSprite33
                                      target:self
                                      selector:@selector(pressPage:)];
            btn6.tag = 11;
            btn6.position = ccp(255, -50);
            
            CCMenu *menu = [CCMenu menuWithItems:btn,btn2,btn3,btn4,btn5,btn6,nil];
            [self addChild:menu];

            
            
            
        }
    }
	return self;
}

-(void)pressPage:(id)sender{
    if ([sender tag] == 1) {
        CCScene *newScene = [SceneManager nextSceneOfScene:self.parent
                                                    choice:kSceneChoiseA next:true];
        [CCTransitionHelper replaceScene:newScene transitionType:TransitionTypePageForward duration:0.5];
    }else if([sender tag] == 2){
        CCScene *newScene = [SceneManager nextSceneOfScene:self.parent
                                                    choice:kSceneChoiseA next:true];
        [CCTransitionHelper replaceScene:newScene transitionType:TransitionTypePageForward duration:0.5];
    }else if([sender tag] == 3){        
        CCScene *newScene = [SceneManager nextSceneOfScene:self.parent
                                                    choice:kSceneChoiseB next:true];
        [CCTransitionHelper replaceScene:newScene transitionType:TransitionTypePageBackward duration:0.5];
    }else if([sender tag] == 4){
        NSString *url = [NSString stringWithFormat:@"http://www.city.nishio.aichi.jp/"];
        [self webBrowser:url];
    }else if([sender tag] == 5){
        id move1 = [CCMoveTo actionWithDuration:0.5f position:ccp(65,275)];
        id ease1 =[CCEaseSineOut actionWithAction:move1];
        [sprite2 runAction:ease1];
        
        id move2 = [CCMoveTo actionWithDuration:0.5f position:ccp(1024,275)];
        id ease2 =[CCEaseSineOut actionWithAction:move2];
        [sprite3 runAction:ease2];
        
        id move3 = [CCMoveTo actionWithDuration:0.5f position:ccp(520,380)];
        id ease3 =[CCEaseSineOut actionWithAction:move3];
        [menu2 runAction:ease3];
        
    }else if([sender tag] == 6){
        id move1 = [CCMoveTo actionWithDuration:0.5f position:ccp(-1024,275)];
        id ease1 =[CCEaseSineOut actionWithAction:move1];
        [sprite2 runAction:ease1];
        
        id move2 = [CCMoveTo actionWithDuration:0.5f position:ccp(65,275)];
        id ease2 =[CCEaseSineOut actionWithAction:move2];
        [sprite3 runAction:ease2];
        
        id move3 = [CCMoveTo actionWithDuration:0.5f position:ccp(-510,380)];
        id ease3 =[CCEaseSineOut actionWithAction:move3];
        [menu2 runAction:ease3];
        
    }else if([sender tag] == 7){
        CCScene *newScene = [SceneManager nextSceneOfScene:self.parent
                                                    choice:kSceneChoiseC next:true];
        [CCTransitionHelper replaceScene:newScene transitionType:TransitionTypePageForward duration:0.5];
    }else if([sender tag] == 8){
        NSURL *appURL = [NSURL URLWithString:@"http://itunes.apple.com/jp/app/ming-jun-ji-liangsan/id463714708?l=ja&ls=1&mt=8"];
        [[UIApplication sharedApplication] openURL:appURL]; 
    }else if([sender tag] == 9){
        NSURL *appURL = [NSURL URLWithString:@"http://itunes.apple.com/jp/app/yi-seno-dadjouchin/id477617280?l=ja&ls=1&mt=8"];
        [[UIApplication sharedApplication] openURL:appURL]; 
    }else if([sender tag] == 10){
        NSURL *appURL = [NSURL URLWithString:@"http://itunes.apple.com/jp/app/tian-gouno-yuuchiwa/id517510934?l=ja&ls=1&mt=8"];
        [[UIApplication sharedApplication] openURL:appURL]; 
    }else if([sender tag] == 11){
        NSURL *appURL = [NSURL URLWithString:@"http://itunes.apple.com/jp/app/zheng-wen-yan/id524936420?l=ja&ls=1&mt=8"];
        [[UIApplication sharedApplication] openURL:appURL]; 
    } else if([sender tag] == 12){ // 終わりページから前ページに戻るとき
        [MiddleLayer lastReturn];
        CCScene *newScene = [SceneManager nextSceneOfScene:self.parent
                                                    choice:kSceneChoiseB next:true];
        [CCTransitionHelper replaceScene:newScene transitionType:TransitionTypePageBackward duration:0.5];
    }
    
    
}
-(void)mapPage:(id)sender{
    if ([sender tag] == 1) {
        NSString *url = [NSString stringWithFormat:@"http://g.co/maps/g6fr3"];
        [self webBrowser:url];
    }else if([sender tag] == 2){
        NSString *url = [NSString stringWithFormat:@"http://g.co/maps/g6fr3"];
        [self webBrowser:url];
    }
}

//ちらつき処理用
-(void)zbufferOff {
    //[[CCDirector sharedDirector] setProjection:kCCDirectorProjection2D];  // 2Dプロジェクションを使い、必要に応じて3Dプロジェクションに切り替える。
    [[CCDirector sharedDirector] setDepthTest: YES];  // zバッファ使わない
}

-(void)webBrowser:(NSString *)urltext{
    NSString *path = [NSString stringWithFormat:@"%@",urltext];
    NSURL *url = [NSURL URLWithString:path];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    
    
    webView_ = [[UIWebView alloc] initWithFrame:CGRectMake(0, 46, 1024, 722)];
    
    webView_.bounds = CGRectMake(0, 46, 1024, 722);
    webView_.scalesPageToFit = YES;
    
    //UIToolbarの生成
    toolBar = [[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 1024, 46)] autorelease];
    
    
    //インジケーターの追加
    /* activityInducator_ = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
     activityInducator_.frame = CGRectMake(0, 0, 20, 20);
     UIBarButtonItem *inducator = [[UIBarButtonItem alloc] initWithCustomView:activityInducator_];
     UIBarButtonItem *adjustment =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
     */
    
    //戻るボタン
    UIBarButtonItem *goBackButton = [[UIBarButtonItem alloc]init];
    goBackButton.title = @"閉じる";
    goBackButton.style = UIBarButtonItemStyleBordered;
    goBackButton.target = self;
    goBackButton.action = @selector(goBack);
    
    NSArray *elements = [[NSArray alloc] initWithObjects:goBackButton, nil];
    
    [goBackButton release];
    [toolBar setItems:elements animated:YES];
    [elements release];
    elements = nil;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0f];
    [UIView setAnimationTransition:UIViewAnimationTransitionCurlDown 
                           forView:[[CCDirector sharedDirector] openGLView] cache:YES];
    //webView_.delegate =self;
    [[[CCDirector sharedDirector] openGLView] addSubview:webView_];
    [[[CCDirector sharedDirector] openGLView] addSubview:toolBar];
    
    [UIView commitAnimations];
    [webView_ loadRequest:req];
}

- (void)goBack {
	[UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0f];
    [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp
                           forView:[[CCDirector sharedDirector] openGLView] cache:YES];
    [webView_ removeFromSuperview];
    [toolBar removeFromSuperview];
    [UIView commitAnimations];
}


#pragma mark イベント処理
- (void)tappedButton:(id)sender {
    CCScene *newScene = [SceneManager nextSceneOfScene:self.parent
                                                choice:kSceneChoiseA next:true];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionTurnOffTiles
                                               transitionWithDuration:1.5
                                               scene:newScene]];
}

- (void) dealloc
{
    
    [self removeAllChildrenWithCleanup:YES];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    [webView_ release];
	[super dealloc];
}

@end